# A diferència el reduce_palette, també agafa una paleta original. Dels colors obtinguts mira quins hi ha a la paleta original i hi posa el mateix índex

# !!!!!!!!!! NO ho he enfocat bé !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!!!!! La imatge que ja tinc, només cal que busqui els nous centres i assigni els punt !!!!!!
# !!!!!!!!!! No necessito que trobi els nous centres !!!!!!!!!!!!!!!!!!!!!!
import cv2
import numpy as np
import subprocess
import threading

from matplotlib.pyplot import imshow
from matplotlib import pyplot as plt
from PIL import Image, ImageDraw


def tasca():
    subprocess.call(['xview', './colors.png'])


# Per reduir la paleta usa el mètode del K-Means
img_BGR = cv2.imread('./bricks_sc7.png')
img = cv2.cvtColor(img_BGR, cv2.COLOR_BGR2RGB)

Z = img.reshape((-1, 3))

Z = np.float32(Z)
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 1.0)
K = 16
ret, label, center = cv2.kmeans(Z, K, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)

# Now convert back into uint8, and make original image
center = np.uint8(center)  # El center té els centres dels K-means, els 16 colors
# He de passar de paleta de 256*3 a paleta de 8*3. Redueixo cada croma
center_3b = np.uint8(center / 32)
# Al reduir la paleta hi ha colors que han convergit, com el negre. Vull eliminar repeticions per així poder utilitzar un diccionari lliure
nou_dic = {0: 0}
quantsdif = 1
index_colors_diferents = [0]
# He de fer amb els diferents un diccionari dels reubicats. Primer miraré tots els colors que són diferents en un array i un cop creat el diccionari els reubicaré. Així faré dic_reubicat[nou_dic[pos]]
for k in range(1, K):
    trobat = False
    for dic in nou_dic:
        if np.array_equal(center_3b[k], center_3b[dic]):
            nou_dic[k] = dic
            trobat = True
            break
    if not trobat:
        nou_dic[k] = k
        index_colors_diferents.append(k)
        quantsdif += 1

# Si converteixo la mateixa imatge diferents vegades, pot donar resultats diferents

# Ara miro els colors que hi havia a la paleta passada i si coincideixen he de canviar l'índex
with open("imatge.pl7", "rb") as f:
    bytes_paleta = f.read()

arr_paleta_flatten = np.empty(0).astype('int8')
for k in range(len(bytes_paleta) - 7):
    arr_paleta_flatten = np.append(arr_paleta_flatten, bytes_paleta[k + 6] >> 4)
    arr_paleta_flatten = np.append(arr_paleta_flatten, bytes_paleta[k + 7] & 0xF)
arr_paleta = arr_paleta_flatten.reshape(16, 3)
# Busquem dels colors que teníem quins ja existeixen
quants_colors_trobats_antiga_paleta = 0
dic_paleta_antiga = {}
arr_colors_no_paleta_antiga = np.empty(0).astype('int8')
for k, color_dibuix in enumerate(index_colors_diferents):
    trobat = False
    for idx, color_paleta_antiga in enumerate(arr_paleta):
        if np.array_equal(color_paleta_antiga, center_3b(color_dibuix)):
            trobat = True
            quants_colors_trobats_antiga_paleta = quants_colors_trobats_antiga_paleta + 1
            dic_paleta_antiga[color_dibuix] = idx
            break
    if not trobat:
        # La paleta anterior és completa, si no està a la paleta, els hauré de guardar per poder preguntar què es vol fer amb ells
        arr_colors_no_paleta_antiga = np.append(arr_colors_no_paleta_antiga, color_dibuix)

# A on hem de posar els que no hem trobat? Els hem de mapejar a un dels possibles
# Mostro imatge dels colors de la paleta i pregunto pels colors que falten
if len(arr_colors_no_paleta_antiga):
    img_paleta = Image.new("RGB", (16 * 5, 16 * (16 + len(arr_colors_no_paleta_antiga))))
    # 5 columnes de 16 pels arrays de 3 i 16*2 per pintar el color ; 16 columnes que hi ha la paleta més la dels colors
    img_draw = ImageDraw.Draw(img_paleta)
    for k, color in enumerate(arr_paleta):
        img_draw.text([16 + 2, k * 16], str(color[0]))
        img_draw.text([2 * 16 + 2, k * 16], str(color[1]))
        img_draw.text([3 * 16 + 2, k * 16], str(color[2]))
        img_draw.rectangle([(4 * 16, k * 16), (6 * 16, k * 16)], fill=tuple(color), outline="white")
    for k, color in enumerate(arr_colors_no_paleta_antiga):
        img_draw.text([16 + 2, (17 + k) * 16], str(color[0]))
        img_draw.text([2 * 16 + 2, (17 + k) * 16], str(color[1]))
        img_draw.text([3 * 16 + 2, (17 + k) * 16], str(color[2]))
        img_draw.rectangle([(4 * 16, k * 16), (6 * 16, (17 + k) * 16)], fill=tuple(color), outline="white")

    img_paleta.save("./paleta.png")

# Ara hem de preguntar com a l'altre a on s'han de posar els colors que no estan indexats

res = center[label.flatten()]
res2 = res.reshape((img.shape))

# Ara hem de passar la imatge a format binari, he d'afegir la capçalera de 7 bits
# Crec que només he de traduir els bits del label, té 4 bits de 0-15
with open("imatge_reub.sc7", "wb") as f:
    capçalera = b'\xfe\x01\x02\x03\x04\x05\x06'
    f.write(capçalera)
    labflat = label.flatten()
    for k in range(int(len(labflat) / 2)):
        valor = int(dic_reubicat[nou_dic[labflat[2 * k]]] << 4) + dic_reubicat[nou_dic[labflat[2 * k + 1]]]
        # valor = int(dic_reubicat[nou_dic[labflat[2 * k]]] << 4) + dic_reubicat[nou_dic[labflat[2 * k + 1]]]
        f.write(np.byte(valor))

# Em falta guardar la paleta que és el center. En aquest enllaç el guarden en un binari apart
# He intentat mirar que la paleta fos compatible amb el format pl7 del mif https://www.msx.org/forum/msx-talk/development/mifui-and-loadsc7bas-bad-file-mode, però no n'he tret el format
# El fusion-c fa el format de RGB amb array de 3 numèric. Així és com el guardaré
centflat = center_3b.flatten()
with open("imatge.pl7", "wb") as f:
    capçalera = b'\xfe\x01\x02\x03\x04\x05\x06'
    f.write(capçalera)
    for k in range(int(len(centflat) / 2)):
        valor = (centflat[2 * k] << 4) + centflat[2 * k + 1]
        f.write(np.byte(valor))

print(f"Hem trobat {len(index_colors_diferents)} colors diferents")
plt.imshow(res2)
plt.show()
