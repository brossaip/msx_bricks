/*
  Programa modular de la pantalla inicial per a ser cridat pel principal
  Podria fer les funcions comunes, però si ja es queda dins dels 16K no tinc cap
  problema que les torni a cridar. Encara que no sigui òptim
*/
#include "FM_MBM.h"
#include "fusion-c/include/msx_fusion.h"
#include "fusion-c/include/vdp_graph2.h"
#include <string.h>

// Definim constants de l'animació inicial
#define TEMPS_CAIGUDA_MAONS 10
#define POS_Y_MAONS 128

// Constants del scroll text
#define AMPLADA_CARACTER 8
#define ALCADA_CARACTER 8
#define ALCADA_BANDERA 14

unsigned int y_pos_caracter_a_copiar[3]; // Català 0, Anglès 1, Castellà 2
char alcada_caracter_a_copiar[3];
char pos_caracter_a_copiar[3]; // Català 0, Anglès 1, Castellà 2
int pos_text[3];

__at 0x8000 char nombre_jugadors;

// Definit dins la funció genera molt codi, no sé perquè. És una de les recomanacions de l'Eric que les variables siguin globals
char text[183] = {
      "wx1- 1 JUGADOR. 2- 2 JUGADORS. EN JOE ES CONTROLA AMB LES FLETXES I ESPAI PER AGAFAR IMPULS PER SALTAR. EL SAM ES CONTROLA AMB LES LLETRES A,D I EL SHIFT PER SALTAR. ES PODRAN REUNIR?"};
char text_eng[178] = {"uv1- 1 PLAYER. 2- 2 PLAYERS. JOE IS CONTROLLED BY THE ARROWS AND SPACEBAR TO GET BOOSTS TO JUMP. SAM IS CONTROLLED WITH THE LETTERS A,D AND SHIFT TO JUMP. COULD THEY BE REUNITED?"};
char text_spa[179] = {"yz1- 1 JUGADOR. 2- 2 JUGADORES. JOE SE CONTROLA CON LAS FLECHAS Y ESPACIO PARA COGER IMPULSO Y SALTAR. SAM SE CONTROLA CON LAS LETRAS A,D Y EL SHIFT PARA SALTAR. SE PODRAN REUNIR?"};


static FCB file; // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC5 2560

unsigned char LDbuffer[BUFFER_SIZE_SC5]; // Création d'un buffer de 2560 octets (20
                                     // lignes en Sc5)
#define HALT __asm halt __endasm     // wait for the next interrupt
void WAIT(int cicles) {
  int i;
  for (i = 0; i < cicles; i++)
    HALT;
  return;
}

void FT_SetName( FCB *p_fcb, const char *p_name )  // Routine servant à vérifier le format du nom de fichier
{
  char i, j;
  memset( p_fcb, 0, sizeof(FCB) );
  for( i = 0; i < 11; i++ ) {
    p_fcb->name[i] = ' ';
  }
  for( i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++ ) {
    p_fcb->name[i] =  p_name[i];
  }
  if( p_name[i] == '.' ) {
    i++;
    for( j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.'); j++ ) {
      p_fcb->ext[j] =  p_name[i + j] ;
    }
  }
}


void FT_errorHandler(char n, char *name)            // Gère les erreurs
{
  Screen(0);
  SetColors(15,6,6);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
  Exit(0);
}

int FT_LoadSc5Image(
    char *file_name, unsigned int start_Y, char *buffer,
    unsigned int amplada_linia,
    unsigned int tamany_buffer) // Charge les données d'un fichiers
{
  // El tamany de tamany_buffer és el nombre de bytes que fan les 20 linies. En
  // screen 5 és 2560, però en screen 7 és 2560*2
  int rd = 2560;

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  while (rd != 0) {
    rd = fcb_read(&file, buffer,
                  tamany_buffer); // Read 20 lines of image data (128bytes per
                                  // line in screen5)
    HMMC(buffer, 0, start_Y, amplada_linia, 20); // Move the buffer to VRAM.
    start_Y = start_Y + 20;
  }

  return (1);
}

int FT_openFile(char *file_name) {
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }
}

unsigned char OldHook[5];
unsigned char MyHook[5];
unsigned char IntFunc[5];
unsigned char TypeOfInt;
__at 0xFD9F unsigned char VdpIntHook[];
__at 0xFD9A unsigned char AllIntHook[];
__at 0xF344 unsigned char RAMAD3;

void InterruptHandlerHelper (void) __naked
{
__asm
    push af
    call _IntFunc
    pop af
    jp _OldHook
__endasm;
}

void InitializeMyInterruptHandler (int myInterruptHandlerFunction, unsigned char isVdpInterrupt)
{
    unsigned char ui;
    MyHook[0]=0xF7; //RST 30 is interslot call both with bios or dos
    MyHook[1]=RAMAD3; //Page 3 generally is not paged out and is the slot of the ram, so this should be good
    MyHook[2]=(unsigned char)((int)InterruptHandlerHelper&0xff);
    MyHook[3]=(unsigned char)(((int)InterruptHandlerHelper>>8)&0xff);
    MyHook[4]=0xC9;
    IntFunc[0]=0xCD; //CALL
    IntFunc[1]=(unsigned char)((int)myInterruptHandlerFunction&0xff);
    IntFunc[2]=(unsigned char)(((int)myInterruptHandlerFunction>>8)&0xff);
    IntFunc[3]=0xC9;
    TypeOfInt = isVdpInterrupt;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();
    if (isVdpInterrupt)
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=VdpIntHook[ui];
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=MyHook[ui];
    }
    else
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=AllIntHook[ui];
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=MyHook[ui];
    }

    //Re-enable Interrupts
    EnableInterrupt();
}

void EndMyInterruptHandler (void)
{
    unsigned char ui;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();

    if (TypeOfInt)
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=OldHook[ui];
    else
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=OldHook[ui];

    //Re-enable Interrupts
    EnableInterrupt();
}

void main_loop(void) {
  playPatro();
}

void calcula_pos_caracter_a_copiar( char caracter, char idioma) {
  y_pos_caracter_a_copiar[idioma] = 256 + 167;
  alcada_caracter_a_copiar[idioma] = ALCADA_CARACTER;

  if (caracter > 116) {
    y_pos_caracter_a_copiar[idioma] = 256 + 174;
    alcada_caracter_a_copiar[idioma] = ALCADA_BANDERA;
    // Bandera anglesa uv, catalana wx, espanyola yz
    if (caracter == 117) {
      // u
      pos_caracter_a_copiar[idioma] = 0;
    }
    if (caracter == 118) {
      // v
      pos_caracter_a_copiar[idioma] = AMPLADA_CARACTER;
    }
    if (caracter == 119) {
      // w
      pos_caracter_a_copiar[idioma] = 2 * AMPLADA_CARACTER;
    }
    if (caracter == 120) {
      // x
      pos_caracter_a_copiar[idioma] = 3 * AMPLADA_CARACTER;
    }
    if (caracter == 121) {
      // y
      pos_caracter_a_copiar[idioma] = 4 * AMPLADA_CARACTER;
    }
    if (caracter == 122) {
      // z
      pos_caracter_a_copiar[idioma] = 5 * AMPLADA_CARACTER;
    }
  }
  else {
    if (caracter == 32) {
      // espai
      pos_caracter_a_copiar[idioma] = 246;
    }
    if (caracter == 39) {
      // '
      pos_caracter_a_copiar[idioma] = 232;
    }
    if (caracter == 46) {
      // .
      pos_caracter_a_copiar[idioma] = 224;
    }
    if (caracter == 44) {
      // ,
      pos_caracter_a_copiar[idioma] = 240;
    }
    if (caracter == 49) {
      // 1
      pos_caracter_a_copiar[idioma] = 208;
    }
    if (caracter == 50) {
      // 2
      pos_caracter_a_copiar[idioma] = 216;
    }
    if (caracter == 63) {
      // ?
      y_pos_caracter_a_copiar[idioma] = 256 + 174;
      pos_caracter_a_copiar[idioma] = 49;
    }
    if (caracter == 45) {
      // ?
      y_pos_caracter_a_copiar[idioma] = 256 + 174;
      pos_caracter_a_copiar[idioma] = 57;
    }
    if (caracter >= 65 && caracter <= 90) {
      pos_caracter_a_copiar[idioma] = (caracter - 65) * AMPLADA_CARACTER;
    }
  }
}

void animacio_pantalla_inicial(void) {
  char mypalette[] = {
    0, 0,0,0,
    1, 1,0,1,
    2, 1,1,2,
    3, 2,1,1,
    4, 3,1,1,
    5, 1,2,3,
    6, 2,3,4,
    7, 5,0,3,
    8, 6,1,0,
    9, 5,3,2,
    10, 7,5,2,
    11, 7,6,2,
    12, 6,6,6,
    13, 7,6,5,
    14, 6,7,7,
    15, 7,7,7
  };

  Screen(5);

  FT_LoadSc5Image("sergialf.S05", 256, LDbuffer,256,BUFFER_SIZE_SC5);

  SetSC5Palette((Palette *)mypalette);

  // Música
  FT_openFile("IM_LOST.MBM");
  fcb_read(&file, songFile, sizeof(songFile));
  ompleCapcaleraPatrons();
  InicialitzemCanalsReproductor();
  tempo = capcalera.start_tempo;

  InitializeMyInterruptHandler((int)main_loop, 1);

  // Farem els personatges i els bricks a la vegada
  char pos_x = 255; // Original arribava a 195 com a màxim
  int pos_x_gat = -30;
  while (pos_x > 225) {
    for (int k = 0; k < 6; k++) {
      HMMM(k * 32, 256 + 96, pos_x, POS_Y_MAONS, 32, 32);
      if (pos_x_gat < 0) {
        // El Man surt de darrera la pantalla de mica en mica, però el gat si el
        // poso a la coordenada 0 surt tot, l'he d'anar copiant de mica en mica
        HMMM((char)((k * 32) - pos_x_gat), 256 + 11, 0, POS_Y_MAONS+10,
             (char)(32 + pos_x_gat), 32);
      } else {
        HMMM(k * 32, 256 + 11, pos_x_gat, POS_Y_MAONS+10, 32, 32);
      }
      WAIT(TEMPS_CAIGUDA_MAONS);
      HMMM(200, 256, pos_x, POS_Y_MAONS, 4, 32); // Borrem els quadrats. No cal tot, només una part
      HMMM(200, 256, pos_x_gat, POS_Y_MAONS, 4, 32);
      pos_x = pos_x - 4;
      pos_x_gat = pos_x_gat + 4;
    }
  }

  // Els maons tarden 8 animacions en estar al lloc, els de la filera d'abaix
  char pos_mao_anim =   0;
  char numero_animacio_caracters = 0;
  char num_animacions;
  for ( num_animacions = 0; num_animacions < 8;
                           num_animacions++) {
    // Brick central
    HMMM(0, 256 + 128, 82 + 32, pos_mao_anim, 32, 32);
    HMMM(numero_animacio_caracters * 32, 256 + 11, pos_x_gat, POS_Y_MAONS+10, 32, 32);
    HMMM(numero_animacio_caracters * 32, 256 + 96, pos_x, POS_Y_MAONS, 32, 32);
    WAIT(TEMPS_CAIGUDA_MAONS);
    // Borrem
    HMMM(200, 256, 82 + 32, pos_mao_anim, 32, 16);
    HMMM(200, 256, pos_x, POS_Y_MAONS, 4, 32); // Borrem els quadrats
    HMMM(200, 256, pos_x_gat, POS_Y_MAONS+10, 4, 32);
    pos_x = pos_x - 4;
    pos_x_gat = pos_x_gat + 4;
    pos_mao_anim = pos_mao_anim + 16;
    numero_animacio_caracters ++;
    if (numero_animacio_caracters == 6) {
      numero_animacio_caracters = 0;
    }
  }
  // L'últim maó queda borrat, el copiem totalment de nou
  HMMM(0, 256 + 128, 82 + 32, pos_mao_anim, 32, 32);

  // La part de dalt ja fa que arribin al mig amb el maó principal, faré ara el moviment aleatori del Man i el Sam mentre van caient els maons. No, faré l'aleatori només al final, quan ja han caigut tots els maons
  // Ara el de la dreta amb els personatges anant cap als extrems
  pos_mao_anim = 0;
  numero_animacio_caracters = 0;
  for ( num_animacions = 0; num_animacions < 8; num_animacions++) {
    // Brick dreta
    HMMM(0, 256 + 128, 82 + 32*2, pos_mao_anim, 32, 32);
    HMMM(numero_animacio_caracters * 32, 256 + 44, pos_x_gat, POS_Y_MAONS + 10,
         32, 26);
    HMMM(numero_animacio_caracters * 32, 256 + 64, pos_x, POS_Y_MAONS, 32, 32);
    WAIT(TEMPS_CAIGUDA_MAONS);
    // Borrem
    HMMM(200, 256, 82 + 32*2, pos_mao_anim, 32, 16);
    HMMM(200, 256, pos_x, POS_Y_MAONS, 4, 32); // Borrem els quadrats
    HMMM(200, 256, pos_x_gat+28, POS_Y_MAONS + 10, 4, 32);
    pos_x = pos_x + 4;
    pos_x_gat = pos_x_gat - 4;
    pos_mao_anim = pos_mao_anim + 16;
    numero_animacio_caracters++;
    if (numero_animacio_caracters == 6) {
      numero_animacio_caracters = 0;
    }
  }
  // L'últim maó queda borrat, el copiem totalment de nou
  HMMM(0, 256 + 128, 82 + 32*2, pos_mao_anim, 32, 32);

  // Quan tornen amb el maó de l'esquerra cap al centre, aixafen els maons, faig un parell més d'animacions
  for ( num_animacions = 0; num_animacions < 2; num_animacions++) {
    HMMM(numero_animacio_caracters * 32, 256 + 44, pos_x_gat, POS_Y_MAONS + 10,
         32, 26);
    HMMM(numero_animacio_caracters * 32, 256 + 64, pos_x, POS_Y_MAONS, 32, 32);
    WAIT(TEMPS_CAIGUDA_MAONS);
    // Borrem
    HMMM(200, 256, pos_x, POS_Y_MAONS, 4, 32); // Borrem els quadrats
    HMMM(200, 256, pos_x_gat + 28, POS_Y_MAONS + 10, 4, 32);
    pos_x = pos_x + 4;
    pos_x_gat = pos_x_gat - 4;
    pos_mao_anim = pos_mao_anim + 16;
    numero_animacio_caracters++;
    if (numero_animacio_caracters == 6) {
      numero_animacio_caracters = 0;
    }
  }

  // El maó de l'esquerra
  pos_mao_anim = 0;
  numero_animacio_caracters = 0;
  for ( num_animacions = 0; num_animacions < 8; num_animacions++) {
    // Brick esquerra
    HMMM(0, 256 + 128, 82, pos_mao_anim, 32, 32);
    HMMM(numero_animacio_caracters * 32, 256 + 11, pos_x_gat, POS_Y_MAONS + 10,
         32, 32);
    HMMM(numero_animacio_caracters * 32, 256 + 96, pos_x, POS_Y_MAONS, 32, 32);
    WAIT(TEMPS_CAIGUDA_MAONS);
    // Borrem
    HMMM(200, 256, 82, pos_mao_anim, 32, 16);
    HMMM(200, 256, pos_x, POS_Y_MAONS, 4, 32); // Borrem els quadrats
    HMMM(200, 256, pos_x_gat, POS_Y_MAONS + 10, 4, 32);
    pos_x = pos_x - 4;
    pos_x_gat = pos_x_gat + 4;
    pos_mao_anim = pos_mao_anim + 16;
    numero_animacio_caracters++;
    if (numero_animacio_caracters == 6) {
      numero_animacio_caracters = 0;
    }
  }
  HMMM(0, 256 + 128, 82, pos_mao_anim, 32, 32);

  // El segon del mig
  pos_mao_anim = 0;
  numero_animacio_caracters = 0;
  for ( num_animacions = 0; num_animacions < 6; num_animacions++) {
    // Brick dreta
    HMMM(0, 256 + 128, 82 + 32, pos_mao_anim, 32, 32);
    HMMM(numero_animacio_caracters * 32, 256 + 44, pos_x_gat, POS_Y_MAONS + 10,
         32, 26);
    HMMM(numero_animacio_caracters * 32, 256 + 64, pos_x, POS_Y_MAONS, 32, 32);
    WAIT(TEMPS_CAIGUDA_MAONS);
    // Borrem
    HMMM(200, 256, 82 + 32, pos_mao_anim, 32, 16);
    HMMM(200, 256, pos_x, POS_Y_MAONS, 4, 32); // Borrem els quadrats
    HMMM(200, 256, pos_x_gat + 28, POS_Y_MAONS + 10, 4, 32);
    pos_x = pos_x + 4;
    pos_x_gat = pos_x_gat - 4;
    pos_mao_anim = pos_mao_anim + 16;
    numero_animacio_caracters++;
    if (numero_animacio_caracters == 6) {
      numero_animacio_caracters = 0;
    }
  }
  HMMM(0, 256 + 128, 82 + 32, pos_mao_anim, 32, 32);

  // Cartell de bricks
  HMMM(32, 256 + 128, 34, 20, 188, 8); // 28
  WAIT(2);
  HMMM(32, 256 + 136, 34, 28, 188, 10); // 28
  WAIT(2);
  HMMM(32, 256 + 146, 34, 38, 188, 10); // 28
  WAIT(2);


  char sortir = 0;
  char numero_animacio_caracters_gat = 0;
  numero_animacio_caracters = 0;
  char dreta_man = 0;
  char dreta_gat = 0;
  char i,j;
  // Scroll text i moviment personatges
  for (i = 0; i < 3; i++) {
    pos_text[i] = 0;
  }
  while (!sortir) {
    // Mirem el següent caràcter
    calcula_pos_caracter_a_copiar(text[pos_text[0]], 0);
    calcula_pos_caracter_a_copiar(text_eng[pos_text[1]], 1);
    calcula_pos_caracter_a_copiar(text_spa[pos_text[2]], 2);

    // Fem el sorteig
    dreta_man = Rnd(_Time) & 1;
    dreta_gat = Rnd(_Time) & 1;
    // Miro si puc realitzar el moviment i no em surto dels límits. Ja que cada
    // animació són 6 moviments que fan 24 pixels, però el caràcter són animacions de 8 frames que van de 2 en 2, en total 4 frames
    if (dreta_man == 1 && ((pos_x + 16) > 240)) {
      dreta_man = 0;
    } else if (dreta_man == 0 && ((pos_x - 16) < 178)) {
      dreta_man = 1;
    }

    // Miro si puc realitzar el moviment i no em surto dels límits. Ja que cada
    // animació són 6 moviments que fan 24 pixels
    if (dreta_gat == 1 && ((pos_x_gat + 16) > 64)) {
      dreta_gat = 0;
    } else if (dreta_gat == 0 && (((int)pos_x_gat) - 16) < 0) {
      dreta_gat = 1;    // !!!!!!!!!!!!!! Debugar si entra aquí
    }
    for (i = 0; i < AMPLADA_CARACTER; i = i + 2) {
      // Fem pels 3 idiomes
      HMMM(2, 170, 0, 170, 254,
           ALCADA_BANDERA*3); // No ho puc fer per 1 ja que hi havia
                            // l'arrodoniment, ha de ser parell
      for (j = 0; j < 3; j++) {
        // Desplacem text a l'esquerra
        // Afegim la part del caràcter
        HMMM(pos_caracter_a_copiar[j] + i, y_pos_caracter_a_copiar[j], 255, 170+j*ALCADA_BANDERA, 2,
             alcada_caracter_a_copiar[j]);
      }
      if (dreta_man == 0) {
        // Moviment MAN esquerra
        HMMM( numero_animacio_caracters * 32, 256 + 96, pos_x, POS_Y_MAONS, 32, 32);
        HMMM(200, 256, pos_x, POS_Y_MAONS, 4, 32); // Borrem els quadrats. No cal tot, només una part
        pos_x = pos_x - 4;
      }
      // També podria fer el dreta amb signe i sumar o resta el pos_x segons el
      // signe i ajuntar els dos bucles
      else {
        // Moviment MAN dreta
        HMMM(numero_animacio_caracters * 32, 256 + 64, pos_x, POS_Y_MAONS, 32, 32);
        HMMM(200, 256, pos_x, POS_Y_MAONS, 4, 32);
        pos_x = pos_x + 4;
      }
      if (dreta_gat == 0) {
        // Moviment Sam esquerra
        HMMM(numero_animacio_caracters_gat * 32, 256 + 44, pos_x_gat, POS_Y_MAONS + 10, 32, 26);
        HMMM(200, 256, pos_x_gat + 28, POS_Y_MAONS + 10, 4, 32); // Borrem els quadrats. No cal tot, només una part
        pos_x_gat = pos_x_gat - 4;
      }
      // També podria fer el dreta amb signe i sumar o resta el pos_x segons el
      // signe i ajuntar els dos bucles
      else {
        // Moviment Sam dreta
        HMMM(numero_animacio_caracters_gat * 32, 256 + 11, pos_x_gat, POS_Y_MAONS + 10, 32, 32);
        HMMM(200, 256, pos_x_gat, POS_Y_MAONS + 10, 4, 32);
        pos_x_gat = pos_x_gat + 4;
      }
      numero_animacio_caracters ++;
      numero_animacio_caracters_gat ++;
      if (numero_animacio_caracters == 6 ){
        numero_animacio_caracters = 0;
      }
      if (numero_animacio_caracters_gat == 6) {
        numero_animacio_caracters_gat = 0;
      }

      // Detectem la tecla per si acabem o escollim número de jugadors
      for(j=0; j<3; j++ ){
        char linia0 = GetKeyMatrix(0);
        if ((linia0 & 2) == 0) {
          nombre_jugadors = 1;
          sortir = 1;
        } else if ((linia0 & 4) == 0) {
          nombre_jugadors = 2;
          sortir = 1;
        }
        char linia6 = GetKeyMatrix(6);
        if (((linia6 & 32) == 0)) {
          // Tecla F1
          nombre_jugadors = 0;
          sortir = 1;
        }

        WAIT(1);
      }
    }

    for (j = 0; j < 3; j++) {
      pos_text[j]++;
    }
    if (pos_text[0] == 183) {
      pos_text[0]= 0;
    }
    if (pos_text[1] == 178) {
      pos_text[1] = 0;
    }
    if (pos_text[2] == 179) {
      pos_text[2] = 0;
    }
  }

  // Animació MAN / Caràcter principal i Sam / Gat aleatori
  WaitKey();
}

void main() {
  // Quan ja estigui totalment finalitzada l'animació canviaré el nom de la funció per ser cridada pel bloc principal del joc
  SetColors(15,0,0);
  animacio_pantalla_inicial();

  EndMyInterruptHandler();
  apaguemOPLL();
  Screen(0);
  Exit(0);
}

