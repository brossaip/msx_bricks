import cv2
import numpy as np
import subprocess
import threading
import sys
import os

from matplotlib.pyplot import imshow
from matplotlib import pyplot as plt
from PIL import Image, ImageDraw

################################################
# El primer argument és el nom del fitxer
# El segon argument és el número de colors diferents que es vol tenir. Pot ser 15 ó 16, depèn si es vol color transparent o no
# El tercer argument és el fitxer _labels que s'havia obtingut en una altra aproximació per si es vol tornar a executar el procés
################################################

cami_imatge = sys.argv[1]

try:
    num_colors = int(sys.argv[2])
except IndexError:
    raise IndexError("S'ha d'indicar el número de colors. Normalment 15 ó 16 per al MSX")

try:
    # Obrir el fitxer
    nuclis = np.asarray(eval(sys.argv[3]))
    labels_ant = np.reshape(nuclis, (nuclis.shape[0]*nuclis.shape[1],1))
    num_colors = len(nuclis)
except IndexError:
    print("Si vols tenir un valor inicial per la paleta de colors, escriu una llista amb els valors.\n Executant amb nuclis inicials aleatoris. Si es passa el vector, sobreescriu el número de colors per la quantitat de nuclis passats")
    nuclis = []


# Per reduir la paleta usa el mètode del K-Means
img_BGR = cv2.imread(cami_imatge)
img = cv2.cvtColor(img_BGR, cv2.COLOR_BGR2RGB)

# Potser abans de reduir la paleta hauria d'aproximar cada pixel en un dels 512 colors possibles del MSX
# Faria la inversa de quan trunco els colors
# Hauria de fer un algoritme per buscar les distàncies més curtes i aproximar-ho. Semblant al que dec fer a AproximaPaleta
# Sembla que l'openCV també té un KNearest i no cal que faci l'algoritme en pyhton https://stackoverflow.com/questions/73666119/open-cv-python-quantize-to-a-given-color-palette

Z = img.reshape((-1, 3))

Z = np.float32(Z)
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 1.0)
if len(nuclis)>0:
    ret, label, center = cv2.kmeans(Z, num_colors, nuclis, criteria, 10, cv2.KMEANS_PP_CENTERS)
else:
    ret, label, center = cv2.kmeans(Z, num_colors, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)

# Now convert back into uint8, and make original image
center = np.uint8(center)  # El center té els centres dels K-means, els 16 colors
with open(cami_imatge[:-4] + "_centres.txt","w") as f:
    f.write(str(center))
# Escric els centres per si vull conèixer la paleta utilitzada

# Escric els labels per si vull tornar a executar només un cop amb el resultat el més semblant possible
with open(cami_imatge[:-4] + "_labels.txt","w") as f:
    f.write(str(label))
# Escric els centres per si vull conèixer la paleta utilitzada

# He de passar de paleta de 256*3 a paleta de 8*3. Redueixo cada croma
center_3b = np.uint8(center / 32)
# Al reduir la paleta hi ha colors que han convergit, com el negre. Vull eliminar repeticions per així poder utilitzar una part de colors lliure
quantsdif = 0
nou_dic = {}
# He de fer amb els diferents un diccionari dels reubicats. Primer miraré tots els colors que són diferents en un array i un cop creat el diccionari els reubicaré. Així faré dic_reubicat[nou_dic[pos]]
for k in range(num_colors):
    trobat = False
    for dic in nou_dic:
        if np.array_equal(center_3b[k], center_3b[dic]):
            nou_dic[k] = dic
            trobat = True
            break
    if not trobat:
        nou_dic[k] = k
        quantsdif += 1

# Si és de 15 colors per mantenir el 0 transparent, he de buscar un lloc lliure per posar-lo en el diccionari
# No cal, el nou_dic només tindrà 14 colors, després s'ha de definir quin és el transparent que serà el 15

res = center[label.flatten()]
res2 = res.reshape((img.shape)).astype('uint8')

# Ara hem de passar la imatge a format binari, he d'afegir la capçalera de 7 bits
# Crec que només he de traduir els bits del label, té 4 bits de 0-15
with open(cami_imatge[:-3] + "sc7", "wb") as f:
    capçalera = b'\xfe\x01\x02\x03\x04\x05\x06'
    f.write(capçalera)
    labflat = label.flatten()
    for k in range(int(len(labflat) / 2)):
        valor = int(nou_dic[labflat[2 * k]] << 4) + nou_dic[labflat[2 * k + 1]]
        # valor = int(dic_reubicat[nou_dic[labflat[2 * k]]] << 4) + dic_reubicat[nou_dic[labflat[2 * k + 1]]]
        f.write(np.byte(valor))


# Em falta guardar la paleta que és el center. En aquest enllaç el guarden en un binari apart
# He intentat mirar que la paleta fos compatible amb el format pl7 del mif https://www.msx.org/forum/msx-talk/development/mifui-and-loadsc7bas-bad-file-mode, però no n'he tret el format
# El fusion-c fa el format de RGB amb array de 3 numèric. Així és com el guardaré
centflat = center_3b.flatten()
with open(cami_imatge[:-3] + "pl7", "wb") as f:
    capçalera = b'\xfe\x01\x02\x03\x04\x05\x06'
    f.write(capçalera)
    for k in range(int(len(centflat) / 2)):
        valor = (centflat[2 * k] << 4) + centflat[2 * k + 1]
        f.write(np.byte(valor))

print(f"Hem trobat {quantsdif} colors diferents")
cv2.imwrite(cami_imatge[:-4] + "_reduced.png",res2)
plt.imshow(res2)
plt.show()

