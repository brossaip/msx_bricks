from PIL import Image
import numpy as np

im = Image.open("/home/jep/MSX/FUSION-C-1.3-main/Sprites/Main1.gif")
im = im.convert('RGB') # El Gif no estava en format RGB, provo amb aquesta conversió
im_array = np.array(im)

# Havia pensat que buscaria el color en el patró, però crec que simplificant l'array dividint per 32 obtindré el mateix.
# El patró té la primera coordenada que és el verd, la filera, que va de 0 a 7. Cada filera té 64 pixels a on la primera posició és el Red i després canvia el blue de 0 a 7. Un cop hem escombrat tots els blue canviem al següent Red
# impatro = Image.open("/home/jep/MSX/FUSION-C-1.3-main/RGB_9bits_patro.png")

# Per passar-ho a sprite, he de generar els 0 i 1 i després guardar el color
# Faré un color de fondo per paràmetre i tot el color que no sigui fondo el consideraré com a 1, el fondo com a 0.
# Aquesta informació la passaré a RGB8

fons = [255,255,255]
# Amb la funció Sprite32Bytes es converteix el patró a com ha de ser el SetSpritePattern, amb l'ordre de 0-2;1-3 dels patrons de 8

# Averigüem si la imatge és de 16x16 o 8x8. Però tant és, ja que a tots dos he de crear el patró, l'únic que un passarà per Sprite32Bytes i l'altre ja serà directe
[tam_x, tam_y] = im_array.shape[0:2]

array_colors = []
fw = open("sortida_sprite.txt","w")
fw_pal = open("sortida_paletasprite.txt","w")
if tam_x == 16:
    fw.write("static const unsigned int pattern[]={\n")
else:
    fw.write("static const char pattern[]={\n")
fw_pal.write("static const char [] ={\n")
for x in range(tam_x):
    fw.write("0b")
    # print(f"Línia: {x}")
    colorLiniaTrobat = False
    for y in range(tam_y):
        # print(f"pixel: {im_array[x][y]}")
        if np.array_equal(im_array[x][y], fons):
            # print("0")
            fw.write("0")
        else:
            # print("1")
            fw.write("1")
            if not colorLiniaTrobat:
                Trobat = False
                for k in range(len(array_colors)):
                     Trobat = np.array_equal(im_array[x][y],array_colors[k])
                     if Trobat:
                         fw_pal.write(f"{k} // {im_array[x][y]/32}\n")
                         colorLiniaTrobat = True
                         break
                if not Trobat:
                    fw_pal.write(f"{len(array_colors)} // {im_array[x][y]/32}\n")
                    array_colors.append(im_array[x][y])
                    colorLiniaTrobat = True
    fw.write(",\n")
    fw_pal.write(",\n")
fw.write("};")
fw_pal.write("};")

fw.close()
fw_pal.close()
im.close()
# I ara queda convertir el color per crear el text de la funció SetSpriteColors
# És el mateix bucle que tinc en el reduce_palette que faig el diccionari però  d'array de colors a index. El faré en el bucle anterior. No he de fer cap bucle, he de detectar un color per línia i escriure la seva paleta


