#include "sc7brick.h"
#include "fusion-c/include/vdp_graph2.h"
#include "fusion-c/include/vdp_sprites.h"
#include "fusion-c/include/pattern_transform.h"

#define HALT __asm halt __endasm            // wait for the next interrupt

static FCB file;                            // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC7 (2560 * 2)
#define BUFFER_SIZE_SC5 2560

unsigned char LDbuffer[BUFFER_SIZE_SC7]; // Utilitzo el gran que ja engloba el petit
unsigned char ayFX_Bank[0x58B];

// Definim els diferents estats
#define MAIN1ESQ 0
#define MAIN1DRE 2
#define MAIN2ESQ 4
#define MAIN2DRE 6
#define MAINSALTESQ 8
#define MAINSALTDRE 10
#define MAINIMPULSESQ 12
#define MAINIMPULSDRE 14
#define BAIXANTESQ 16 // Per detectar el cantó per l'esquerre, encara no té gràfic baixant, però es pot crear mirant cap a baix
#define BAIXANTDRE 18 // Els he d'assignar un sprite, que si no no els pinta
#define SUICIDI 20

#define NUMSPRITEMAO 64
#define NUMSPRITEENERGIA 16 // Es poden definir fins a 256 sprites de 8x8 i 64 de 16x16
#define NUMSPRITEENERGIAGAT 20
#define NUMSPRITEGAT 8
#define NUMSPRITEGATCONTORN 12
#define NUMSPRITEGATTREBALL 24
#define NUMSPRITEGATCONTORNTREBALL 28
#define NUMSPRITEMAN 40 // Abans era el 0, però a la funció Pattern16FlipVram el segon paràmetre de 0 indica copiat a si mateix
#define NUMSPRITEMANCONTORN 44
#define NUMSPRITEMANTREBALL 36
#define NUMSPRITEMANCONTORNTREBALL 32

#define PLACARACTERPRIN 0
#define PLACONTORNCARPRIN 1
#define PLAMAO 8
#define PLAENERGIA 9
#define PLAGAT 2
#define PLAGATCONTORN 3
#define PLAENERGIAGAT 4

// Definicions RKeys
#define BRICKS_KEY_SPACE 1
#define BRICKS_KEY_LEFT 16
#define BRICKS_KEY_UP 32
#define BRICKS_KEY_DOWN 64
#define BRICKS_KEY_RIGHT 128
#define BRICKS_KEY_A 64    // Línia 2
#define BRICKS_KEY_D 2     // Línia 3
#define BRICKS_KEY_W 16    // Línia 5
#define BRICKS_KEY_X 32    // Línia 5
#define BRICKS_KEY_SHIFT 1 // Línia 6

// Definim els estats de les pantalles joc
#define PANT_INICIAL 1
#define PANT_JOC_1P 2
#define PANT_JOC_2P 3
#define PANT_GAMEOVER 4
#define PANT_PINTEMJOC_1P 5
#define PANT_PINTEMJOC_2P 6
#define PANT_ACABEM 0
#define PANT_FINAL 7

// Definim constants de l'animació inicial
#define TEMPS_CAIGUDA_MAONS 10
#define POS_Y_MAONS 128

// Array dels sprites que defineixen el caràcter i que definiran la posició dels
// estats
const char sprites_estats[] = {0,  4,  8,  12, 16, 20, 24, 28,
                               32, 36, 40, 44, 48, 52, 56, 60,
                               32, 36, 40, 44};

// Posició dels digits de la puntuació / score
const int pos_nums[11] = {
    60,  67,  74,  81,  88, 95,
    103, 110, 117, 123, 131}; // L'última posició és on acaba el 9
// El número 8 s'ha de tirar més a la dreta, però queda mal retallat després el 7

// Tot de zeros per borrar el marcador o per si s'han de fer altres reformes
// També podria tenir una zona de la VRAM que contingués el quadre negre per esborrar
const int vectzero[450] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //25
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //50
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //75
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //100
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //125
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //150
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //175
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //200
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //225
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //250
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //275
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //300
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //325
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //350
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //375
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //400
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //425
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //450
};

static char copsVsync = 0;
static char test = 0;
static char Point1;
static char Point2;
static char Point3;
static char debug;

__at 0xbfff char nombre_jugadors;
__at 0x8000 char pag2[0x4000];

/* Les paletes de la pantalla final que es carreguen al mòdul principal */
__at 0xbfbf char mypalette_3[16 * 4];
__at 0xbf7f char mypalette_2[16 * 4];
__at 0xbf3f char mypalette_1[16 * 4];

typedef struct {
  char pos_x;
  char pos_y;
  char estat; // És l'estat de la màquina d'estats. Abans tenia una traducció directa amb l'estat, ara tindré una altra variable per l'animació
  char index_sprite; // indicarà a partir de quin lloc ha de començar a agafar l'sprite de l'animació
  char ferFlip; // No tindré els respectius, faré un flip
  unsigned int temps_espai;
  char hedefersalt; // si el temps de pujar ha estat suficient per escalar
  char sona_efecte; // Indica si l'efecte sonor s'està generant
  char collisionat;
  char vides;
  char processant_moviment;
} caracter_principal;

caracter_principal struct_car_principal;
caracter_principal struct_car_secundari;

typedef struct {
  char pos_x;
  char pos_y;
  unsigned int temps_mao;
  char velocitat_mao;
  char continuem_generant_maons;
  char processar_pos_mao;
} estructura_mao;

estructura_mao struct_mao;
char nivell;
int puntuacio;
char temps_entre_maons = 10;
char estat_pantalla;
unsigned char linia_A;
unsigned char linia_D;
unsigned char linia_W_X;
unsigned char linia_Shift;

void WAIT(int cicles) {
  int i;
  for (i = 0; i < cicles; i++)
    HALT;
  return;
}

unsigned char OldHook[5];
unsigned char MyHook[5];
unsigned char IntFunc[5];
unsigned char TypeOfInt;
__at 0xFD9F unsigned char VdpIntHook[];
__at 0xFD9A unsigned char AllIntHook[];
__at 0xF344 unsigned char RAMAD3;

void InterruptHandlerHelper (void) __naked
{
__asm
    push af
    call _IntFunc
    pop af
    jp _OldHook
__endasm;
}

void InitializeMyInterruptHandler (int myInterruptHandlerFunction, unsigned char isVdpInterrupt)
{
    unsigned char ui;
    MyHook[0]=0xF7; //RST 30 is interslot call both with bios or dos
    MyHook[1]=RAMAD3; //Page 3 generally is not paged out and is the slot of the ram, so this should be good
    MyHook[2]=(unsigned char)((int)InterruptHandlerHelper&0xff);
    MyHook[3]=(unsigned char)(((int)InterruptHandlerHelper>>8)&0xff);
    MyHook[4]=0xC9;
    IntFunc[0]=0xCD; //CALL
    IntFunc[1]=(unsigned char)((int)myInterruptHandlerFunction&0xff);
    IntFunc[2]=(unsigned char)(((int)myInterruptHandlerFunction>>8)&0xff);
    IntFunc[3]=0xC9;
    TypeOfInt = isVdpInterrupt;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();
    if (isVdpInterrupt)
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=VdpIntHook[ui];
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=MyHook[ui];
    }
    else
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=AllIntHook[ui];
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=MyHook[ui];
    }

    //Re-enable Interrupts
    EnableInterrupt();
}

void EndMyInterruptHandler (void)
{
    unsigned char ui;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();

    if (TypeOfInt)
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=OldHook[ui];
    else
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=OldHook[ui];

    //Re-enable Interrupts
    EnableInterrupt();

}

void calcula_pos_mao(){
  if (struct_mao.temps_mao>temps_entre_maons && struct_mao.continuem_generant_maons==1) {
    struct_mao.pos_y += 8 + struct_mao.velocitat_mao;

    struct_mao.temps_mao = 0;
    test = 0;
    char posicio_16 = (struct_mao.pos_y >> 4) << 4; // Dividim entre 16 (2^4) i tornem a multiplicar per agafar el múltiple correcte
    char nou_mao = 0;
    // mirem si hi ha maó a sota i el dibuixem per tornar a començar
    if (struct_mao.pos_y > 182) {
      HMMM(2, 256, struct_mao.pos_x * 2, 171, 24, 16);
      nou_mao = 1;
    }
    else if ((Point(struct_mao.pos_x*2+7, posicio_16+16+4) !=0 ) ) {
      // He afegit l'OR perquè per certes velocitats del maó no el trobava, fent proves amb level 3 i 5
      char offset = 0;
      // El problema amb molta velocitat és que salta una filera. He de comprovar si la filera de dalt està ocupada i si és així enganxar-lo més amunt. És molt possible que hagi de mirar més d'una filera
      if (Point(struct_mao.pos_x * 2 + 7, posicio_16 + 6) != 0) {
        offset = 16;
      }
      // Deixem el maó fix
      HMMM(2, 256, struct_mao.pos_x * 2, posicio_16 - 8 + 3 - offset, 24, 16);
      nou_mao = 1;
    }
    if (nou_mao==1){
      puntuacio = puntuacio + 10;
      posa_puntuacio(puntuacio);
      // Nou maó
      struct_mao.pos_x = 34 + (rand()%18)*12; //Hi havia 19, però no cal darrere el personatge principal
      struct_mao.pos_y = 0;
      PlayFX(7);
    }

    PutSprite(PLAMAO, NUMSPRITEMAO, struct_mao.pos_x, struct_mao.pos_y+3,0);
  }
}

void main_loop(void) {
  if (copsVsync > 4) {
    copsVsync = 0;
    if (struct_car_principal.estat != SUICIDI) {
      struct_car_principal.processant_moviment = 1;
    }
  }
  if (struct_mao.processar_pos_mao == 0) {
    struct_mao.processar_pos_mao = 1;
  }
  struct_mao.temps_mao++;
  copsVsync++;
  UpdateFX(); // Actualitzem el PSG
  playPatro();
}

void posa_puntuacio(int punts) {
  // La puntuació - score
  int dig;
  int posX_score = 140;
  // Primer borrarem tota la puntuacio perquè a cops els dígits deixen rastre ja que tenen gruixos diferents
  HMMC(vectzero, 105, 198, 147 - 105, 208 - 198);

  while (punts > 0) {
    dig = punts % 10;
    posX_score = posX_score - pos_nums[dig + 1] + pos_nums[dig];
    HMMM(pos_nums[dig] - 2, 257, posX_score, 198,
         pos_nums[dig + 1] - pos_nums[dig],
         9); // He posat el -2 a pos_nums[dig]-2 per l'offset en el 2.0
    punts = punts / 10;
  }
}

void pinta_vides(char vides) {
  HMMC(vectzero, 211, 198, 24, 10);
  HMMM(pos_nums[vides]-2, 257, 211, 198, pos_nums[vides + 1] - pos_nums[vides], 9);
  // He posat el -2 a pos_nums[vides]-2 per l'offset en el 2.0
}

void pinta_numero_level() {
  HMMC(vectzero, 311, 198, 24, 10);
  HMMM(pos_nums[nivell]-2, 257, 311, 198, pos_nums[nivell + 1] - pos_nums[nivell], 9);
}

char puc_esquerra() {
  // Segons posicio nas
  // Les parets no coincideixen amb el 5, poden ser el nou
  // No he de mirar just a la cara, he de mirar els peus que són els que determinen el salt
  if (Point(struct_car_principal.pos_x * 2 + 2,
            struct_car_principal.pos_y + 13) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 4,
            struct_car_principal.pos_y + 13) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 2,
            struct_car_principal.pos_y + 13) == 9 ||
      Point(struct_car_principal.pos_x * 2 + 4,
            struct_car_principal.pos_y + 13) == 9 ||
      Point(struct_car_principal.pos_x * 2 + 2,
            struct_car_principal.pos_y + 12) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 4,
            struct_car_principal.pos_y + 12) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 2,
            struct_car_principal.pos_y + 12) == 9 ||
      Point(struct_car_principal.pos_x * 2 + 4,
            struct_car_principal.pos_y + 12) == 9) {
    return 0;
  } else {
    return 1;
  }
}

char puc_esquerra_gat() {
  //Segons posicio cua
  if (Point(struct_car_secundari.pos_x * 2 + 0,
            struct_car_secundari.pos_y + 14) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 2,
            struct_car_secundari.pos_y + 14) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 0,
            struct_car_secundari.pos_y + 14) == 9 ||
      Point(struct_car_secundari.pos_x * 2 + 2,
            struct_car_secundari.pos_y + 14) == 9 ||
      Point(struct_car_secundari.pos_x * 2 + 0,
            struct_car_secundari.pos_y + 12) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 2,
            struct_car_secundari.pos_y + 12) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 0,
            struct_car_secundari.pos_y + 12) == 9 ||
      Point(struct_car_secundari.pos_x * 2 + 2,
            struct_car_secundari.pos_y + 12) == 9) {
    return 0;
  } else {
    return 1;
  }
}

char puc_caure_esquerra() {
  // Segons punta peu dret
  if (Point(struct_car_principal.pos_x * 2 + 7,
            struct_car_principal.pos_y + 18) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 8,
            struct_car_principal.pos_y + 18) == 5) {
    return 0;
  } else {
    return 1;
  }
}

char puc_caure_esquerra_gat() {
  // Segons punta pota dreta
  if (Point(struct_car_secundari.pos_x * 2 + 6,
            struct_car_secundari.pos_y + 18) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 8,
            struct_car_secundari.pos_y + 18) == 5) {
    return 0;
  } else {
    return 1;
  }
}

char puc_dreta() {
  // Segons posicio nas
  // Les parets no coincideixen amb el 5, poden ser el nou
  // L'sprite fa uns 16 d'amplada, per 2 ja que fa el doble que els altres sprites
  // També he de mirar més d'una posició de les y perquè a cops amb el salt enganxa un forat negre
  if (Point(struct_car_principal.pos_x * 2 + 30,
            struct_car_principal.pos_y + 13) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 28,
            struct_car_principal.pos_y + 13) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 30,
            struct_car_principal.pos_y + 13) == 9 ||
      Point(struct_car_principal.pos_x * 2 + 28,
            struct_car_principal.pos_y + 13) == 9 ||
      Point(struct_car_principal.pos_x * 2 + 30,
            struct_car_principal.pos_y + 12) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 28,
            struct_car_principal.pos_y + 12) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 30,
            struct_car_principal.pos_y + 12) == 9 ||
      Point(struct_car_principal.pos_x * 2 + 28,
            struct_car_principal.pos_y + 12) == 9) {
    return 0;
    }
  else {
    return 1;
  }
}

char puc_dreta_gat() {
  // Segons posicio nas
  if (Point(struct_car_secundari.pos_x * 2 + 30+0,
            struct_car_secundari.pos_y + 13) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 29+0,
            struct_car_secundari.pos_y + 13) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 30+0,
            struct_car_secundari.pos_y + 13) == 9 ||
      Point(struct_car_secundari.pos_x * 2 + 28+0,
            struct_car_secundari.pos_y + 13) == 9 ||
      Point(struct_car_secundari.pos_x * 2 + 30+0,
            struct_car_secundari.pos_y + 12) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 28+0,
            struct_car_secundari.pos_y + 12) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 30+0,
            struct_car_secundari.pos_y + 12) == 9 ||
      Point(struct_car_secundari.pos_x * 2 + 28+0,
            struct_car_secundari.pos_y + 12) == 9) {
    return 0;
  } else {
    return 1;
  }
}

char puc_caure_dreta() {
  // Segons punta peu dret
  // He d'afinar perquè amb 20 i 21 és quan puja al maó, però quan cau cap a la dreta en el salt és una altra funció
  if (Point(struct_car_principal.pos_x * 2 + 18,
            struct_car_principal.pos_y + 16) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 17,
            struct_car_principal.pos_y + 17) == 5 ) {
    return 0;
  } else {
    return 1;
  }
}

char puc_caure_dreta_gat() {
  // Segons punta pota dreta
  if (Point(struct_car_secundari.pos_x * 2 + 18,
            struct_car_secundari.pos_y + 18) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 20,
            struct_car_secundari.pos_y + 18) == 5 ) {
    return 0;
  } else {
    return 1;
  }
}

char puc_caure_dreta_mirantEsq() {
  // En no ser simètric, no és la mateixa posició quan estic mirant a la dreta que a l'esquerra i per tant he de calcular el caure de forma diferent
  if (Point(struct_car_principal.pos_x * 2 + 25,
            struct_car_principal.pos_y + 18) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 26,
            struct_car_principal.pos_y + 18) == 5) {
    return 0;
  } else {
    return 1;
  }
}

char puc_caure_dreta_mirantEsq_gat() {
  if (Point(struct_car_secundari.pos_x * 2 + 25,
            struct_car_secundari.pos_y + 18) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 26,
            struct_car_secundari.pos_y + 18) == 5) {
    return 0;
  } else {
    return 1;
  }
}

void calcula_pos_car_principal(unsigned char tecla_apretada) {
  // !!!!!! He de controlar que si no ha fet més d'un cert temps de l'espai, més de 4 que és l'escala que tinc, no canviï d'estat i es posi en estandard
  /*************************************
   * BAixant cap a l'esquerra
   */
  // Primer de tot mirem si estem caient
  // Ho farem segons la posicio del peu que es 7 pixels dreta
  // Els nous colors del maó
  if (puc_caure_dreta_mirantEsq()==1 && puc_caure_esquerra()==1 &&
      struct_car_principal.estat == BAIXANTESQ) {
    // He posat +2 i +4 perquè hi ha el punt entre mig dels maons que és negre
    struct_car_principal.pos_y+=4; // Hi havia 16 a l'original, he posat només 4 per seguir com fa el salt
  }
  if ((puc_caure_dreta_mirantEsq() == 0 || puc_caure_esquerra() == 0) &&
      struct_car_principal.estat == BAIXANTESQ) {
    // Miro si puc continuar baixant, sinó és que ja em puc aturar
    struct_car_principal.estat = MAIN1ESQ;
    struct_car_principal.index_sprite = 0;
    struct_car_principal.ferFlip = 0;
  }
  if (puc_caure_dreta_mirantEsq()==1 && puc_caure_esquerra()==1 &&
      (struct_car_principal.estat == MAIN1ESQ ||
       struct_car_principal.estat == MAIN2ESQ)) {
    // He posat +2 i +4 perquè hi ha el punt entre mig dels maons que és negre
    struct_car_principal.pos_y += 4; // Hi havia 16 a l'original, he posat només
                                     // 4 per seguir com fa el salt
    struct_car_principal.estat = BAIXANTESQ;
    struct_car_principal.index_sprite = 4;
    struct_car_principal.ferFlip = 0;
  }

  /******************************************
   *** Moviments bàsics esquerra
   */
  if ((struct_car_principal.estat == MAIN1ESQ) &&
      (tecla_apretada == BRICKS_KEY_LEFT)) {
    struct_car_principal.estat = MAIN2ESQ;
    struct_car_principal.index_sprite = 2;
    struct_car_principal.ferFlip = 0;
    if (puc_esquerra() == 1) {
      struct_car_principal.pos_x += -1;
    }
  } else if ((struct_car_principal.estat == MAIN1ESQ) &&
             (tecla_apretada == BRICKS_KEY_RIGHT)) {
    struct_car_principal.estat = MAIN1DRE;
    struct_car_principal.index_sprite = 0;
    struct_car_principal.ferFlip = 1;
    struct_car_principal.pos_x += 1;
  } else if ((struct_car_principal.estat == MAIN2ESQ) &&
             (tecla_apretada == BRICKS_KEY_LEFT)) {
    struct_car_principal.estat = MAIN1ESQ;
    struct_car_principal.index_sprite = 0;
    struct_car_principal.ferFlip = 0;
    if (puc_esquerra() == 1) {
      struct_car_principal.pos_x += -1;
    }
  } else if ((struct_car_principal.estat == MAIN2ESQ) &&
             (tecla_apretada == BRICKS_KEY_RIGHT)) {
    struct_car_principal.estat = MAIN2DRE;
    struct_car_principal.index_sprite = 2;
    struct_car_principal.ferFlip = 1;
    struct_car_principal.pos_x += 1;
  } else if ((struct_car_principal.estat == MAIN1ESQ ||
              struct_car_principal.estat == MAIN2ESQ) &&
             (tecla_apretada == BRICKS_KEY_SPACE)) {
    struct_car_principal.estat = MAINIMPULSESQ;
    struct_car_principal.index_sprite = 6;
    struct_car_principal.ferFlip = 0;
    struct_car_principal.temps_espai = 0;
  }
  /*************************************
   * Agafant impuls
   */
  else if ((struct_car_principal.estat == MAINIMPULSESQ ||
            struct_car_principal.estat == MAINIMPULSDRE) &&
           tecla_apretada == BRICKS_KEY_SPACE) {
    // Si copsVsync = 5 temps espai compta fins a 10 a cada segons, 5 segons
    // seran 50 comptats. Serà el màxim
    if (struct_car_principal.temps_espai < 50) {
      struct_car_principal.temps_espai++;
    }
    if (struct_car_principal.temps_espai>4) {
      struct_car_principal.hedefersalt = 1;
      if (struct_car_principal.temps_espai == 5) {
        PutSprite(PLAENERGIA, NUMSPRITEENERGIA, struct_car_principal.pos_x, struct_car_principal.pos_y, 0);
        SetColorPalette(2, 0, 2, 2);
      } else if (struct_car_principal.temps_espai == 15) {
        SetColorPalette(2, 0, 4, 3);
      } else if (struct_car_principal.temps_espai == 30) {
      SetColorPalette(2, 0, 4, 5);
      } else if (struct_car_principal.temps_espai == 49) {
        SetColorPalette(2, 0, 6, 6);
      }
      if (struct_car_principal.sona_efecte == 0) {
        struct_car_principal.sona_efecte = 1;
      }
    }
  } else if ((struct_car_principal.estat == MAINIMPULSESQ) &&
             tecla_apretada != BRICKS_KEY_SPACE) {
    if (struct_car_principal.hedefersalt == 1) {
      struct_car_principal.estat = MAINSALTESQ;
      struct_car_principal.index_sprite = 4;
      struct_car_principal.ferFlip = 0;
      struct_car_principal.hedefersalt = 0;
    } else {
      struct_car_principal.estat = MAIN1DRE;
      struct_car_principal.index_sprite = 0;
      struct_car_principal.ferFlip = 1;
    }
    struct_car_principal.sona_efecte = 0;
  } else if ((struct_car_principal.estat == MAINIMPULSDRE) &&
             tecla_apretada != BRICKS_KEY_SPACE) {
    if (struct_car_principal.hedefersalt == 1) {
      struct_car_principal.estat = MAINSALTDRE;
      struct_car_principal.index_sprite = 4;
      struct_car_principal.ferFlip = 1;
      struct_car_principal.hedefersalt = 0;
    } else {
      struct_car_principal.estat = MAIN1DRE;
      struct_car_principal.index_sprite = 0;
      struct_car_principal.ferFlip = 1;
    }
    struct_car_principal.sona_efecte = 0;
  }

  /*************************************
   * Baixant cap a la dreta
   */
  if (puc_caure_dreta()==1 && puc_caure_esquerra()==1 &&
      struct_car_principal.estat == BAIXANTDRE) {
    struct_car_principal.pos_y+=4; 
  }
  if ((puc_caure_dreta()==0 || puc_caure_esquerra()==0) && struct_car_principal.estat == BAIXANTDRE ) {
    // Miro si puc continuar baixant, sinó és que ja em puc aturar
    struct_car_principal.estat = MAIN1DRE;
    struct_car_principal.index_sprite = 0;
    struct_car_principal.ferFlip = 1;
  }
  // detectem quan comencem a caure
  if (puc_caure_dreta()==1 && puc_caure_esquerra()==1 &&
      (struct_car_principal.estat == MAIN1DRE ||
       struct_car_principal.estat == MAIN2DRE)) {
    struct_car_principal.pos_y += 4; 
    struct_car_principal.estat = BAIXANTDRE;
    struct_car_principal.index_sprite = 4;
    struct_car_principal.ferFlip = 1;
  }

  /*************************************
   * Moviments bàsics dreta
   */
  else if ((struct_car_principal.estat == MAIN1DRE) &&
           (tecla_apretada == BRICKS_KEY_RIGHT)) {
    struct_car_principal.estat = MAIN2DRE;
    struct_car_principal.index_sprite = 2;
    struct_car_principal.ferFlip = 1;
    if (puc_dreta() == 1) {
      struct_car_principal.pos_x += 1;
    }
  } else if ((struct_car_principal.estat == MAIN1DRE) &&
             (tecla_apretada == BRICKS_KEY_LEFT)) {
    struct_car_principal.estat = MAIN1ESQ;
    struct_car_principal.index_sprite = 0;
    struct_car_principal.ferFlip = 0;
    struct_car_principal.pos_x -= 1;
  } else if ((struct_car_principal.estat == MAIN2DRE) &&
             (tecla_apretada == BRICKS_KEY_RIGHT)) {
    struct_car_principal.estat = MAIN1DRE;
    struct_car_principal.index_sprite = 0;
    struct_car_principal.ferFlip = 1;
    if (puc_dreta() == 1) {
      struct_car_principal.pos_x += 1;
    }
  } else if ((struct_car_principal.estat == MAIN2DRE) &&
             (tecla_apretada == BRICKS_KEY_LEFT)) {
    struct_car_principal.estat = MAIN2ESQ;
    struct_car_principal.index_sprite = 2;
    struct_car_principal.ferFlip = 0;
    struct_car_principal.pos_x -= 1;
  } else if ((struct_car_principal.estat == MAIN1DRE ||
              struct_car_principal.estat == MAIN2DRE) &&
             (tecla_apretada == BRICKS_KEY_SPACE)) {
    struct_car_principal.estat = MAINIMPULSDRE;
    struct_car_principal.index_sprite = 6;
    struct_car_principal.ferFlip = 1;
    struct_car_principal.temps_espai = 0;
  }
  /*************************************
   * saltant
   */
  else if (struct_car_principal.estat == MAINSALTESQ) {
    // Hem de fer els càlculs de les y a pujar pel canvi d'estat
    // Podrà saltar 3 maons (16px) = 48 en blocs de 4 fan 12 iteracions de 4
    // px; 50/12 = 4
    //
    PutSprite(PLAENERGIA, NUMSPRITEENERGIA, 255, 0, 0);
    if (struct_car_principal.temps_espai > 4) {
      struct_car_principal.temps_espai -= 4;
      struct_car_principal.pos_y -= 4;
    } else if (struct_car_principal.temps_espai <= 4) {
      struct_car_principal.temps_espai = 0;
      // PErquè no es quedi entremig, he de fer els càlculs de si puc saltar amb l'offset
      struct_car_principal.pos_x -=5;
      if (puc_esquerra()==1){
        struct_car_principal.estat = BAIXANTESQ;
        struct_car_principal.index_sprite = 4;
        struct_car_principal.ferFlip = 0;
        // Per borrar-lo podria posar l'sprite a 0 o enviar-lo fora la pantalla
        // que és el que faré
      } else {
        struct_car_principal.estat = MAIN1ESQ;
        struct_car_principal.index_sprite = 0;
        struct_car_principal.ferFlip = 0;
      }
      if (puc_esquerra()) {
        struct_car_principal.pos_x -= 5;
      }
      struct_car_principal.pos_x += 5;
    }
  } else if (struct_car_principal.estat == MAINSALTDRE) {
    PutSprite(PLAENERGIA, NUMSPRITEENERGIA, 255,0,0); // El borro. L'havia fet que seguís el personatge, però s'haurien d'adaptar a l'sprite del salt i no al de l'impuls
    if (struct_car_principal.temps_espai > 4) {
      struct_car_principal.pos_y -= 4;
      struct_car_principal.temps_espai -= 4;
      test = 10;
    } else if (struct_car_principal.temps_espai <= 4) {
      struct_car_principal.temps_espai = 0;
      struct_car_principal.pos_x -= 12; // No és el mateix quan estic saltan per veure a on caic que quan estic caminant i vull veure a on vaig, per això faig aquest offset
      if (puc_caure_dreta() == 1 && puc_caure_esquerra()==1) {
        struct_car_principal.estat = BAIXANTDRE;
        struct_car_principal.index_sprite = 4;
        struct_car_principal.ferFlip = 1;
      } else {
        struct_car_principal.estat = MAIN1DRE;
        struct_car_principal.index_sprite = 0;
        struct_car_principal.ferFlip = 1;
      }
      struct_car_principal.pos_x += 12;
      if (puc_dreta() == 1) {
        struct_car_principal.pos_x += 5;
      }
    }
  }

  /*************************************
   * Dibuixem sprite
   */
  SetSpritePattern(NUMSPRITEMANTREBALL,
                   Sprite32Bytes(&man_Joe[struct_car_principal.index_sprite*16]), 32);
  SetSpritePattern(
      NUMSPRITEMANCONTORNTREBALL,
      Sprite32Bytes(&man_Joe[(struct_car_principal.index_sprite+1) * 16]), 32);

  if (struct_car_principal.ferFlip==1) {
    Pattern16FlipVram(NUMSPRITEMANTREBALL, NUMSPRITEMAN, 0);
    Pattern16FlipVram(NUMSPRITEMANCONTORNTREBALL, NUMSPRITEMANCONTORN, 0);
  } else {
    SetSpritePattern(
        NUMSPRITEMAN,
        Sprite32Bytes(&man_Joe[struct_car_principal.index_sprite * 16]), 32);
    SetSpritePattern(
        NUMSPRITEMANCONTORN,
        Sprite32Bytes(&man_Joe[(struct_car_principal.index_sprite + 1) * 16]),
        32);
  }

  SetSpriteColors(PLACARACTERPRIN,
                  &paleta_patrons[struct_car_principal.index_sprite*16]);
  SetSpriteColors(PLACONTORNCARPRIN,
                  &paleta_patrons[(struct_car_principal.index_sprite+1) * 16]);
  PutSprite(PLACARACTERPRIN, NUMSPRITEMAN,
            struct_car_principal.pos_x, struct_car_principal.pos_y, 0);
  PutSprite(PLACONTORNCARPRIN, NUMSPRITEMANCONTORN,
            struct_car_principal.pos_x, struct_car_principal.pos_y, 0);
  
  // Si l'Sprite arriba a dalt de tot faig la seqüència del suïcidi per tornar a
  // començar
  if (struct_car_principal.pos_y < 12) {
    struct_car_principal.estat = SUICIDI;
    suicidi();
  }
}

void calcula_pos_car_secundari() {
  // Llegim les tecles
  if (estat_pantalla == PANT_JOC_2P) {
    linia_A = GetKeyMatrix(2);
    linia_D = GetKeyMatrix(3);
    linia_W_X = GetKeyMatrix(5);
    linia_Shift = GetKeyMatrix(6);
  } else {
    linia_A = 255;
    linia_D = 255;
    linia_W_X = 255;
    linia_Shift = 255;
  }
  
  /*************************************
   * BAixant cap a l'esquerra
   */
  // Primer de tot mirem si estem caient
  if (puc_caure_dreta_mirantEsq_gat()==1 && puc_caure_esquerra_gat()==1 &&
      struct_car_secundari.estat == BAIXANTESQ) {
    struct_car_secundari.pos_y+=4; 
  }
  if ((puc_caure_dreta_mirantEsq_gat() == 0 || puc_caure_esquerra_gat() == 0) &&
      struct_car_secundari.estat == BAIXANTESQ) {
    // Miro si puc continuar baixant, sinó és que ja em puc aturar
    struct_car_secundari.estat = MAIN1ESQ;
    struct_car_secundari.index_sprite = 0;
    struct_car_secundari.ferFlip = 1;
  }
  if (puc_caure_dreta_mirantEsq_gat()==1 && puc_caure_esquerra_gat()==1 &&
      (struct_car_secundari.estat == MAIN1ESQ ||
       struct_car_secundari.estat == MAIN2ESQ)) {
    // He posat +2 i +4 perquè hi ha el punt entre mig dels maons que és negre
    struct_car_secundari.pos_y += 4; // Hi havia 16 a l'original, he posat només
                                     // 4 per seguir com fa el salt
    struct_car_secundari.estat = BAIXANTESQ;
    struct_car_secundari.index_sprite = 4;
    struct_car_secundari.ferFlip = 1;
  }

  /******************************************
   *** Moviments bàsics esquerra
   */
  if ((struct_car_secundari.estat == MAIN1ESQ) &&
      ((linia_A & BRICKS_KEY_A) == 0)) {
    struct_car_secundari.estat = MAIN2ESQ;
    struct_car_secundari.index_sprite = 2;
    struct_car_secundari.ferFlip = 1;
    // L'estat ja m'hauria d'indicar quina animació he d'agafar
    // i fer el PutSprite al final de tot comú a tothom
    if (puc_esquerra_gat() == 1) {
      struct_car_secundari.pos_x += -1;
    }
  } else if ((struct_car_secundari.estat == MAIN1ESQ) &&
             ((linia_D & BRICKS_KEY_D) == 0)) {
    struct_car_secundari.estat = MAIN1DRE;
    struct_car_secundari.index_sprite = 0;
    struct_car_secundari.ferFlip = 0;
    struct_car_secundari.pos_x += 1;
  } else if ((struct_car_secundari.estat == MAIN2ESQ) &&
             ((linia_A & BRICKS_KEY_A) == 0)) {
    struct_car_secundari.estat = MAIN1ESQ;
    struct_car_secundari.index_sprite = 0;
    struct_car_secundari.ferFlip = 1;
    if (puc_esquerra_gat() == 1) {
      struct_car_secundari.pos_x += -1;
    }
  } else if ((struct_car_secundari.estat == MAIN2ESQ) &&
             ((linia_D & BRICKS_KEY_D) == 0)) {
    struct_car_secundari.estat = MAIN2DRE;
    struct_car_secundari.index_sprite = 2;
    struct_car_secundari.ferFlip = 0;
    struct_car_secundari.pos_x += 1;
  } else if ((struct_car_secundari.estat == MAIN1ESQ ||
              struct_car_secundari.estat == MAIN2ESQ) &&
             ((linia_Shift & BRICKS_KEY_SHIFT)==0)) {
    struct_car_secundari.estat = MAINIMPULSESQ;
    struct_car_secundari.index_sprite = 6;
    struct_car_secundari.ferFlip = 1;
    struct_car_secundari.temps_espai = 0;
  }
  /*************************************
   * Agafant impuls
   */
  else if ((struct_car_secundari.estat == MAINIMPULSESQ ||
            struct_car_secundari.estat == MAINIMPULSDRE) &&
           (linia_Shift & BRICKS_KEY_SHIFT) == 0) {
    // Si copsVsync = 5 temps espai compta fins a 10 a cada segons, 5 segons
    // seran 50 comptats. Serà el màxim
    if (struct_car_secundari.temps_espai < 50) {
      struct_car_secundari.temps_espai++;
    }
    if (struct_car_secundari.temps_espai>4) {
      struct_car_secundari.hedefersalt = 1;
      if (struct_car_secundari.temps_espai == 5) {
        PutSprite(PLAENERGIAGAT, NUMSPRITEENERGIAGAT, struct_car_secundari.pos_x, struct_car_secundari.pos_y, 0);
        SetColorPalette(6, 0, 2, 2);
      } else if (struct_car_secundari.temps_espai == 15) {
        SetColorPalette(6, 0, 4, 3);
      } else if (struct_car_secundari.temps_espai == 30) {
        SetColorPalette(6, 0, 4, 5);
      } else if (struct_car_secundari.temps_espai == 49) {
        SetColorPalette(6, 0, 6, 6);
      }
      if (struct_car_secundari.sona_efecte == 0) {
        struct_car_secundari.sona_efecte = 1;
      }
    }
  } else if ((struct_car_secundari.estat == MAINIMPULSESQ) &&
             (linia_Shift & BRICKS_KEY_SHIFT) == 1) {
    if (struct_car_secundari.hedefersalt == 1) {
      struct_car_secundari.estat = MAINSALTESQ;
      struct_car_secundari.index_sprite = 4;
      struct_car_secundari.ferFlip = 1;
      struct_car_secundari.hedefersalt = 0;
    } else {
      struct_car_secundari.estat = MAIN1DRE;
      struct_car_secundari.index_sprite = 0;
      struct_car_secundari.ferFlip = 0;
    }
    struct_car_secundari.sona_efecte = 0;
  } else if ((struct_car_secundari.estat == MAINIMPULSDRE) &&
             (linia_Shift & BRICKS_KEY_SHIFT) == 1) {
    if (struct_car_secundari.hedefersalt == 1) {
      struct_car_secundari.estat = MAINSALTDRE;
      struct_car_secundari.index_sprite = 4;
      struct_car_secundari.ferFlip = 0;
      struct_car_secundari.hedefersalt = 0;
    } else {
      struct_car_secundari.estat = MAIN1DRE;
      struct_car_secundari.index_sprite = 0;
      struct_car_secundari.ferFlip = 0;
    }
    struct_car_secundari.sona_efecte = 0;
  }

  /*************************************
   * Baixant cap a la dreta
   */
  if (puc_caure_dreta_gat()==1 && puc_caure_esquerra_gat()==1 &&
      struct_car_secundari.estat == BAIXANTDRE) {
    struct_car_secundari.pos_y+=4; 
  }
  if ((puc_caure_dreta_gat()==0 || puc_caure_esquerra_gat()==0) && struct_car_secundari.estat == BAIXANTDRE ) {
    // Miro si puc continuar baixant, sinó és que ja em puc aturar
    struct_car_secundari.estat = MAIN1DRE;
    struct_car_secundari.index_sprite = 0;
    struct_car_secundari.ferFlip = 0;
  }
  // detectem quan comencem a caure
  if (puc_caure_dreta_gat()==1 && puc_caure_esquerra_gat()==1 &&
      (struct_car_secundari.estat == MAIN1DRE ||
       struct_car_secundari.estat == MAIN2DRE)) {
    struct_car_secundari.pos_y += 4; 
    struct_car_secundari.estat = BAIXANTDRE;
    struct_car_secundari.index_sprite = 4;
    struct_car_secundari.ferFlip = 0;
  }

  /*************************************
   * Moviments bàsics dreta
   */
  else if ((struct_car_secundari.estat == MAIN1DRE) &&
           ((linia_D & BRICKS_KEY_D) == 0)) {
    struct_car_secundari.estat = MAIN2DRE;
    struct_car_secundari.index_sprite = 2;
    struct_car_secundari.ferFlip = 0;
    if (puc_dreta_gat() == 1) {
      struct_car_secundari.pos_x += 1;
    }
  } else if ((struct_car_secundari.estat == MAIN1DRE) &&
             ((linia_A & BRICKS_KEY_A) == 0)) {
    struct_car_secundari.estat = MAIN1ESQ;
    struct_car_secundari.index_sprite = 0;
    struct_car_secundari.ferFlip = 1;
    struct_car_secundari.pos_x -= 1;
  } else if ((struct_car_secundari.estat == MAIN2DRE) &&
             ((linia_D & BRICKS_KEY_D) == 0)) {
    struct_car_secundari.estat = MAIN1DRE;
    struct_car_secundari.index_sprite = 0;
    struct_car_secundari.ferFlip = 0;
    if (puc_dreta_gat() == 1) {
      struct_car_secundari.pos_x += 1;
    }
  } else if ((struct_car_secundari.estat == MAIN2DRE) &&
             ((linia_A & BRICKS_KEY_A) == 0)) {
    struct_car_secundari.estat = MAIN2ESQ;
    struct_car_secundari.index_sprite = 2;
    struct_car_secundari.ferFlip = 1;
    struct_car_secundari.pos_x -= 1;
  } else if ((struct_car_secundari.estat == MAIN1DRE ||
              struct_car_secundari.estat == MAIN2DRE) &&
             ((linia_Shift & BRICKS_KEY_SHIFT) == 0)) {
    struct_car_secundari.estat = MAINIMPULSDRE;
    struct_car_secundari.index_sprite = 6;
    struct_car_secundari.ferFlip = 0;
    struct_car_secundari.temps_espai = 0;
  }
  /*************************************
   * saltant
   */
  else if (struct_car_secundari.estat == MAINSALTESQ) {
    // Hem de fer els càlculs de les y a pujar pel canvi d'estat
    // Podrà saltar 3 maons (16px) = 48 en blocs de 4 fan 12 iteracions de 4
    // px; 50/12 = 4
    //
    PutSprite(PLAENERGIAGAT, NUMSPRITEENERGIAGAT, 255, 0, 0);
    if (struct_car_secundari.temps_espai > 4) {
      struct_car_secundari.temps_espai -= 4;
      struct_car_secundari.pos_y -= 4;
    } else if (struct_car_secundari.temps_espai <= 4) {
      struct_car_secundari.temps_espai = 0;
      if (puc_esquerra_gat()==1){
        struct_car_secundari.estat = BAIXANTESQ;
        struct_car_secundari.index_sprite = 4;
        struct_car_secundari.ferFlip = 1;
        // Per borrar-lo podria posar l'sprite a 0 o enviar-lo fora la pantalla
        // que és el que faré
      } else {
        struct_car_secundari.estat = MAIN1ESQ;
        struct_car_secundari.index_sprite = 0;
        struct_car_secundari.ferFlip = 1;
      }
      if (puc_esquerra_gat()) {
        struct_car_secundari.pos_x -= 5;
      }
    }
  } else if (struct_car_secundari.estat == MAINSALTDRE) {
    PutSprite(PLAENERGIAGAT, NUMSPRITEENERGIAGAT, 255,0,0); // El borro. L'havia fet que seguís el personatge, però s'haurien d'adaptar a l'sprite del salt i no al de l'impuls
    if (struct_car_secundari.temps_espai > 4) {
      struct_car_secundari.pos_y -= 4;
      struct_car_secundari.temps_espai -= 4;
      test = 10;
    } else if (struct_car_secundari.temps_espai <= 4) {
      struct_car_secundari.temps_espai = 0;
      struct_car_secundari.pos_x -= 12; // No és el mateix quan estic saltant per veure a on caic que quan estic caminant i vull veure a on vaig, per això faig aquest offset
      if (puc_caure_dreta_gat() == 1 && puc_caure_esquerra_gat()==1) {
        struct_car_secundari.estat = BAIXANTDRE;
        struct_car_secundari.index_sprite = 4;
        struct_car_secundari.ferFlip = 0;
      } else {
        struct_car_secundari.estat = MAIN1DRE;
        struct_car_secundari.index_sprite = 0;
        struct_car_secundari.ferFlip = 0;
      }
      struct_car_secundari.pos_x += 12;
      if (puc_dreta_gat() == 1) {
        struct_car_secundari.pos_x += 5;
      }
    }
  }

  /*************************************
   * Dibuixem sprite
   */
  SetSpritePattern(NUMSPRITEGATTREBALL,
                   Sprite32Bytes(&gat_Sam[struct_car_secundari.index_sprite*16]), 32);
  SetSpritePattern(NUMSPRITEGATCONTORNTREBALL,
                   Sprite32Bytes(&gat_Sam[(struct_car_secundari.index_sprite+1) * 16]), 32);

  // Fa pampallugues, tindré un num sprite per fer la còpia abans del flip i després els canviaré
  if (struct_car_secundari.ferFlip == 1) {
    Pattern16FlipVram(NUMSPRITEGATTREBALL, NUMSPRITEGAT, 0);
    Pattern16FlipVram(NUMSPRITEGATCONTORNTREBALL, NUMSPRITEGATCONTORN, 0);
  } else {
    SetSpritePattern(
        NUMSPRITEGAT,
        Sprite32Bytes(&gat_Sam[struct_car_secundari.index_sprite * 16]), 32);
    SetSpritePattern(
        NUMSPRITEGATCONTORN,
        Sprite32Bytes(&gat_Sam[(struct_car_secundari.index_sprite + 1) * 16]),
        32);
    // He fet un spritePattern de memòria, provaré movent els bytes del
    // NUMSPRITEGATTREBALL a NUMSPRITEGAT de la VRAM Segons el MSX2 Technical
    // Handbook: F000H - F7FFH	-->	Sprite generator table
    // Els moviments de HMMM van per : 0x10000 bytes entre 512*256=0x20000 Cada coordenada és mig byte
    // F000 / 512 * 2 = 240 . A la línia 240 hi ha l'sprite0, al punt (2,240) hi ha el segon byte de l'sprite
    // 512 / 2 / 8 = 24 sprites a la primera línia arriba al F100. Això farien 172 sprites i crec que el límit eren 64. Hi deu haver algun error en el càlcul.
    // Potser hi ha una comanda que mou bytes de VRAM a VRAM, sense les funcions de rectangle del HMMM
    // Bé, de moment ho deixem així
  }

  PutSprite(PLAGAT, NUMSPRITEGAT,
            struct_car_secundari.pos_x, struct_car_secundari.pos_y, 0);
  PutSprite(PLAGATCONTORN, NUMSPRITEGATCONTORN,
            struct_car_secundari.pos_x, struct_car_secundari.pos_y, 0);
}

void init_sprites(void) {
  SpriteReset();
  SpriteSmall();
  Sprite16();
  SpriteOn();

  // He descobert que hi havia una comanda en el Fusion que és Pattern16FlipRam que gira a la dreta o a dalt i a baix un sprite. Així no se'n necessiten tants com els que jo he utilitzat

  /* SetSpritePattern(NUMSPRITEMAN, Sprite32Bytes(&man_Joe[0]), 32); // 0 */
  /* SetSpritePattern(NUMSPRITEMANCONTORN, Sprite32Bytes(&man_Joe[16]), 32); // 1 */

  SetSpritePattern(NUMSPRITEMAO, Sprite32Bytes(mao), 32);
  SetSpriteColors(PLAMAO, paleta_mao);

  SetSpritePattern(NUMSPRITEENERGIA, Sprite32Bytes(energia), 32);
  SetSpriteColors(PLAENERGIA, paleta_energia);
  SetSpritePattern(NUMSPRITEENERGIAGAT, Sprite32Bytes(energia_Sam), 32);
  SetSpriteColors(PLAENERGIAGAT, paleta_energia_Sam);

  /* SetSpritePattern(NUMSPRITEGAT, Sprite32Bytes(&gat_Sam[0]), 32); */
  /* SetSpritePattern(NUMSPRITEGATCONTORN, Sprite32Bytes(&gat_Sam[16]), 32); */

  SetSpriteColors(PLAGAT, LineColorsLayer1);
  SetSpriteColors(PLAGATCONTORN, LineColorsLayer2);

  // Els colors dels sprites només indiquen l'índex, la tonalitat es fa amb
  // el SetColorPalette
  SetColorPalette(11, 7, 6, 2); // Joe
  SetColorPalette(12, 7, 3, 3); // Joe
  SetColorPalette(13, 0, 6, 1); // Joe
  SetColorPalette(10, 1, 4, 4); // Joe
  SetColorPalette(4, 0, 0, 0);  // Joe
  SetColorPalette(15, 1, 1, 1); // Joe
  SetColorPalette(2, 5, 5, 0);  // Energia
  SetColorPalette(6, 5, 5, 0);  // Energia Sam
  SetColorPalette(3, 2, 2, 6);  // Sam
  SetColorPalette(14, 5, 5, 5); // Sam
}

void FT_SetName( FCB *p_fcb, const char *p_name )  // Routine servant à vérifier le format du nom de fichier
{
  char i, j;
  memset( p_fcb, 0, sizeof(FCB) );
  for( i = 0; i < 11; i++ ) {
    p_fcb->name[i] = ' ';
  }
  for( i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++ ) {
    p_fcb->name[i] =  p_name[i];
  }
  if( p_name[i] == '.' ) {
    i++;
    for( j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.'); j++ ) {
      p_fcb->ext[j] =  p_name[i + j] ;
    }
  }
}


void FT_errorHandler(char n, char *name)            // Gère les erreurs
{
  Screen(0);
  SetColors(15,6,6);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}
 
int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer, unsigned int amplada_linia, unsigned int tamany_buffer)        // Charge les données d'un fichiers
    {
      // El tamany de tamany_buffer és el nombre de bytes que fan les 20 linies. En screen 5 és 2560, però en screen 7 és 2560*2
        int rd=2560;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file  
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, tamany_buffer );  // Read 20 lines of image data (128bytes per line in screen5)
             HMMC(buffer, 0,start_Y,amplada_linia,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

return(1);
}
int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{
  // si el tipus és 0 carrega l'antic format de les imatges que tenia 16 colors,
  // Si és 1 és el nou format que té 15 colors, ja que el 0 és transparent
  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

int FT_openFile(char *file_name) {
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }
}

void init_pantalla_joc() {
  FT_LoadSc5Image("imatge.sc7", 256, LDbuffer,512,BUFFER_SIZE_SC7); // On charge l'umage
  FT_LoadPalette("image.pl7", LDbuffer, mypalette_1);
  SetPalette((Palette *)mypalette_1);

  // Copiem la pantalla de joc
  HMMM(0, 256, 0, 0, 48, 212); // és fins a 212 de les y, altrament esborrem
                               // dades de sprites. Sense overscan és 192
  HMMM(49, 256 + 188, 49, 188, 496 - 48, 212 - 188);
  HMMM(497, 256, 497, 0, 511 - 497, 212);

  // L'error de la primera vegada és que com que només copio els laterals, el color del fondo de la resta de pantalla és el 4 i no el 0 que es posa després. Hauré de fer que tot sigui 0
  // Posem les vides i el marcador a 0
  puntuacio = 0;
  posa_puntuacio(puntuacio);
  struct_car_principal.vides = 5;
  pinta_vides(struct_car_principal.vides);
  nivell = 1;
  pinta_numero_level();
}

void esborrar_pantalla_joc() {
  // Anirem copiant els zeros que tinc
  // Hem de borrar des de la casella (49,0) a la (498,187)
  // El vector de zeros per borrar té 450 caràcters, aniré borrant de línia en linia
  for (int k=0; k<188; k++) {
    // HMMC(vectzero, 49, k, 449,1);
    // Copiant de memòria és lent, vaig a provar de VDP a VDP
    HMMM(49,256+186,49,k,449,1);
    // Molt més ràpid. Va perfecte
  }
}

void missatge_level(){
  EndMyInterruptHandler();
  apaguemOPLL();
  // L
  HMMM(215, 256 + 70, 154,40, 43, 52);
  // E
  HMMM(156, 256 + 135, 154 + 50, 40, 47, 52);
  // V
  HMMM(104, 256 + 135, 154 + 100, 40, 50, 52);
  // E
  HMMM(156, 256 + 135, 154 + 150, 40, 47, 52);
  // L
  HMMM(215, 256 + 70, 154+200,40, 43, 52);
 
  // Número de nivell
  int amplada_numero;
  if (nivell==1){
    amplada_numero=36;
  }
  else if (nivell==4){
    amplada_numero = 36;
  }
  else {
    amplada_numero = 40;
  }
  HMMM(290 + (nivell-2)*40, 256 + 70, 154+100, 100, amplada_numero, 52);

  WAIT(100);

  // Borrem el cartell de Level
  // L
  HMMM(450, 256 + 70, 154,40, 43, 52);
  // E
  HMMM(450, 256 + 70, 154 + 50, 40, 47, 52);
  // V
  HMMM(446, 256 + 79, 154 + 100, 40, 52, 52);
  // E
  HMMM(450, 256 + 70, 154 + 150, 40, 47, 52);
  // L
  HMMM(450, 256 + 70, 154 + 200, 40, 43, 52);
  // Borrem número
  HMMM(450, 256 + 70, 154 + 100, 100, 43, 52);

  InicialitzemCanalsReproductor();
  InitializeMyInterruptHandler((int)main_loop, 1);
}


void canvi_nivell() {
  // Esborrem pantalla
  esborrar_pantalla_joc();
  // Esborrem sprites
  // Esborrem sprites de les ales
  PutSprite(PLAENERGIA, NUMSPRITEENERGIA, 255, 0, 0);
  // Maó del mateix color que el nivell Pintem nova pantalla segons el nivell a
  // on estem
  nivell++;
  // Missatge de nivell resolt o animació maó cau al mig i es tornen a separar.
  missatge_level();
  pinta_numero_level();
  // No augmento la velocitat dels maons, ja està ajustada, sinó semblava molt aleatori, no hi havia habilitat per esquivar el maó
  // struct_mao.velocitat_mao += 2;
  //  Augmentem velocitat de caiguda dels maons
  //  temps_entre_maons--;
  if (nivell == 2) {
    // Canviem paleta
    SetColorPalette(9, 5, 2, 5);
    // Dibuixem torra al mig
    HMMM(2, 256, 212, 171, 24, 16);
    HMMM(2, 256, 212+24, 171, 24, 16);
    HMMM(2, 256, 212+24*2, 171, 24, 16);
    HMMM(2, 256, 212+24, 171-16, 24, 16);
    // Augmentem velocitat maons
  } else if (nivell == 3) {
    // Canviem paleta
    SetColorPalette(5, 3, 1, 4);
    // Dibuixem 2 torres al mig
    HMMM(2, 256, 212 - 3 * 24, 171, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24, 171, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24 * 2, 171, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24, 171 - 16, 24, 16);
    //
    HMMM(2, 256, 212 + 3 * 24, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24 * 2, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171 - 16, 24, 16);
  } else if (nivell == 4) {
    // Canviem paleta
    SetColorPalette(9, 4, 2, 3);
    // Dibuixem 1 torra al mig més alta que al nivell 2
    // Filera d'abaix
    HMMM(2, 256, 212 - 3 * 24, 171, 24, 16); 
    HMMM(2, 256, 212 - 3 * 24 + 24, 171, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24 * 2, 171, 24, 16);
    HMMM(2, 256, 212, 171, 24, 16);
    HMMM(2, 256, 212 + 24, 171, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24 * 2, 171, 24, 16);
    // Filera de dalt
    HMMM(2, 256, 212 - 3 * 24 + 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24 * 2, 171 - 16, 24, 16);
    HMMM(2, 256, 212, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 3 * 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171 - 16, 24, 16);
    // Filera de més amunt
    HMMM(2, 256, 212 - 3 * 24 + 24 * 2, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212 + 24, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212 + 3 * 24, 171 - 16*2, 24, 16);
    // Filera de més a més amunt
    HMMM(2, 256, 212, 171 - 16 * 3, 24, 16);
    HMMM(2, 256, 212 + 24, 171 - 16 * 3, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171 - 16 * 3, 24, 16);
    // Filera de més a més a més amunt
    HMMM(2, 256, 212 + 24, 171 - 16 * 4, 24, 16);
  } else if (nivell == 5) {
    // Canviem paleta
    SetColorPalette(5, 4, 2, 4);
    // Columnes de 2 tot recte amb altres de 2 columnes esporàdiques
    // plataforma d'alçada 2
    // Filera d'abaix
    HMMM(2, 256, 212 - 3 * 24, 171, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24, 171, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24 * 2, 171, 24, 16);
    HMMM(2, 256, 212, 171, 24, 16);
    HMMM(2, 256, 212 + 24, 171, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24 * 2, 171, 24, 16);
    // 2a filera
    HMMM(2, 256, 212 - 3 * 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24 * 2, 171 - 16, 24, 16);
    HMMM(2, 256, 212, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 3 * 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24 * 2, 171 - 16, 24, 16);
    // 3a filera
    HMMM(2, 256, 212 - 3 * 24 + 24, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171 - 16*2, 24, 16);
    // 4a filera
    HMMM(2, 256, 212 - 3 * 24 + 24, 171 - 16 * 3, 24, 16);
    HMMM(2, 256, 212, 171 - 16 * 3, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171 - 16 * 3, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171 - 16 * 3, 24, 16);
  }
  // Col·loquem personatges
  struct_car_principal.pos_x = (char)234;
  struct_car_principal.pos_y = (char)(16 * 10 - 1);
  struct_car_principal.estat = MAIN1ESQ;
  struct_car_principal.index_sprite = 0;
  struct_car_principal.ferFlip = 0;
  struct_car_principal.hedefersalt = 0;
  struct_car_principal.sona_efecte = 0;
  struct_car_principal.collisionat = 0;
  struct_car_secundari.pos_x = 28;
  struct_car_secundari.pos_y = (char)(16 * 11 - 5);
  struct_car_secundari.estat = MAIN1ESQ;
  struct_car_secundari.index_sprite = 0;
  struct_car_secundari.ferFlip = 0;

  // Actualitzem la informació del level
  pinta_numero_level();
  // Tornem a posar el maó a l'alçada 0
  struct_mao.pos_y = 0;
}

void animacioFinal(){
  // El tenia posat com a estat de pantalla, però això no s'executa. He de fer que es comporti com el gameOver
     // Carreguem cançó
      Screen(0);
      SetColors(6, 12, 12);
      Screen(5);

      SpriteSmall();
      Sprite8();

      apaguemOPLL();
      EndMyInterruptHandler();

      // Carreguem el binari del bloc de pantalla inicial
      FT_openFile("PantFin.bMo");
      FcbRead(&file, pag2, 0x4000);

      // Carreguem imatges a tota la VRAM
      // !!!!!!!!!!! Aquí es queda penjat !!!!!!!
      // !!!!! Provar que funcioni individualment !!!!
      FT_LoadSc5Image("Final_1.sc7", 256, LDbuffer, 512,
                      BUFFER_SIZE_SC5); // On charge l'umage
      FT_LoadPalette("Final_1.pl7", LDbuffer, mypalette_1);
      FT_LoadSc5Image("Final_2.sc7", 512, LDbuffer, 512,
                      BUFFER_SIZE_SC5); // On charge l'umage
      FT_LoadPalette("Final_2.pl7", LDbuffer, mypalette_2);
      FT_LoadSc5Image("Final_3.sc7", 768, LDbuffer, 512,
                      BUFFER_SIZE_SC5); // On charge l'umage
      FT_LoadPalette("Final_3.pl7", LDbuffer, mypalette_3);

      // Música
      FT_openFile("LEVEL.MBM");
      fcb_read(&file, songFile, sizeof(songFile));
      ompleCapcaleraPatrons();
      InicialitzemCanalsReproductor();
      tempo = capcalera.start_tempo;

      InitializeMyInterruptHandler((int)main_loop, 1);

      SpriteOn();

      SetTransparent(1); // Disable color 0 Transparent mode
      // Cridem la pantalla animació final _CallMain_asm
      __asm call #0x8ea6 __endasm;
      SetTransparent(0); // Enable color 0 to Transparent mode

      apaguemOPLL();
      EndMyInterruptHandler();

      estat_pantalla = PANT_INICIAL;
}

void gameOver() {
  // Borrar sprites
  SpriteOff();
  struct_mao.continuem_generant_maons=0;
  EndMyInterruptHandler();

  // Posar la pantalla en negre. Copiarem el bloc en negre a la VRAM i
  // després copiarem de VRAM a VRAM
  HMMC(vectzero, 0, 0, 16, 16);
  for(int k=1;k<32;k++){
    HMMM(0,0,k*16,0,16,16);
  }
  for (int kk=1;kk<14;kk++){
    for (int k = 0; k < 32; k++) {
      HMMM(0, 0, k * 16, kk*16, 16, 16);
    }
  }

  // Canviar la música. Potser podria fer un fade out dels canals
  // Per fer un fade és al registre 30, però necessito saber quin instrument està
  // sonant
  apaguemOPLL();
  FT_openFile("badend1.mbm");
  fcb_read(&file, songFile, sizeof(songFile));
  ompleCapcaleraPatrons();
  InicialitzemCanalsReproductor();
  tempo = capcalera.start_tempo;

  InitializeMyInterruptHandler((int)main_loop, 1);
  // Copiar els caràcters de Game Over
  // Els caràcters van del 289-86, fan 203 d'amplada, 512-203=309px espai lliure, entre dos fan 154
  // GAM
  HMMM(52, 256 + 70, 154, 40, 162, 55);
  // E
  HMMM(156, 256 + 135, 154 + 162, 40, 47, 52);
  // OVER
  HMMM(52, 256 + 129, 154, 40+55, 208, 55);

  /* // Esperar un cert temps i/o esperar una tecla */
  WAIT(300);
  KillKeyBuffer();
  WaitKey();
  //Tornem a pantalla inicial
  // Parem joc
  EndMyInterruptHandler();
  apaguemOPLL();
  //StopFX(); // Fa aturar tot el banc d'efectes, després ja no sona
  for (int i = 8; i < 11; i++) PSGwrite(i, 0);  // Silencia el PSG, semblant al SilencePSG però no em compilava
  RestorePalette();
  estat_pantalla = PANT_INICIAL;
}

void suicidi(void) {
  // Animació de suïcidi i efecte sonor
  // Copiem les dades dels dos sprites a on toquen.
  // Provaré sobreescrivint el primer patró
  if ((struct_car_principal.estat == MAIN1ESQ) ||
      (struct_car_principal.estat == MAIN2ESQ) ||
      (struct_car_principal.estat == MAINSALTESQ) ||
      (struct_car_principal.estat == MAINIMPULSESQ) ||
      (struct_car_principal.estat == BAIXANTESQ)) {
    SetSpritePattern(NUMSPRITEMAN, Sprite32Bytes(Mort_a), 32);
    SetSpritePattern(NUMSPRITEMANCONTORN, Sprite32Bytes(Mort_b), 32);
  }
  else {
    // Si estàvem a estat mirant dreta hem de girar les dades i canviar els
    // patrons
    SetSpritePattern(NUMSPRITEMAN, Sprite32Bytes(Mort_a), 32);
    SetSpritePattern(NUMSPRITEMANCONTORN, Sprite32Bytes(Mort_b), 32);
    Pattern16FlipVram(NUMSPRITEMAN, NUMSPRITEMAN, 0);
    Pattern16FlipVram(NUMSPRITEMANCONTORN, NUMSPRITEMANCONTORN, 0);
  }

  struct_car_principal.estat = SUICIDI;

  PutSprite(PLACARACTERPRIN, NUMSPRITEMAN, struct_car_principal.pos_x,
            struct_car_principal.pos_y, 0);
  PutSprite(PLACONTORNCARPRIN, NUMSPRITEMANCONTORN, struct_car_principal.pos_x,
            struct_car_principal.pos_y, 0);

  // Efecte sonor
  PlayFX(8);
  WAIT(50);

  struct_car_principal.vides--;
  if (struct_car_principal.vides==0) {
    // Fem el Game Over
    gameOver();
  }
  else {
    pinta_vides(struct_car_principal.vides);
    nivell--; // El canvi de nivell afegeix un, així continuem amb el mateix
    // Tornem els sprites de caràcter principal en moviment
    SetSpritePattern(NUMSPRITEMAN, Sprite32Bytes(&man_Joe[0]), 32); // 0
    SetSpritePattern(NUMSPRITEMANCONTORN, Sprite32Bytes(&man_Joe[16]), 32); // 1
    /* SetSpriteColors(PLACARACTERPRIN, &paleta_patrons[0]); */
    /* SetSpriteColors(PLACONTORNCARPRIN, &paleta_patrons[16]); */

    struct_mao.velocitat_mao -=2;
    canvi_nivell(); // Cada cop que em suïcido els maons van més ràpids ja que hi ha el canvi de nivell, he de tornar a enrere també la velocitat
  }
}


void gamePlay(void) {
  if (struct_mao.processar_pos_mao == 1) {
    if (struct_car_principal.collisionat == 0) {
      if ((struct_car_principal.pos_x + 12 >= struct_mao.pos_x) &&
          (struct_car_principal.pos_x <= struct_mao.pos_x + 8) &&
          (struct_car_principal.pos_y + 0 >= struct_mao.pos_y) &&
          (struct_car_principal.pos_y <= struct_mao.pos_y + 20)) {
        // He posat 0 en lloc de 16 en la 3a condició perquè mai estarà el
        // personatge per sobre d'un maó i feia que la col·lisió fos entrada
        // dos cops
        struct_car_principal.collisionat = 1;
      }
    }
    if (struct_car_secundari.collisionat == 0) {
      if ((struct_car_secundari.pos_x + 12 >= struct_mao.pos_x) &&
          (struct_car_secundari.pos_x <= struct_mao.pos_x + 8) &&
          (struct_car_secundari.pos_y + 0 >= struct_mao.pos_y) &&
          (struct_car_secundari.pos_y <= struct_mao.pos_y + 20)) {
        // He posat 0 en lloc de 16 en la 3a condició perquè mai estarà el
        // personatge per sobre d'un maó i feia que la col·lisió fos entrada
        // dos cops
        struct_car_secundari.collisionat = 1;
      }
    }
    calcula_pos_mao();
    struct_mao.processar_pos_mao = 0;
  }

  unsigned char linia6 = GetKeyMatrix(6);
  if ((linia6 & 128) == 0) {
    // Tecla F3
    // Ens suicidem
    suicidi();
  } else if ((linia6 & 64) == 0) {
    // F2
    /* SetSpritePattern(NUMSPRITEMAN, Sprite32Bytes(&man_Joe[0]), 32);         // 0 */
    /* SetSpritePattern(NUMSPRITEMANCONTORN, Sprite32Bytes(&man_Joe[16]), 32); // 1 */
    /* PutSprite(PLACARACTERPRIN, NUMSPRITEMAN, 120, 120, 0); */
    /* PutSprite(PLACONTORNCARPRIN, NUMSPRITEMANCONTORN, 120, 120, 0); */
    /* InitializeMyInterruptHandler((int)main_loop, 1); */
  }
  /* unsigned char linia4 = GetKeyMatrix(4); */
  /* if ((linia4 & 0x20) == 0) { */
  /*   // Tecla P */
  /*   // canvi_nivell(); */
  /* } */
  /* if ((linia4 & 0x01) == 0) { */
  /*   // Tecla K */
  /*   //gameOver(); */
  /*   // animacioFinal(); */
  /* } */

  // Processem moviment caràcter principal
  if (struct_car_principal.processant_moviment == 1) {
    calcula_pos_car_principal(Rkeys());
    calcula_pos_car_secundari();
    struct_car_principal.processant_moviment = 0;
  }

  // Col·lisió caràcter principal
  if (struct_car_principal.collisionat == 1) {
    PlayFX(6);
    // Efecte visual
    // Traiem el personatge d'estar en col·lisió
    struct_car_principal.pos_y -= 40;
    // Hem de posar-ho en estat sense salt si el maó et cau saltant, ja que
    // altrament no baixa i pots saltar més
    if (struct_car_principal.estat == MAINIMPULSDRE ||
        struct_car_principal.estat == MAINSALTDRE) {
      struct_car_principal.estat = MAIN1DRE;
      // Hem d'esborrar les ales
      PutSprite(PLAENERGIA, NUMSPRITEENERGIA, 255, 0, 0);
    } else if (struct_car_principal.estat == MAINIMPULSESQ ||
               struct_car_principal.estat == MAINSALTESQ) {
      struct_car_principal.estat = MAIN1ESQ;
      // Hem d'esborrar les ales
      PutSprite(PLAENERGIA, NUMSPRITEENERGIA, 255, 0, 0);
    }
    struct_car_principal.vides--;
    if (struct_car_principal.vides == 0) {
      gameOver();
    }
    pinta_vides(struct_car_principal.vides);
    struct_car_principal.collisionat = 0;
  }

  // Col·lisió caràcter secundari / gat
  if (struct_car_secundari.collisionat == 1) {
    PlayFX(6);
    // Efecte visual
    // Traiem el personatge d'estar en col·lisió
    struct_car_secundari.pos_y -= 40;
    // Hem de posar-ho en estat sense salt si el maó et cau saltant, ja que
    // altrament no baixa i pots saltar més
    if (struct_car_secundari.estat == MAINIMPULSDRE ||
        struct_car_secundari.estat == MAINSALTDRE) {
      struct_car_secundari.estat = MAIN1DRE;
      // Hem d'esborrar les ales
      PutSprite(PLAENERGIAGAT, NUMSPRITEENERGIA, 255, 0, 0);
    } else if (struct_car_secundari.estat == MAINIMPULSESQ ||
               struct_car_secundari.estat == MAINSALTESQ) {
      struct_car_secundari.estat = MAIN1ESQ;
      // Hem d'esborrar les ales
      PutSprite(PLAENERGIAGAT, NUMSPRITEENERGIA, 255, 0, 0);
    }
    if (estat_pantalla == PANT_JOC_2P) {
      struct_car_principal.vides--;
      if (struct_car_principal.vides == 0) {
        gameOver();
      }
      pinta_vides(struct_car_principal.vides);
    }
    struct_car_secundari.collisionat = 0;
  }

  // Animació energètica
  if (struct_car_principal.estat == MAINIMPULSDRE ||
      struct_car_principal.estat == MAINIMPULSESQ) {
    // Fer un if per controlar els temps i el canvi d'imatge de l'impuls per fer
    // diferents sons
    if (struct_car_principal.temps_espai == 4) {
      PlayFX(3);
    } else if (struct_car_principal.temps_espai == 15) {
      PlayFX(2);
    } else if (struct_car_principal.temps_espai == 30) {
      PlayFX(0);
    } else if (struct_car_principal.temps_espai == 49) {
      PlayFX(4);
    }
  } else if (struct_car_principal.estat == MAINSALTDRE ||
             struct_car_principal.estat == MAINSALTESQ) {
    if (struct_car_principal.sona_efecte == 0) {
      PlayFX(5);
      struct_car_principal.sona_efecte = 1;
    }
  }

  // Animació energètica gat
  if (struct_car_secundari.estat == MAINIMPULSDRE ||
      struct_car_secundari.estat == MAINIMPULSESQ) {
    // Fer un if per controlar els temps i el canvi d'imatge de l'impuls per fer
    // diferents sons
    if (struct_car_secundari.temps_espai == 4) {
      PlayFX(3);
    } else if (struct_car_secundari.temps_espai == 15) {
      PlayFX(2);
    } else if (struct_car_secundari.temps_espai == 30) {
      PlayFX(0);
    } else if (struct_car_secundari.temps_espai == 49) {
      PlayFX(4);
    }
  } else if (struct_car_secundari.estat == MAINSALTDRE ||
             struct_car_secundari.estat == MAINSALTESQ) {
    if (struct_car_secundari.sona_efecte == 0) {
      PlayFX(5);
      struct_car_secundari.sona_efecte = 1;
    }
  }

  // Detecció col·lisió maó amb caràcter secundari
  // El primer l'havia fet a la interrupció del VDP, però pel Telegram del
  // MSX-Lab havien comentat que només utilitzen la interrupció del VDP per la
  // música i el control del teclat Hem de tenir en compte que la posició del
  // maó x és per 2
  if ((struct_car_secundari.pos_x + 12 >= struct_mao.pos_x * 2) &&
      (struct_car_secundari.pos_x <= struct_mao.pos_x * 2 + 12) &&
      (struct_car_secundari.pos_y + 0 >= struct_mao.pos_y) &&
      (struct_car_secundari.pos_y <= struct_mao.pos_y + 30)) {
    // Posem el caràcter just per sobre
    PutSprite(PLAGAT, NUMSPRITEGAT, struct_car_secundari.pos_x,
              struct_car_secundari.pos_y, 0);
    // Avancem el maó perquè no hi hagi col·lisió un altre cop
    struct_mao.pos_y += 16;
  }

  // Ara provaré de fer la detecció dels dos caràcters aquí fora de la
  // interrupció, com abans he fet la del maó
  if ((struct_car_secundari.pos_x + 12 >= struct_car_principal.pos_x) &&
      (struct_car_secundari.pos_x <= struct_car_principal.pos_x + 12) &&
      (struct_car_secundari.pos_y + 8 >= struct_car_principal.pos_y) &&
      (struct_car_secundari.pos_y <= struct_car_principal.pos_y + 8)) {
    // Donem 200 punts
    puntuacio = puntuacio + 200;
    posa_puntuacio(puntuacio);
    if (nivell>=5) {
      estat_pantalla == PANT_FINAL;
      animacioFinal();
    }
    else {
      // Animació de retrobament està feta dins el canvi nivell
      // canvi de pantalla i de velocitat
      canvi_nivell();
    }
  }
}

void main(void) {
  SetColors(15, 0, 0); // Després del screen7 no té efecte
  Cls();

  // Inicialitzem PSG
  FT_openFile("bricks.afb");
  fcb_read(&file, ayFX_Bank, sizeof(ayFX_Bank));
  InitFX(ayFX_Bank);

  TIME tm;      // Init the Time Structure variable
  GetTime(&tm); // Retreive MSX-DOS Clock, and set the tm strucutre with
  // clock's values
  srand(tm.sec); // use current clock seconds as seed in the random generator

  estat_pantalla = PANT_INICIAL;
  //estat_pantalla = PANT_PINTEMJOC_1P;
  while(estat_pantalla != PANT_ACABEM) {
    if (estat_pantalla == PANT_PINTEMJOC_1P || estat_pantalla == PANT_PINTEMJOC_2P) {
      // Init FM music
      FT_openFile("pep.mbm");
      fcb_read(&file, songFile, sizeof(songFile));
      ompleCapcaleraPatrons();
      InicialitzemCanalsReproductor();
      tempo = capcalera.start_tempo;

      Screen(7);

      init_pantalla_joc();
      init_sprites(); // Ha d'anar després de init_pantalla_joc, si no es matxaquen bytes de VRAM dels sprites.

      struct_car_principal.pos_x = (char)234;
      struct_car_principal.pos_y = (char)(16*10-1);
      struct_car_principal.estat = MAIN1ESQ;
      struct_car_principal.index_sprite = 0;
      struct_car_principal.ferFlip = 0;
      struct_car_principal.hedefersalt = 0;
      struct_car_principal.sona_efecte = 0;
      struct_car_principal.collisionat = 0;

      struct_mao.pos_x = 22 + (rand() % 19) * 12;
      struct_mao.pos_y = 0;
      struct_mao.temps_mao = 0;
      struct_mao.velocitat_mao = 10; // A 23 sempre es queden a la primera línia. Hauria de revisar els càlculs. A 20 fa només dues línies
      // Hi ha cops que les col·lisions amb la velocitat no els fa cas. Hauré d'estudiar el punt a on passa
      struct_mao.continuem_generant_maons = 1;

      struct_car_secundari.pos_x = 28;
      struct_car_secundari.pos_y = (char)(16 * 11-5);
      struct_car_secundari.estat = MAIN1DRE;
      struct_car_secundari.index_sprite = 0;
      struct_car_secundari.ferFlip = 0;
      struct_car_secundari.hedefersalt = 0;
      struct_car_secundari.sona_efecte = 0;
      struct_car_secundari.collisionat = 0;

      if (estat_pantalla == PANT_PINTEMJOC_1P) {
        PutSprite(PLAGAT, NUMSPRITEGAT, struct_car_secundari.pos_x,
                  struct_car_secundari.pos_y, 0);
        PutSprite(PLAGATCONTORN, NUMSPRITEGATCONTORN,
                  struct_car_secundari.pos_x, struct_car_secundari.pos_y, 0);
      }

      struct_mao.processar_pos_mao=0;
      InitializeMyInterruptHandler((int)main_loop,1);

      if (estat_pantalla == PANT_PINTEMJOC_1P) {
        estat_pantalla = PANT_JOC_1P;
      }
      else if (estat_pantalla == PANT_PINTEMJOC_2P) {
        estat_pantalla = PANT_JOC_2P;
      }
    } else if (estat_pantalla == PANT_JOC_1P || estat_pantalla == PANT_JOC_2P) {
      gamePlay();
    } else if (estat_pantalla == PANT_INICIAL) {
      mypalette_1[0] = 0;      mypalette_1[16] = 4;    mypalette_1[32] = 8;    mypalette_1[48] =12;
      mypalette_1[1] = 0;      mypalette_1[17] = 3;    mypalette_1[33] = 6;    mypalette_1[49] = 6;
      mypalette_1[2] = 0;      mypalette_1[18] = 1;    mypalette_1[34] = 1;    mypalette_1[50] = 6;
      mypalette_1[3] = 0;      mypalette_1[19] = 1;    mypalette_1[35] = 0;    mypalette_1[51] = 6;
                                                                                                   
      mypalette_1[4] = 1;      mypalette_1[20] = 5;    mypalette_1[36] = 9;    mypalette_1[52] = 13;
      mypalette_1[5] = 1;      mypalette_1[21] = 1;    mypalette_1[37] = 5;    mypalette_1[53] = 7;
      mypalette_1[6] = 0;      mypalette_1[22] = 2;    mypalette_1[38] = 3;    mypalette_1[54] = 6;
      mypalette_1[7] = 1;      mypalette_1[23] = 3;    mypalette_1[39] = 2;    mypalette_1[55] = 5;
                                                                                                   
      mypalette_1[8] = 2;      mypalette_1[24] = 6;    mypalette_1[40] =10;    mypalette_1[56] =14;
      mypalette_1[9] = 1;      mypalette_1[25] = 2;    mypalette_1[41] = 7;    mypalette_1[57] = 6;
      mypalette_1[10] = 1;     mypalette_1[26] = 3;    mypalette_1[42] = 5;    mypalette_1[58] = 7;
      mypalette_1[11] = 2;     mypalette_1[27] = 4;    mypalette_1[43] = 2;    mypalette_1[59] = 7;
                                                                                                   
      mypalette_1[12] = 3;     mypalette_1[28] = 7;    mypalette_1[44] =11;    mypalette_1[60] =15;
      mypalette_1[13] = 2;     mypalette_1[29] = 5;    mypalette_1[45] = 7;    mypalette_1[61] = 7;
      mypalette_1[14] = 1;     mypalette_1[30] = 0;    mypalette_1[46] = 6;    mypalette_1[62] = 7;
      mypalette_1[15] = 1;     mypalette_1[31] = 3;    mypalette_1[47] = 2;    mypalette_1[63] = 7;

      // He de posar el fondo negre
      Screen(0);
      SetColors(15,0,0);

      Screen(5);

      FT_LoadSc5Image("sergialf.S05", 256, LDbuffer,256,BUFFER_SIZE_SC5);

      SetSC5Palette((Palette *)mypalette_1);

      // Carreguem el binari del bloc de pantalla inicial
      FT_openFile("PantInic.bMo");
      FcbRead(&file, pag2, 0x4000);

      // Música
      FT_openFile("IM_LOST.MBM");
      fcb_read(&file, songFile, sizeof(songFile));
      ompleCapcaleraPatrons();
      InicialitzemCanalsReproductor();
      tempo = capcalera.start_tempo;

      InitializeMyInterruptHandler((int)main_loop, 1);

      // Cridem la pantalla animació inicial
      //WaitForKey();
      __asm
        call #0x8d6b
        __endasm;

      if (nombre_jugadors==1) {
        estat_pantalla = PANT_PINTEMJOC_1P;
      }
      else if (nombre_jugadors==2) {
        estat_pantalla = PANT_PINTEMJOC_2P;
      }

      apaguemOPLL();
      EndMyInterruptHandler();

    } else if (estat_pantalla == PANT_FINAL) {
      animacioFinal();
    }
  }

  apaguemOPLL();
  StopFX();
  for (int i = 8; i < 11; i++)
    PSGwrite(
             i, 0); // Silencia el PSG, semblant al SilencePSG però no em compilava

  /* Point1 = nivell; */
  /* Point2 = pos_nums[nivell]; */
  /* Point3 = pos_nums[nivell + 1]; */

  /* Screen(0); */
  /* printf("\nEstat: %d,%d,%d, %d\n", struct_car_principal.pos_x, */
  /*        struct_car_secundari.pos_y, struct_mao.pos_x, struct_mao.pos_y); */
  /* printf("P1: %d\n", Point1); */
  /* printf("P2: %d\n", Point2); */
  /* printf("P3: %d\n", Point3); */
  /* printf("Test: %d\n", test); */
  /* printf("Mao: %d,%d\n", struct_mao.pos_x, struct_mao.pos_y); */
  /* printf("Secunda: %d,%d\n", struct_car_secundari.pos_x, */
  /*        struct_car_secundari.pos_y); */
  RestorePalette();
  Locate(0, 80);
  printf("Gracies per jugar!!!");
  Locate(0, 120);
  Exit(0);
}
// Al nivell 2 ja van molt ràpids els maons. Potser ja no hauria de tocar la velocitat per nivell 2023-01-14
