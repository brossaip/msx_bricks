/* Programa per crear l'animació final i les pantalles de les lletres */
// L'A és el 65
#include "FM_MBM.h"
#include "fusion-c/include/msx_fusion.h"
#include "fusion-c/include/vdp_graph2.h"
#include "fusion-c/include/vdp_sprites.h"
#include <string.h>

#define HALT __asm halt __endasm // wait for the next interrupt

static FCB file; // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC7 (2560 * 2)
#define BUFFER_SIZE_SC5 2560
#define TEMPS_ANIMACIO 10
#define POS_Y_ANIMACIONS 104

unsigned char
    LDbuffer[BUFFER_SIZE_SC5]; // No necessito sc7 per aquesta animació
unsigned __at 0xbffd int comptador_VDP;

char mypalette_3[16 * 4];
char mypalette_2[16 * 4];
char mypalette_1[16 * 4];
/* char mypalette_personatges[] = { */
/*   0,  0, 0, 0, // 15 La part de davant de la visera */
/*   1,  1, 0, 1,  */
/*   2,  1, 1, 2, // 3 - Part extensa gat */
/*   3,  2, 1, 1, // 8 - Cama esquerra Joe */
/*   4,  3, 1, 1, // 2 - PArt cama Joe */
/*   5,  1, 2, 3, // 14 - Ratlles gat - Color jersei man */
/*   6,  2, 3, 4, // 5- Part jersei Joe */
/*   7,  5, 0, 3, */
/*   8,  6, 1, 0, */
/*   9,  5, 3, 2, // 9 - Part mà dreta man */
/*   10, 7, 5, 2, // 10 - Part braç man */
/*   11, 7, 6, 2, // 4 - Peu Joe */
/*   12, 6, 6, 6, // cara gat - Ha desaparegut a la conversió. L'hauré de pintar de nou¿? */
/*   13, 7, 6, 5, // 6 - Color CAra */
/*   14, 6, 7, 7, */
/*   15, 7, 7, 7}; */

char mypalette_personatges[] = {
  0,  0, 0, 0, 
  1,  1, 0, 1, 
  2,  3, 1, 1, 
  3,  1, 1, 2, 
  4,  7, 6, 2, 
  5,  2, 3, 4, 
  6,  7, 6, 5,
  7,  5, 0, 3,
  8,  2, 1, 1, 
  9,  5, 3, 2, 
  10, 7, 5, 2, // Un d'aquests dos colors, el 10 ó el 11 el podria fer per pintar el Sam la cara blanca
  11, 7, 6, 2, 
  12, 0, 0, 0, // Fondo al meu dibuix
  13, 7, 6, 5, 
  14, 1, 2, 3,
  15, 5, 0, 3} // És la part de la visra del dfavant. Que ja està repetit al 7. Hauria de canviar la imatge i així tenir el color 15 lliure
  ;

static const unsigned char ball_pattern[] = {
  0b00010000,
  0b00111100,
  0b01100110,
  0b01000011,
  //
  0b11000010,
  0b01100110,
  0b00111100,
  0b00001000
};

static const unsigned char ball_pattern2[] = {
    0b00010000,
    0b00010000,
    0b00111000,
    0b00100111,
    //
    0b11100100,
    0b00011100,
    0b00001000,
    0b00001000};

static const unsigned char ball_pattern3[] = {
    0b00000000,
    0b00010000,
    0b00011000,
    0b00101110,
    //
    0b01110100,
    0b00011000,
    0b00001000,
    0b00000000};

static const unsigned char ball_pattern4[] = {
    0b00000000,
    0b00000000,
    0b00010000,
    0b00011100,
    //
    0b00111000,
    0b00001000,
    0b00000000,
    0b00000000};

// El patró de les ales
static const unsigned int ales[]={
0b0000000000000000,
0b0000000000000000,
0b0001100001100000,
0b0001110011100000,
0b0001110011100000,
0b0011110011110000,
0b0011110011110000,
0b0110110011011000,
0b1101000000101100,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000
};

static const unsigned int ales_2[]={
0b0000000000000000,
0b0000000000000000,
0b0001000010000000,
0b0001100110000000,
0b0011100111000000,
0b0011100111000000,
0b0111100111100000,
0b1100000000110000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
};

static const unsigned int ales_3[]={
0b0000000000000000,
0b0000000000000000,
0b0001010000000000,
0b0011011000000000,
0b0011011000000000,
0b0111011100000000,
0b1100000110000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
};

static const unsigned int ales_4[]={
0b0000000000000000,
0b0000000000000000,
0b0010100000000000,
0b0110110000000000,
0b0110110000000000,
0b1100011000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
0b0000000000000000,
};

int k; // variable de loops

struct particle{
  char pos_x;
  char pos_y;
  int canvi_x; // Pot prendre valors negatius
  char temps_canvi_x; 
  char temps_canvi_y;
  char radi; //Indicarà el número de patró que és el tamany de la bola
  char temps_dec_radi;
} ;

struct particle memoria_particles[32]; // 32 és el nombre màxim de sprites

// Part aleatòria extreta de
// https://sourceforge.net/p/sdcc/feature-requests/464/

unsigned long num_llarg_aleatori = 2463534242;

void rand_xor_init() { num_llarg_aleatori = (_Time << 15) | (unsigned long)_Time; }

int rand_xor() {
  num_llarg_aleatori = num_llarg_aleatori ^ (num_llarg_aleatori << 13);
  num_llarg_aleatori = num_llarg_aleatori ^ (num_llarg_aleatori >> 17);
  num_llarg_aleatori = num_llarg_aleatori ^ (num_llarg_aleatori << 5);
  return (num_llarg_aleatori & 0x7fff);
}

void WAIT(int cicles) {
  int i;
  for (i = 0; i < cicles; i++)
    HALT;
  return;
}

void FT_SetName( FCB *p_fcb, const char *p_name )  // Routine servant à vérifier le format du nom de fichier
{
  char i, j;
  memset( p_fcb, 0, sizeof(FCB) );
  for( i = 0; i < 11; i++ ) {
    p_fcb->name[i] = ' ';
  }
  for( i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++ ) {
    p_fcb->name[i] =  p_name[i];
  }
  if( p_name[i] == '.' ) {
    i++;
    for( j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.'); j++ ) {
      p_fcb->ext[j] =  p_name[i + j] ;
    }
  }
}


void FT_errorHandler(char n, char *name)            // Gère les erreurs
{
  Screen(0);
  SetColors(6,0,0);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}
 
int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer, unsigned int amplada_linia, unsigned int tamany_buffer)        // Charge les données d'un fichiers
    {
      // El tamany de tamany_buffer és el nombre de bytes que fan les 20 linies. En screen 5 és 2560, però en screen 7 és 2560*2
        int rd=2560;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file  
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, tamany_buffer );  // Read 20 lines of image data (128bytes per line in screen5)
             HMMC(buffer, 0,start_Y,amplada_linia,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

return(1);
}
int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{

  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

int FT_openFile(char *file_name) {
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }
}

unsigned char OldHook[5];
unsigned char MyHook[5];
unsigned char IntFunc[5];
unsigned char TypeOfInt;
__at 0xFD9F unsigned char VdpIntHook[];
__at 0xFD9A unsigned char AllIntHook[];
__at 0xF344 unsigned char RAMAD3;

void InterruptHandlerHelper (void) __naked
{
__asm
    push af
    call _IntFunc
    pop af
    jp _OldHook
__endasm;
}

void InitializeMyInterruptHandler (int myInterruptHandlerFunction, unsigned char isVdpInterrupt)
{
    unsigned char ui;
    MyHook[0]=0xF7; //RST 30 is interslot call both with bios or dos
    MyHook[1]=RAMAD3; //Page 3 generally is not paged out and is the slot of the ram, so this should be good
    MyHook[2]=(unsigned char)((int)InterruptHandlerHelper&0xff);
    MyHook[3]=(unsigned char)(((int)InterruptHandlerHelper>>8)&0xff);
    MyHook[4]=0xC9;
    IntFunc[0]=0xCD; //CALL
    IntFunc[1]=(unsigned char)((int)myInterruptHandlerFunction&0xff);
    IntFunc[2]=(unsigned char)(((int)myInterruptHandlerFunction>>8)&0xff);
    IntFunc[3]=0xC9;
    TypeOfInt = isVdpInterrupt;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();
    if (isVdpInterrupt)
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=VdpIntHook[ui];
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=MyHook[ui];
    }
    else
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=AllIntHook[ui];
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=MyHook[ui];
    }

    //Re-enable Interrupts
    EnableInterrupt();
}

void EndMyInterruptHandler (void)
{
    unsigned char ui;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();

    if (TypeOfInt)
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=OldHook[ui];
    else
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=OldHook[ui];

    //Re-enable Interrupts
    EnableInterrupt();
}

void main_loop(void) {
  playPatro();
  comptador_VDP++;
}

void TornaPaletaANegre(char paleta[]){
  char color ;
  char rgb ;
  char cops_a_reduir;

  for(cops_a_reduir=0;cops_a_reduir<7;cops_a_reduir++){
    for (color=0;color<16;color++) {
      paleta[color*4]=color;
      for(rgb=0;rgb<3;rgb++) {
        if (paleta[color + 1 + 3 * color + rgb] - 1 >= 0) {
          paleta[color + 1 + 3 * color + rgb] -= 1;
        }
      }
    }

    SetPalette((Palette *)paleta);
    WAIT(30);
  }
}

__at 0xc000 char paleta_passos[16 * 4];
void TornaNegreAColor(char paletaFinal[]){
  char color;
  char rgb;
  char cops_a_iterar;

  for (cops_a_iterar=0; cops_a_iterar<16*4; cops_a_iterar++){
    paleta_passos[cops_a_iterar]=0;
  }
  for (cops_a_iterar = 0; cops_a_iterar < 16; cops_a_iterar++) {
    paleta_passos[cops_a_iterar*4] = cops_a_iterar; // Posem l'índex de color
  }

  /* for (cops_a_iterar = 0; cops_a_iterar < 7; cops_a_iterar++) { */
  /*   for (color = 0; color < 16; color++) { */
  /*     for (rgb = 0; rgb < 3; rgb++) { */
  /*       if (paleta_passos[color + 1 + 3 * color + rgb] + 1 <= */
  /*           paletaFinal[color + 1 + 3 * color + rgb]) { */
  /*         paleta_passos[color + 1 + 3 * color + rgb] += 1; */
  /*       } */
  /*     } */
  /*   } */

  /*   SetPalette((Palette *)paleta_passos); */
  /*   WAIT(25); */
  /*   WaitKey(); */
  /* } */
  /* El primer color, el 1,1,1 ja brilla molt, ho faré més a poc a poc, per
   * cromes. A passar a negre, com que no tots els components de RGB són iguals, fa el degradat, però aquí de seguida es comença amb 1 */

  /* Primer fem una croma */
  for (cops_a_iterar = 0; cops_a_iterar < 3; cops_a_iterar++) {
    for (color = 0; color < 16; color++) {
      if (paleta_passos[color + 1 + 3 * color] + 1 <=
          paletaFinal[color + 1 + 3 * color]) {
        paleta_passos[color + 1 + 3 * color] += 1;
      }
    }
    SetPalette((Palette *)paleta_passos);
    WAIT(15);
  }
  /* Continuem la mateixa croma i fem una altra */
  for (cops_a_iterar = 0; cops_a_iterar < 4; cops_a_iterar++) {
    for (color = 0; color < 16; color++) {
      for(rgb=0;rgb<2;rgb++){
        if (paleta_passos[color + 1 + 3 * color + rgb] + 1 <=
            paletaFinal[color + 1 + 3 * color +rgb]) {
          paleta_passos[color + 1 + 3 * color +rgb] += 1;
        }
      }
    }
    SetPalette((Palette *)paleta_passos);
    WAIT(15);
  }
  /* La segona i la tercera component */
  for (cops_a_iterar = 0; cops_a_iterar < 7; cops_a_iterar++) {
    for (color = 0; color < 16; color++) {
      for (rgb = 1; rgb < 3; rgb++) {
        if (paleta_passos[color + 1 + 3 * color + rgb] + 1 <=
            paletaFinal[color + 1 + 3 * color + rgb]) {
          paleta_passos[color + 1 + 3 * color + rgb] += 1;
        }
      }
    }
    SetPalette((Palette *)paleta_passos);
    WAIT(15);
  }
}

void crea_particula(char k) {
    memoria_particles[k].pos_x = 120 + (rand_xor() & 15);
    memoria_particles[k].pos_y = 90 + (rand_xor() & 15);
    memoria_particles[k].canvi_x = (rand_xor() & 1) - (rand_xor() & 1);
    memoria_particles[k].temps_canvi_x = (rand_xor() & 3) + 4;
    memoria_particles[k].temps_canvi_y = (rand_xor() & 1) + 1;
    memoria_particles[k].radi = 4;
    memoria_particles[k].temps_dec_radi =
        (rand_xor() & 31) +
        90; // Sembla que per molt que el variï no afecti gaire
}

unsigned int MyTime; // Set a Global variable to verify time passed

char WaitTime(char nSeconds) // NSeconds is the number of seconds to wait
{
    SetRealTimer(0); // Reset the Timer (not mandatory) _Time got from 0 to
                     // 65535 in 18,2 minutes on a 60Hz MSX
    // Deu posar _Time a 0

    MyTime =
        _Time +
        nSeconds * 60; // Set the Time to wait based on the _Time reference. *50
                       // for PAL computer , *60 for NTSC computer

    while (_Time < MyTime) // Wait
    {
    }

    return (1);
}

void animacio_pantalla_inicial(void) {
    Screen(0);
    SetColors(6, 12, 12);
    rand_xor_init();
    Screen(5);
    Cls();

    SpriteSmall();
    Sprite8();
    SetSpritePattern(1, ball_pattern4, 8);
    SetSpritePattern(2, ball_pattern3, 8);
    SetSpritePattern(3, ball_pattern2, 8);
    SetSpritePattern(4, ball_pattern, 8);

    // Escombrem per crear les partícules i els patrons
    for (char k = 0; k < 32; k++) {
        crea_particula(k);
    }

    // Carreguem imatges a tota la VRAM
    FT_LoadSc5Image("Final_1.sc7", 256, LDbuffer, 512,
                    BUFFER_SIZE_SC5); // On charge l'umage
    FT_LoadPalette("Final_1.pl7", LDbuffer, mypalette_1);
    FT_LoadSc5Image("Final_2.sc7", 512, LDbuffer, 512,
                    BUFFER_SIZE_SC5); // On charge l'umage
    FT_LoadPalette("Final_2.pl7", LDbuffer, mypalette_2);
    FT_LoadSc5Image("Final_3.sc7", 768, LDbuffer, 512,
                    BUFFER_SIZE_SC5); // On charge l'umage
    FT_LoadPalette("Final_3.pl7", LDbuffer, mypalette_3);
    SetPalette((Palette *)mypalette_personatges);

    // Fem el moviment dels personatges perquè es trobin
    char pos_x = 255; // Original arribava a 195 com a màxim
    int pos_x_gat = -30;
    while (pos_x > 225) {
      for (k = 0; k < 6; k++) {
        HMMM(k * 32, 768 + 181, pos_x, POS_Y_ANIMACIONS, 32, 29);
        if (pos_x_gat < 0) {
          // El Man surt de darrera la pantalla de mica en mica, però el gat
          // si el poso a la coordenada 0 surt tot, l'he d'anar copiant de
          // mica en mica
          HMMM((char)((k * 32) - pos_x_gat), 768 + 160, 0, POS_Y_ANIMACIONS+6,
               (char)(32 + pos_x_gat), 22);
        } else {
          HMMM(k * 32, 768+160, pos_x_gat, POS_Y_ANIMACIONS+6, 32, 22);
        }
        WAIT(TEMPS_ANIMACIO);
        HMMM(200, 768+183, pos_x, POS_Y_ANIMACIONS, 4, 29); // Borrem els quadrats. No cal tot, només una part
        HMMM(200, 768+183, pos_x_gat, POS_Y_ANIMACIONS+6, 4, 22);
      pos_x = pos_x - 4;
      pos_x_gat = pos_x_gat + 4;
    }
  }

  char numero_animacio_caracters = 0;
  char num_animacions;
  for ( num_animacions = 0; num_animacions < 21;
                           num_animacions++) {
    // Brick central
    HMMM(numero_animacio_caracters * 32, 768+160, pos_x_gat, POS_Y_ANIMACIONS+6, 32, 22);
    HMMM(numero_animacio_caracters * 32, 768+181, pos_x, POS_Y_ANIMACIONS, 32, 29);
    WAIT(TEMPS_ANIMACIO);
    // Borrem
    HMMM(200, 768+183, pos_x, POS_Y_ANIMACIONS, 4, 29); // Borrem els quadrats
    HMMM(200, 768+183, pos_x_gat, POS_Y_ANIMACIONS+6, 4, 22);
    pos_x = pos_x - 4;
    pos_x_gat = pos_x_gat + 4;
    numero_animacio_caracters ++;
    if (numero_animacio_caracters == 6) {
      numero_animacio_caracters = 0;
    }
  }

  // Una úlima còpia del gat:
  pos_x_gat = pos_x_gat+2;
  HMMM(numero_animacio_caracters * 32, 768 + 160, pos_x_gat,
       POS_Y_ANIMACIONS + 6, 32, 22);
  HMMM(200, 768 + 183, pos_x_gat-4, POS_Y_ANIMACIONS + 6, 4, 22);

  // Comencen les bombolles
  SetRealTimer(0);
  MyTime = _Time+5*60; // 5 són els segons a esperar a 60Hz
  while(_Time<MyTime) {
    for (k=0; k<12; k++) {
      PutSprite(k,memoria_particles[k].radi, (int)memoria_particles[k].pos_x, (int)memoria_particles[k].pos_y,0);
      memoria_particles[k].temps_dec_radi -= 1;
      if (memoria_particles[k].temps_dec_radi == 0) {
        memoria_particles[k].radi -= 1;
        memoria_particles[k].temps_dec_radi = (rand_xor() & 31) + 30;
      }
      memoria_particles[k].temps_canvi_x -=1;
      if (memoria_particles[k].temps_canvi_x==0){
        memoria_particles[k].pos_x += memoria_particles[k].canvi_x;
        memoria_particles[k].temps_canvi_x = (rand_xor() & 3) + 4;
      }
      memoria_particles[k].temps_canvi_y -=1;
      if (memoria_particles[k].temps_canvi_y == 0) {
        memoria_particles[k].pos_y -=1;
        memoria_particles[k].temps_canvi_y = (rand_xor() & 3) + 2;
      }
      if (memoria_particles[k].pos_y < 3 || memoria_particles[k].radi == 0) {
        // Hem de crear una nova partícula:
        crea_particula(k);
      }
    }
    WAIT(1);
  }

  TornaPaletaANegre(mypalette_personatges);

  // Copiem imatge 1
  HMMM(0,256,0,0,256,211);
  SpriteOff();
  // He de retornar la imatge de negre a color
  TornaNegreAColor(mypalette_1);

  SetRealTimer(0);
  WaitTime(5);

  TornaPaletaANegre(mypalette_1);

  // Copiem imatge 2
  HMMM(0, 512, 0, 0, 256, 211);
   // He de retornar la imatge de negre a color
  TornaNegreAColor(mypalette_2);

  SetRealTimer(0);
  WaitTime(5);

  TornaPaletaANegre(mypalette_2);

  // Copiem imatge 3
  // Hem de borrar la part que no pintem de negre
  // Estic esborrant la zona dels sprites que és 0x7400->0x7fff
  for (k = 0; k < 4; k++) {
    HMMM(0, 768, 0, k * 15, 256, 15);
    HMMM(0, 768, 0, 211 - (k * 15) - 15, 256, 15);
  }
  HMMM(0, 768, 0, 25, 256, 156);
  // He de retornar la imatge de negre a color
  TornaNegreAColor(mypalette_3);

  // Faig les aletes en comptes de bombolles
  SetSpritePattern(4, Sprite32Bytes(ales_4), 32); // 0 // El radio 0 no arriba mai ja que es crea una nova
                        // partícula. Per això comena amb radi 4
  SetSpritePattern(8, Sprite32Bytes(ales_3), 32);  // 1
  SetSpritePattern(12, Sprite32Bytes(ales_2), 32); // 2
  SetSpritePattern(16, Sprite32Bytes(ales), 32); // 3 radi més gran, al principi
  Sprite16();
  SpriteOn();

  // Comencen les aletes
  // Hem de posar un color diferent a les bombolles que no es veuen
  char color_bombolles[8];
  for (char linia=0; linia < 8; linia++) {
    color_bombolles[linia]=10;
  }
  for (k = 0; k < 12; k++) {
    SetSpriteColors(k, color_bombolles);
  }

  char primerCop = 1;
  char tecla_apretada = 0;
  KillKeyBuffer();
  while(!tecla_apretada) {
    SetRealTimer(0);
    MyTime = _Time + 5 * 60; // 5 són els segons a esperar a 60Hz
    while (_Time < MyTime && tecla_apretada==0) {
      for (k=0; k<12; k++) {
        PutSprite(k,memoria_particles[k].radi*4, (int)memoria_particles[k].pos_x+75, (int)memoria_particles[k].pos_y+45,0);
        // He afegit un offset a la posició inicial de les partícules per posar-les a prop de les ales de xalats
        memoria_particles[k].temps_dec_radi -= 1;
        if (memoria_particles[k].temps_dec_radi == 0) {
          memoria_particles[k].radi -= 1;
          memoria_particles[k].temps_dec_radi = (rand_xor() & 31) + 30;
        }
        memoria_particles[k].temps_canvi_x -=1;
        if (memoria_particles[k].temps_canvi_x==0){
          memoria_particles[k].pos_x += memoria_particles[k].canvi_x;
          memoria_particles[k].temps_canvi_x = (rand_xor() & 3) + 4;
        }
        memoria_particles[k].temps_canvi_y -=1;
        if (memoria_particles[k].temps_canvi_y == 0) {
          memoria_particles[k].pos_y -=1;
          memoria_particles[k].temps_canvi_y = (rand_xor() & 3) + 2;
        }
        if (memoria_particles[k].pos_y < 3 || memoria_particles[k].radi == 0) {
          // Hem de crear una nova partícula:
          crea_particula(k);
        }
        if (primerCop ==0) {
          if (Inkey()!=0) {
            tecla_apretada=1;
          }
        }
      }
      WAIT(1);
    }
    primerCop = 0;
  }

}

/* void CallMain_asm()__naked { */
/*   __asm */
/*     call _animacio_pantalla_inicial */
/*     ret */
/*   __endasm; */
/* } */

void main() {
  // Carreguem música que ja ho farà el programa principal
  FT_openFile("level.MBM");
  fcb_read(&file, songFile, sizeof(songFile));
  ompleCapcaleraPatrons();
  InicialitzemCanalsReproductor();
  tempo = capcalera.start_tempo;

  InitializeMyInterruptHandler((int)main_loop,1);

  SpriteOn();
  animacio_pantalla_inicial();
  EndMyInterruptHandler();
  apaguemOPLL();

  Screen(0);
  RestorePalette();
  Exit(0);
}
