# Els videos que hi havia en avi, vull extreure el so perquè ocupin menys

import glob
import ffmpeg
import os

fitxers =  sorted(glob.glob('/home/jepsuse/MSX/MUSICPAC/*MBM'))
numPorto = 1
fitxersNoTrobats = []
fw = open("/home/jepsuse/MSX/MUSICPAC/NomCançons.txt","w")
for fitxer in fitxers:
    print("*****************************************")
    print(f"Cançó {fitxer}. Número {numPorto} de {len(fitxers)}")
    print("*****************************************")
    [path, nomfitx]=os.path.split(fitxer)
    try:
        fr = open(fitxer,'rb')
        data_byte = fr.read(0xf8)
        nom_canco = data_byte[0xcf:0xf7].decode('utf-8')
        fw.write(f"{nomfitx} - {nom_canco}\n")
        (
            ffmpeg
            .input('/home/jepsuse/.openMSX/videos/' + nomfitx + '.avi')
            .output('/home/jepsuse/MSX/MUSICPAC/mp3/' + nomfitx[0:-4] + '.mp3', map='0:a', acodec='libmp3lame')
            .run()
        )
        fr.close()
    except:
        print(f"No trobat {nomfitx}")
        fitxersNoTrobats.append(nomfitx)
    numPorto = numPorto + 1
fw.write("No hem trobat els fitxers: " + str(fitxersNoTrobats))
print("No hem trobat els fitxers: " + str(fitxersNoTrobats) )
fw.close()
