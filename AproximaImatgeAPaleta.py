# Tenim una imatge en png i una paleta del MSX i crea la imatge en sc7 que més s'aproxima al color de la paleta.
# A diferència del reduce_palette, aquest no crea la paleta. El reduce crea una paleta segons la imatge. Aquí usem una paleta ja donada.

# El primer paràmetre és el nom de la imatge
# El segon és el fitxer que conté la paleta
# El Tercer indica el format del fitxer de la paleta: 0 és el format pl7 i 1 és un fitxer text obtingut com a _centres.txt en el reduce_palette

import sys
import numpy as np
import cv2
from matplotlib.pyplot import imshow
from matplotlib import pyplot as plt

# PEr cada pixel de la imatge he de buscar quin és el més curt de la paleta

cami_imatge = sys.argv[1]
cami_paleta = sys.argv[2]
tipus_paleta = sys.argv[3]

# Llegim la paleta
if tipus_paleta == 0:
    with open(cami_paleta, "rb") as f:
        bytes_paleta = f.read()

    arr_paleta_flatten = np.empty(0).astype('uint8')
    for k in range(len(bytes_paleta) - 7):
        arr_paleta_flatten = np.append(arr_paleta_flatten, bytes_paleta[k + 6] >> 4)
        arr_paleta_flatten = np.append(arr_paleta_flatten, bytes_paleta[k + 7] & 0xF)
    arr_paleta = arr_paleta_flatten.reshape(16, 3)
    arr_paleta_8b = arr_paleta * 32
else:
    with open(cami_paleta,'r') as f:
        linies = f.readlines()
        cadena = ''
        for linia in linies:
            cadena = cadena + linia.strip()
        arr_paleta_8b = np.asarray(eval(cadena))

# Llegim la imatge a convertir
img_BGR = cv2.imread(cami_imatge)
img = cv2.cvtColor(img_BGR, cv2.COLOR_BGR2RGB)

[h, w] = img.shape[0:2]
img_sc7 = np.empty(0).astype('uint8')
img_visualize = np.empty(0).astype('uint8')  # La imatge per veure com ha quedat
for y in range(h):
    for x in range(w):
        dist = 256 * 256 * 3
        distpos = -1
        for k in range(16):
            distpunt = np.linalg.norm(img[y, x] - arr_paleta_8b[k])
            if distpunt < dist:
                dist = distpunt
                distpos = k
        img_sc7 = np.append(img_sc7, distpos)
        img_visualize = np.append(img_visualize, arr_paleta_8b[distpos])

# Gravem la imatge a sc7
with open(cami_imatge[:-4] + "_reub.sc7", "wb") as f:
    capçalera = b'\xfe\x01\x02\x03\x04\x05\x06'
    f.write(capçalera)
    for k in range(int(len(img_sc7) / 2)):
        valor = int(img_sc7[2 * k] << 4) + img_sc7[2 * k + 1]
        f.write(np.byte(valor))

# Si hem entrat l'array en text, el convertim a pl7
if tipus_paleta !=0:
    centflat = np.uint(arr_paleta_8b/32).flatten().astype(int)
    with open(cami_imatge[:-3] + "_reub.pl7", "wb") as f:
        capçalera = b'\xfe\x01\x02\x03\x04\x05\x06'
        f.write(capçalera)
        for k in range(int(len(centflat) / 2)):
            valor = (centflat[2 * k] << 4) + centflat[2 * k + 1]
            f.write(np.byte(valor))

# Visualitzem la imatge
img_visualize = img_visualize.reshape((img.shape))
plt.imshow(img_visualize)
plt.show()
