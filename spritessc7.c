#include "sc7brick.h"
#include "fusion-c/include/vdp_graph2.h"
#include "fusion-c/include/vdp_sprites.h"

#define HALT __asm halt __endasm            // wait for the next interrupt

static FCB file;                            // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC7 (2560 * 2)
#define BUFFER_SIZE_SC5 2560

unsigned char LDbuffer[BUFFER_SIZE_SC7]; // Utilitzo el gran que ja engloba el petit
unsigned char ayFX_Bank[0x58B];

// Definim els diferents estats
#define MAIN1ESQ 0
#define MAIN1DRE 2
#define MAIN2ESQ 4
#define MAIN2DRE 6
#define MAINSALTESQ 8
#define MAINSALTDRE 10
#define MAINIMPULSESQ 12
#define MAINIMPULSDRE 14
#define BAIXANTESQ 16 // Per detectar el cantó per l'esquerre, encara no té gràfic baixant, però es pot crear mirant cap a baix
#define BAIXANTDRE 18 // Els he d'assignar un sprite, que si no no els pinta
#define SUICIDI 20

#define NUMSPRITEMAO 64
#define NUMSPRITEENERGIA 68 // Es poden definir fins a 256 sprites de 8x8 i 64 de 16x16
#define NUMSPRITEGAT 72

#define PLACARACTERPRIN 0
#define PLACONTORNCARPRIN 1
#define PLAMAO 8
#define PLAENERGIA 9
#define PLAGAT 2
#define PLAENERGIAGAT 4

// Definicions RKeys
#define BRICKS_KEY_SPACE 1
#define BRICKS_KEY_LEFT 16
#define BRICKS_KEY_UP 32
#define BRICKS_KEY_DOWN 64
#define BRICKS_KEY_RIGHT 128
#define BRICKS_KEY_A 64    // Línia 2
#define BRICKS_KEY_D 2     // Línia 3
#define BRICKS_KEY_W 16    // Línia 5
#define BRICKS_KEY_X 32    // Línia 5
#define BRICKS_KEY_SHIFT 1 // Línia 6

// Definim els estats de les pantalles joc
#define PANT_INICIAL 1
#define PANT_JOC_1P 2
#define PANT_JOC_2P 3
#define PANT_GAMEOVER 4
#define PANT_PINTEMJOC_1P 5
#define PANT_PINTEMJOC_2P 6
#define PANT_ACABEM 0

// Definim constants de l'animació inicial
#define TEMPS_CAIGUDA_MAONS 10
#define POS_Y_MAONS 128
const char pos_x_anim_carPrincipal[] = {1,2};

// Array dels sprites que defineixen el caràcter i que definiran la posició dels
// estats
const char sprites_estats[] = {0,  4,  8,  12, 16, 20, 24, 28,
                               32, 36, 40, 44, 48, 52, 56, 60,
                               32, 36, 40, 44};

// Posició dels digits de la puntuació / score
const int pos_nums[11] = {
    60,  67,  74,  81,  88, 95,
    103, 110, 117, 123, 131}; // L'última posició és on acaba el 9
// El número 8 s'ha de tirar més a la dreta, però queda mal retallat després el 7

// Tot de zeros per borrar el marcador o per si s'han de fer altres reformes
// També podria tenir una zona de la VRAM que contingués el quadre negre per esborrar
const int vectzero[450] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //25
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //50
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //75
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //100
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //125
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //150
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //175
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //200
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //225
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //250
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //275
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //300
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //325
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //350
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //375
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //400
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //425
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //450
};

static char copsVsync = 0;
static char test = 0;
static char Point1;
static char Point2;
static char Point3;
static char debug;

typedef struct {
  char pos_x;
  char pos_y;
  char estat; // Els del dibuix/esquema 1esq, 2esq, imp_esq, salt_esq i els
              // respectius dreta, definits amb els defines
  unsigned int temps_espai;
  char hedefersalt; // si el temps de pujar ha estat suficient per escalar
  char sona_efecte; // Indica si l'efecte sonor s'està generant
  char collisionat;
  char vides;
  char processant_moviment;
} caracter_principal;

caracter_principal struct_car_principal;
caracter_principal struct_car_secundari;

typedef struct {
  char pos_x;
  char pos_y;
  unsigned int temps_mao;
  char velocitat_mao;
  char continuem_generant_maons;
  char processar_pos_mao;
} estructura_mao;

estructura_mao struct_mao;
char nivell;
int puntuacio;
char temps_entre_maons = 10;
char estat_pantalla;

void WAIT(int cicles) {
  int i;
  for (i = 0; i < cicles; i++)
    HALT;
  return;
}

unsigned char OldHook[5];
unsigned char MyHook[5];
unsigned char IntFunc[5];
unsigned char TypeOfInt;
__at 0xFD9F unsigned char VdpIntHook[];
__at 0xFD9A unsigned char AllIntHook[];
__at 0xF344 unsigned char RAMAD3;

void InterruptHandlerHelper (void) __naked
{
__asm
    push af
    call _IntFunc
    pop af
    jp _OldHook
__endasm;
}

void InitializeMyInterruptHandler (int myInterruptHandlerFunction, unsigned char isVdpInterrupt)
{
    unsigned char ui;
    MyHook[0]=0xF7; //RST 30 is interslot call both with bios or dos
    MyHook[1]=RAMAD3; //Page 3 generally is not paged out and is the slot of the ram, so this should be good
    MyHook[2]=(unsigned char)((int)InterruptHandlerHelper&0xff);
    MyHook[3]=(unsigned char)(((int)InterruptHandlerHelper>>8)&0xff);
    MyHook[4]=0xC9;
    IntFunc[0]=0xCD; //CALL
    IntFunc[1]=(unsigned char)((int)myInterruptHandlerFunction&0xff);
    IntFunc[2]=(unsigned char)(((int)myInterruptHandlerFunction>>8)&0xff);
    IntFunc[3]=0xC9;
    TypeOfInt = isVdpInterrupt;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();
    if (isVdpInterrupt)
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=VdpIntHook[ui];
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=MyHook[ui];
    }
    else
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=AllIntHook[ui];
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=MyHook[ui];
    }

    //Re-enable Interrupts
    EnableInterrupt();
}

void EndMyInterruptHandler (void)
{

  
    unsigned char ui;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();

    if (TypeOfInt)
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=OldHook[ui];
    else
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=OldHook[ui];

    //Re-enable Interrupts
    EnableInterrupt();

}

void calcula_pos_mao(){
  if (struct_mao.temps_mao>temps_entre_maons && struct_mao.continuem_generant_maons==1) {
    struct_mao.pos_y += 8 + struct_mao.velocitat_mao;

    struct_mao.temps_mao = 0;
    test = 0;
    char posicio_16 = (struct_mao.pos_y >> 4) << 4; // Dividim entre 16 (2^4) i tornem a multiplicar per agafar el múltiple correcte
    char nou_mao = 0;
    // mirem si hi ha maó a sota i el dibuixem per tornar a començar
    if (struct_mao.pos_y > 182) {
      HMMM(2, 256, struct_mao.pos_x * 2, 171, 24, 16);
      nou_mao = 1;
    }
    else if ((Point(struct_mao.pos_x*2+7, posicio_16+16+4) !=0 ) ) {
      // He afegit l'OR perquè per certes velocitats del maó no el trobava, fent proves amb level 3 i 5
      char offset = 0;
      // El problema amb molta velocitat és que salta una filera. He de comprovar si la filera de dalt està ocupada i si és així enganxar-lo més amunt. És molt possible que hagi de mirar més d'una filera
      if (Point(struct_mao.pos_x * 2 + 7, posicio_16 + 6) != 0) {
        offset = 16;
      }
      // Deixem el maó fix
      HMMM(2, 256, struct_mao.pos_x * 2, posicio_16 - 8 + 3 - offset, 24, 16);
      nou_mao = 1;
    }
    if (nou_mao==1){
      puntuacio = puntuacio + 10;
      posa_puntuacio(puntuacio);
      // Nou maó
      struct_mao.pos_x = 34 + (rand()%18)*12; //Hi havia 19, però no cal darrere el personatge principal
      struct_mao.pos_y = 0;
      PlayFX(7);
    }

    PutSprite(PLAMAO, NUMSPRITEMAO, struct_mao.pos_x, struct_mao.pos_y+3,0);
  }
}

void main_loop(void) {
  if (copsVsync > 4) {
    copsVsync = 0;
    if (struct_car_principal.estat != SUICIDI) {
      struct_car_principal.processant_moviment = 1;
    }
  }
  if (struct_mao.processar_pos_mao == 0) {
    struct_mao.processar_pos_mao = 1;
  }
  struct_mao.temps_mao++;
  copsVsync++;
  UpdateFX(); // Actualitzem el PSG
  playPatro();
}

void posa_puntuacio(int punts) {
  // La puntuació - score
  int dig;
  int posX_score = 140;
  // Primer borrarem tota la puntuacio perquè a cops els dígits deixen rastre ja que tenen gruixos diferents
  HMMC(vectzero, 105, 198, 147 - 105, 208 - 198);

  while (punts > 0) {
    dig = punts % 10;
    posX_score = posX_score - pos_nums[dig + 1] + pos_nums[dig];
    HMMM(pos_nums[dig] - 2, 257, posX_score, 198,
         pos_nums[dig + 1] - pos_nums[dig],
         9); // He posat el -2 a pos_nums[dig]-2 per l'offset en el 2.0
    punts = punts / 10;
  }
}

void pinta_vides(char vides) {
  HMMC(vectzero, 211, 198, 24, 10);
  HMMM(pos_nums[vides]-2, 257, 211, 198, pos_nums[vides + 1] - pos_nums[vides], 9);
  // He posat el -2 a pos_nums[vides]-2 per l'offset en el 2.0
}

void pinta_numero_level() {
  HMMC(vectzero, 311, 198, 24, 10);
  HMMM(pos_nums[nivell]-2, 257, 311, 198, pos_nums[nivell + 1] - pos_nums[nivell], 9);
}

char puc_esquerra() {
  // Segons posicio nas
  // Les parets no coincideixen amb el 5, poden ser el nou
  // No he de mirar just a la cara, he de mirar els peus que són els que determinen el salt
  if (Point(struct_car_principal.pos_x * 2 + 2,
            struct_car_principal.pos_y + 13) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 4,
            struct_car_principal.pos_y + 13) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 2,
            struct_car_principal.pos_y + 13) == 9 ||
      Point(struct_car_principal.pos_x * 2 + 4,
            struct_car_principal.pos_y + 13) == 9 ||
      Point(struct_car_principal.pos_x * 2 + 2,
            struct_car_principal.pos_y + 12) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 4,
            struct_car_principal.pos_y + 12) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 2,
            struct_car_principal.pos_y + 12) == 9 ||
      Point(struct_car_principal.pos_x * 2 + 4,
            struct_car_principal.pos_y + 12) == 9) {
    return 0;
  } else {
    return 1;
  }
}

char puc_esquerra_gat() {
  //Segons posicio cua
  if (Point(struct_car_secundari.pos_x * 2 + 2,
            struct_car_secundari.pos_y + 13) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 4,
            struct_car_secundari.pos_y + 13) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 2,
            struct_car_secundari.pos_y + 13) == 9 ||
      Point(struct_car_secundari.pos_x * 2 + 4,
            struct_car_secundari.pos_y + 13) == 9 ||
      Point(struct_car_secundari.pos_x * 2 + 2,
            struct_car_secundari.pos_y + 12) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 4,
            struct_car_secundari.pos_y + 12) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 2,
            struct_car_secundari.pos_y + 12) == 9 ||
      Point(struct_car_secundari.pos_x * 2 + 4,
            struct_car_secundari.pos_y + 12) == 9) {
    return 0;
  } else {
    return 1;
  }
}

char puc_caure_esquerra() {
  // Segons punta peu dret
  if (Point(struct_car_principal.pos_x * 2 + 7,
            struct_car_principal.pos_y + 18) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 8,
            struct_car_principal.pos_y + 18) == 5) {
    return 0;
  } else {
    return 1;
  }
}

char puc_caure_esquerra_gat() {
  // Segons punta pota dreta
  if (Point(struct_car_secundari.pos_x * 2 + 7,
            struct_car_secundari.pos_y + 18) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 8,
            struct_car_secundari.pos_y + 18) == 5) {
    return 0;
  } else {
    return 1;
  }
}

char puc_dreta() {
  // Segons posicio nas
  // Les parets no coincideixen amb el 5, poden ser el nou
  // L'sprite fa uns 16 d'amplada, per 2 ja que fa el doble que els altres sprites
  // També he de mirar més d'una posició de les y perquè a cops amb el salt enganxa un forat negre
  if (Point(struct_car_principal.pos_x * 2 + 30,
            struct_car_principal.pos_y + 13) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 29,
            struct_car_principal.pos_y + 13) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 30,
            struct_car_principal.pos_y + 13) == 9 ||
      Point(struct_car_principal.pos_x * 2 + 29,
            struct_car_principal.pos_y + 13) == 9 ||
      Point(struct_car_principal.pos_x * 2 + 30,
            struct_car_principal.pos_y + 12) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 29,
            struct_car_principal.pos_y + 12) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 30,
            struct_car_principal.pos_y + 12) == 9 ||
      Point(struct_car_principal.pos_x * 2 + 29,
            struct_car_principal.pos_y + 12) == 9) {
    return 0;
    }
  else {
    return 1;
  }
}

char puc_dreta_gat() {
  // Segons posicio nas
  if (Point(struct_car_secundari.pos_x * 2 + 30+0,
            struct_car_secundari.pos_y + 13) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 29+0,
            struct_car_secundari.pos_y + 13) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 30+0,
            struct_car_secundari.pos_y + 13) == 9 ||
      Point(struct_car_secundari.pos_x * 2 + 29+0,
            struct_car_secundari.pos_y + 13) == 9 ||
      Point(struct_car_secundari.pos_x * 2 + 30+0,
            struct_car_secundari.pos_y + 12) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 29+0,
            struct_car_secundari.pos_y + 12) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 30+0,
            struct_car_secundari.pos_y + 12) == 9 ||
      Point(struct_car_secundari.pos_x * 2 + 29+0,
            struct_car_secundari.pos_y + 12) == 9) {
    return 0;
  } else {
    return 1;
  }
}

char puc_caure_dreta() {
  // Segons punta peu dret
  // He d'afinar perquè amb 20 i 21 és quan puja al maó, però quan cau cap a la dreta en el salt és una altra funció
  if (Point(struct_car_principal.pos_x * 2 + 18,
            struct_car_principal.pos_y + 18) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 19,
            struct_car_principal.pos_y + 18) == 5 ) {
    return 0;
  } else {
    return 1;
  }
}

char puc_caure_dreta_gat() {
  // Segons punta pota dreta
  if (Point(struct_car_secundari.pos_x * 2 + 18,
            struct_car_secundari.pos_y + 18) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 19,
            struct_car_secundari.pos_y + 18) == 5 ) {
    return 0;
  } else {
    return 1;
  }
}

char puc_caure_dreta_mirantEsq() {
  // En no ser simètric, no és la mateixa posició quan estic mirant a la dreta que a l'esquerra i per tant he de calcular el caure de forma diferent
  if (Point(struct_car_principal.pos_x * 2 + 25,
            struct_car_principal.pos_y + 18) == 5 ||
      Point(struct_car_principal.pos_x * 2 + 26,
            struct_car_principal.pos_y + 18) == 5) {
    return 0;
  } else {
    return 1;
  }
}

char puc_caure_dreta_mirantEsq_gat() {
  if (Point(struct_car_secundari.pos_x * 2 + 25,
            struct_car_secundari.pos_y + 18) == 5 ||
      Point(struct_car_secundari.pos_x * 2 + 26,
            struct_car_secundari.pos_y + 18) == 5) {
    return 0;
  } else {
    return 1;
  }
}

void calcula_pos_car_principal(unsigned char tecla_apretada) {
  // !!!!!! He de controlar que si no ha fet més d'un cert temps de l'espai, més de 4 que és l'escala que tinc, no canviï d'estat i es posi en estandard
  /*************************************
   * BAixant cap a l'esquerra
   */
  // Primer de tot mirem si estem caient
  // Ho farem segons la posicio del peu que es 7 pixels dreta
  // Els nous colors del maó
  if (puc_caure_dreta_mirantEsq()==1 && puc_caure_esquerra()==1 &&
      struct_car_principal.estat == BAIXANTESQ) {
    // He posat +2 i +4 perquè hi ha el punt entre mig dels maons que és negre
    struct_car_principal.pos_y+=4; // Hi havia 16 a l'original, he posat només 4 per seguir com fa el salt
  }
  if ((puc_caure_dreta_mirantEsq() == 0 || puc_caure_esquerra() == 0) &&
      struct_car_principal.estat == BAIXANTESQ) {
    // Miro si puc continuar baixant, sinó és que ja em puc aturar
    struct_car_principal.estat = MAIN1ESQ;
  }
  if (puc_caure_dreta_mirantEsq()==1 && puc_caure_esquerra()==1 &&
      (struct_car_principal.estat == MAIN1ESQ ||
       struct_car_principal.estat == MAIN2ESQ)) {
    // He posat +2 i +4 perquè hi ha el punt entre mig dels maons que és negre
    struct_car_principal.pos_y += 4; // Hi havia 16 a l'original, he posat només
                                     // 4 per seguir com fa el salt
    struct_car_principal.estat = BAIXANTESQ;
  }

  /******************************************
   *** Moviments bàsics esquerra
   */
  if ((struct_car_principal.estat == MAIN1ESQ) &&
      (tecla_apretada == BRICKS_KEY_LEFT)) {
    struct_car_principal.estat = MAIN2ESQ;
    // L'estat ja m'hauria d'indicar quina animació he d'agafar
    // i fer el PutSprite al final de tot comú a tothom
    if (puc_esquerra() == 1) {
      struct_car_principal.pos_x += -1;
    }
  } else if ((struct_car_principal.estat == MAIN1ESQ) &&
             (tecla_apretada == BRICKS_KEY_RIGHT)) {
    struct_car_principal.estat = MAIN1DRE;
    struct_car_principal.pos_x += 1;
  } else if ((struct_car_principal.estat == MAIN2ESQ) &&
             (tecla_apretada == BRICKS_KEY_LEFT)) {
    struct_car_principal.estat = MAIN1ESQ;
    if (puc_esquerra() == 1) {
      struct_car_principal.pos_x += -1;
    }
  } else if ((struct_car_principal.estat == MAIN2ESQ) &&
             (tecla_apretada == BRICKS_KEY_RIGHT)) {
    struct_car_principal.estat = MAIN2DRE;
    struct_car_principal.pos_x += 1;
  } else if ((struct_car_principal.estat == MAIN1ESQ ||
              struct_car_principal.estat == MAIN2ESQ) &&
             (tecla_apretada == BRICKS_KEY_SPACE)) {
    struct_car_principal.estat = MAINIMPULSESQ;
    struct_car_principal.temps_espai = 0;
  }
  /*************************************
   * Agafant impuls
   */
  else if ((struct_car_principal.estat == MAINIMPULSESQ ||
            struct_car_principal.estat == MAINIMPULSDRE) &&
           tecla_apretada == BRICKS_KEY_SPACE) {
    // Si copsVsync = 5 temps espai compta fins a 10 a cada segons, 5 segons
    // seran 50 comptats. Serà el màxim
    if (struct_car_principal.temps_espai < 50) {
      struct_car_principal.temps_espai++;
    }
    if (struct_car_principal.temps_espai>4) {
      struct_car_principal.hedefersalt = 1;
      if (struct_car_principal.temps_espai == 5) {
        PutSprite(PLAENERGIA, NUMSPRITEENERGIA, struct_car_principal.pos_x, struct_car_principal.pos_y, 0);
        SetColorPalette(2, 0, 2, 2);
      } else if (struct_car_principal.temps_espai == 15) {
        SetColorPalette(2, 0, 4, 3);
      } else if (struct_car_principal.temps_espai == 30) {
      SetColorPalette(2, 0, 4, 5);
      } else if (struct_car_principal.temps_espai == 49) {
        SetColorPalette(2, 0, 6, 6);
      }
      if (struct_car_principal.sona_efecte == 0) {
        struct_car_principal.sona_efecte = 1;
      }
    }
  } else if ((struct_car_principal.estat == MAINIMPULSESQ) &&
             tecla_apretada != BRICKS_KEY_SPACE) {
    if (struct_car_principal.hedefersalt == 1) {
      struct_car_principal.estat = MAINSALTESQ;
      struct_car_principal.hedefersalt = 0;
    } else {
      struct_car_principal.estat = MAIN1DRE;
    }
    struct_car_principal.sona_efecte = 0;
  } else if ((struct_car_principal.estat == MAINIMPULSDRE) &&
             tecla_apretada != BRICKS_KEY_SPACE) {
    if (struct_car_principal.hedefersalt == 1) {
      struct_car_principal.estat = MAINSALTDRE;
      struct_car_principal.hedefersalt = 0;
    } else {
      struct_car_principal.estat = MAIN1DRE;
    }
    struct_car_principal.sona_efecte = 0;
  }

  /*************************************
   * Baixant cap a la dreta
   */
  if (puc_caure_dreta()==1 && puc_caure_esquerra()==1 &&
      struct_car_principal.estat == BAIXANTDRE) {
    struct_car_principal.pos_y+=4; 
  }
  if ((puc_caure_dreta()==0 || puc_caure_esquerra()==0) && struct_car_principal.estat == BAIXANTDRE ) {
    // Miro si puc continuar baixant, sinó és que ja em puc aturar
    struct_car_principal.estat = MAIN1DRE;
  }
  // detectem quan comencem a caure
  if (puc_caure_dreta()==1 && puc_caure_esquerra()==1 &&
      (struct_car_principal.estat == MAIN1DRE ||
       struct_car_principal.estat == MAIN2DRE)) {
    struct_car_principal.pos_y += 4; 
    struct_car_principal.estat = BAIXANTDRE;
  }

  /*************************************
   * Moviments bàsics dreta
   */
  else if ((struct_car_principal.estat == MAIN1DRE) &&
           (tecla_apretada == BRICKS_KEY_RIGHT)) {
    struct_car_principal.estat = MAIN2DRE;
    if (puc_dreta()==1){
      struct_car_principal.pos_x += 1;
    }
  } else if ((struct_car_principal.estat == MAIN1DRE) &&
             (tecla_apretada == BRICKS_KEY_LEFT)) {
    struct_car_principal.estat = MAIN1ESQ;
    struct_car_principal.pos_x -= 1;
  } else if ((struct_car_principal.estat == MAIN2DRE) &&
             (tecla_apretada == BRICKS_KEY_RIGHT)) {
    struct_car_principal.estat = MAIN1DRE;
    if (puc_dreta()==1) {
      struct_car_principal.pos_x += 1;
    }
  } else if ((struct_car_principal.estat == MAIN2DRE) &&
             (tecla_apretada == BRICKS_KEY_LEFT)) {
    struct_car_principal.estat = MAIN2ESQ;
    struct_car_principal.pos_x -= 1;
  } else if ((struct_car_principal.estat == MAIN1DRE ||
              struct_car_principal.estat == MAIN2DRE) &&
             (tecla_apretada == BRICKS_KEY_SPACE)) {
    struct_car_principal.estat = MAINIMPULSDRE;
    struct_car_principal.temps_espai = 0;
  }
  /*************************************
   * saltant 
   */
  else if (struct_car_principal.estat == MAINSALTESQ) {
    // Hem de fer els càlculs de les y a pujar pel canvi d'estat
    // Podrà saltar 3 maons (16px) = 48 en blocs de 4 fan 12 iteracions de 4
    // px; 50/12 = 4
    //
    PutSprite(PLAENERGIA, NUMSPRITEENERGIA, 255, 0, 0);
    if (struct_car_principal.temps_espai > 4) {
      struct_car_principal.temps_espai -= 4;
      struct_car_principal.pos_y -= 4;
    } else if (struct_car_principal.temps_espai <= 4) {
      struct_car_principal.temps_espai = 0;
      if (puc_esquerra()==1){
        struct_car_principal.estat = BAIXANTESQ;
        // Per borrar-lo podria posar l'sprite a 0 o enviar-lo fora la pantalla que és el que faré
      } else {
        struct_car_principal.estat = MAIN1ESQ;
      }
      if (puc_esquerra()) {
        struct_car_principal.pos_x -= 5;
      }
    }
  } else if (struct_car_principal.estat == MAINSALTDRE) {
    PutSprite(PLAENERGIA, NUMSPRITEENERGIA, 255,0,0); // El borro. L'havia fet que seguís el personatge, però s'haurien d'adaptar a l'sprite del salt i no al de l'impuls
    if (struct_car_principal.temps_espai > 4) {
      struct_car_principal.pos_y -= 4;
      struct_car_principal.temps_espai -= 4;
      test = 10;
    } else if (struct_car_principal.temps_espai <= 4) {
      struct_car_principal.temps_espai = 0;
      struct_car_principal.pos_x -= 12; // No és el mateix quan estic saltan per veure a on caic que quan estic caminant i vull veure a on vaig, per això faig aquest offset
      if (puc_caure_dreta() == 1 && puc_caure_esquerra()==1) {
        struct_car_principal.estat = BAIXANTDRE;
        test = 11;
      } else {
        struct_car_principal.estat = MAIN1DRE;
        test=12;
      }
      struct_car_principal.pos_x += 12;
      if (puc_dreta() == 1) {
        struct_car_principal.pos_x += 5;
      }
    }
  }

  /*************************************
   * Dibuixem sprite
   */
  PutSprite(PLACARACTERPRIN, sprites_estats[struct_car_principal.estat],
            struct_car_principal.pos_x, struct_car_principal.pos_y, 0);
  PutSprite(PLACONTORNCARPRIN, sprites_estats[struct_car_principal.estat+1],
            struct_car_principal.pos_x, struct_car_principal.pos_y, 0);
  // Posar els colors que toquen a aquest pla
  SetSpriteColors(PLACARACTERPRIN, &paleta_patrons[sprites_estats[struct_car_principal.estat]*4]);
  SetSpriteColors(
      PLACONTORNCARPRIN,
      &paleta_patrons[sprites_estats[struct_car_principal.estat+1] * 4]);
  // He de tornar l'array a un subarray. Serveix només el punter

  // Si l'Sprite arriba a dalt de tot faig la seqüència del suïcidi per tornar a
  // començar
  if (struct_car_principal.pos_y < 12) {
    struct_car_principal.estat = SUICIDI;
    suicidi();
  }
}

void calcula_pos_car_secundari() {
  // Llegim les tecles
  unsigned char linia_A = GetKeyMatrix(2);
  unsigned char linia_D = GetKeyMatrix(3);
  unsigned char linia_W_X = GetKeyMatrix(5);
  unsigned char linia_Shift = GetKeyMatrix(6);

  /*************************************
   * BAixant cap a l'esquerra
   */
  // Primer de tot mirem si estem caient
  if (puc_caure_dreta_mirantEsq_gat()==1 && puc_caure_esquerra_gat()==1 &&
      struct_car_secundari.estat == BAIXANTESQ) {
    struct_car_secundari.pos_y+=4; 
  }
  if ((puc_caure_dreta_mirantEsq_gat() == 0 || puc_caure_esquerra_gat() == 0) &&
      struct_car_secundari.estat == BAIXANTESQ) {
    // Miro si puc continuar baixant, sinó és que ja em puc aturar
    struct_car_secundari.estat = MAIN1ESQ;
  }
  if (puc_caure_dreta_mirantEsq_gat()==1 && puc_caure_esquerra_gat()==1 &&
      (struct_car_secundari.estat == MAIN1ESQ ||
       struct_car_secundari.estat == MAIN2ESQ)) {
    // He posat +2 i +4 perquè hi ha el punt entre mig dels maons que és negre
    struct_car_secundari.pos_y += 4; // Hi havia 16 a l'original, he posat només
                                     // 4 per seguir com fa el salt
    struct_car_secundari.estat = BAIXANTESQ;
  }

  /******************************************
   *** Moviments bàsics esquerra
   */
  if ((struct_car_secundari.estat == MAIN1ESQ) &&
      ((linia_A & BRICKS_KEY_A) == 0)) {
    struct_car_secundari.estat = MAIN2ESQ;
    // L'estat ja m'hauria d'indicar quina animació he d'agafar
    // i fer el PutSprite al final de tot comú a tothom
    if (puc_esquerra_gat() == 1) {
      struct_car_secundari.pos_x += -1;
    }
  } else if ((struct_car_secundari.estat == MAIN1ESQ) &&
             ((linia_D & BRICKS_KEY_D) == 0)) {
    struct_car_secundari.estat = MAIN1DRE;
    struct_car_secundari.pos_x += 1;
  } else if ((struct_car_secundari.estat == MAIN2ESQ) &&
             ((linia_A & BRICKS_KEY_A) == 0)) {
    struct_car_secundari.estat = MAIN1ESQ;
    if (puc_esquerra_gat() == 1) {
      struct_car_secundari.pos_x += -1;
    }
  } else if ((struct_car_secundari.estat == MAIN2ESQ) &&
             ((linia_A & BRICKS_KEY_A) == 0)) {
    struct_car_secundari.estat = MAIN2DRE;
    struct_car_secundari.pos_x += 1;
  } else if ((struct_car_secundari.estat == MAIN1ESQ ||
              struct_car_secundari.estat == MAIN2ESQ) &&
             ((linia_Shift & BRICKS_KEY_SHIFT)==0)) {
    struct_car_secundari.estat = MAINIMPULSESQ;
    struct_car_secundari.temps_espai = 0;
  }
  /*************************************
   * Agafant impuls
   */
  else if ((struct_car_secundari.estat == MAINIMPULSESQ ||
            struct_car_secundari.estat == MAINIMPULSDRE) &&
           (linia_Shift & BRICKS_KEY_SHIFT) == 0) {
    // Si copsVsync = 5 temps espai compta fins a 10 a cada segons, 5 segons
    // seran 50 comptats. Serà el màxim
    if (struct_car_secundari.temps_espai < 50) {
      struct_car_secundari.temps_espai++;
    }
    if (struct_car_secundari.temps_espai>4) {
      struct_car_secundari.hedefersalt = 1;
      if (struct_car_secundari.temps_espai == 5) {
        PutSprite(PLAENERGIAGAT, NUMSPRITEENERGIA, struct_car_secundari.pos_x, struct_car_secundari.pos_y, 0);
        SetColorPalette(2, 0, 2, 2);
      } else if (struct_car_secundari.temps_espai == 15) {
        SetColorPalette(2, 0, 4, 3);
      } else if (struct_car_secundari.temps_espai == 30) {
        SetColorPalette(2, 0, 4, 5);
      } else if (struct_car_secundari.temps_espai == 49) {
        SetColorPalette(2, 0, 6, 6);
      }
      if (struct_car_secundari.sona_efecte == 0) {
        struct_car_secundari.sona_efecte = 1;
      }
    }
  } else if ((struct_car_secundari.estat == MAINIMPULSESQ) &&
             (linia_Shift & BRICKS_KEY_SHIFT) == 1) {
    if (struct_car_secundari.hedefersalt == 1) {
      struct_car_secundari.estat = MAINSALTESQ;
      struct_car_secundari.hedefersalt = 0;
    } else {
      struct_car_secundari.estat = MAIN1DRE;
    }
    struct_car_secundari.sona_efecte = 0;
  } else if ((struct_car_secundari.estat == MAINIMPULSDRE) &&
             (linia_Shift & BRICKS_KEY_SHIFT) == 1) {
    if (struct_car_secundari.hedefersalt == 1) {
      struct_car_secundari.estat = MAINSALTDRE;
      struct_car_secundari.hedefersalt = 0;
    } else {
      struct_car_secundari.estat = MAIN1DRE;
    }
    struct_car_secundari.sona_efecte = 0;
  }

  /*************************************
   * Baixant cap a la dreta
   */
  if (puc_caure_dreta_gat()==1 && puc_caure_esquerra_gat()==1 &&
      struct_car_secundari.estat == BAIXANTDRE) {
    struct_car_secundari.pos_y+=4; 
  }
  if ((puc_caure_dreta_gat()==0 || puc_caure_esquerra_gat()==0) && struct_car_secundari.estat == BAIXANTDRE ) {
    // Miro si puc continuar baixant, sinó és que ja em puc aturar
    struct_car_secundari.estat = MAIN1DRE;
  }
  // detectem quan comencem a caure
  if (puc_caure_dreta_gat()==1 && puc_caure_esquerra_gat()==1 &&
      (struct_car_secundari.estat == MAIN1DRE ||
       struct_car_secundari.estat == MAIN2DRE)) {
    struct_car_secundari.pos_y += 4; 
    struct_car_secundari.estat = BAIXANTDRE;
  }

  /*************************************
   * Moviments bàsics dreta
   */
  else if ((struct_car_secundari.estat == MAIN1DRE) &&
           ((linia_D & BRICKS_KEY_D) == 0)) {
    struct_car_secundari.estat = MAIN2DRE;
    if (puc_dreta_gat()==1){
      struct_car_secundari.pos_x += 1;
    }
  } else if ((struct_car_secundari.estat == MAIN1DRE) &&
             ((linia_A & BRICKS_KEY_A) == 0)) {
    struct_car_secundari.estat = MAIN1ESQ;
    struct_car_secundari.pos_x -= 1;
  } else if ((struct_car_secundari.estat == MAIN2DRE) &&
             ((linia_D & BRICKS_KEY_D) == 0)) {
    struct_car_secundari.estat = MAIN1DRE;
    if (puc_dreta_gat()==1) {
      struct_car_secundari.pos_x += 1;
    }
  } else if ((struct_car_secundari.estat == MAIN2DRE) &&
             ((linia_A & BRICKS_KEY_A) == 0)) {
    struct_car_secundari.estat = MAIN2ESQ;
    struct_car_secundari.pos_x -= 1;
  } else if ((struct_car_secundari.estat == MAIN1DRE ||
              struct_car_secundari.estat == MAIN2DRE) &&
             ((linia_Shift &BRICKS_KEY_SHIFT)==0)) {
    struct_car_secundari.estat = MAINIMPULSDRE;
    struct_car_secundari.temps_espai = 0;
  }
  /*************************************
   * saltant 
   */
  else if (struct_car_secundari.estat == MAINSALTESQ) {
    // Hem de fer els càlculs de les y a pujar pel canvi d'estat
    // Podrà saltar 3 maons (16px) = 48 en blocs de 4 fan 12 iteracions de 4
    // px; 50/12 = 4
    //
    PutSprite(PLAENERGIAGAT, NUMSPRITEENERGIA, 255, 0, 0);
    if (struct_car_secundari.temps_espai > 4) {
      struct_car_secundari.temps_espai -= 4;
      struct_car_secundari.pos_y -= 4;
    } else if (struct_car_secundari.temps_espai <= 4) {
      struct_car_secundari.temps_espai = 0;
      if (puc_esquerra_gat()==1){
        struct_car_secundari.estat = BAIXANTESQ;
        // Per borrar-lo podria posar l'sprite a 0 o enviar-lo fora la pantalla que és el que faré
      } else {
        struct_car_secundari.estat = MAIN1ESQ;
      }
      if (puc_esquerra_gat()) {
        struct_car_secundari.pos_x -= 5;
      }
    }
  } else if (struct_car_secundari.estat == MAINSALTDRE) {
    PutSprite(PLAENERGIAGAT, NUMSPRITEENERGIA, 255,0,0); // El borro. L'havia fet que seguís el personatge, però s'haurien d'adaptar a l'sprite del salt i no al de l'impuls
    if (struct_car_secundari.temps_espai > 4) {
      struct_car_secundari.pos_y -= 4;
      struct_car_secundari.temps_espai -= 4;
      test = 10;
    } else if (struct_car_secundari.temps_espai <= 4) {
      struct_car_secundari.temps_espai = 0;
      struct_car_secundari.pos_x -= 12; // No és el mateix quan estic saltan per veure a on caic que quan estic caminant i vull veure a on vaig, per això faig aquest offset
      if (puc_caure_dreta_gat() == 1 && puc_caure_esquerra_gat()==1) {
        struct_car_secundari.estat = BAIXANTDRE;
      } else {
        struct_car_secundari.estat = MAIN1DRE;
      }
      struct_car_secundari.pos_x += 12;
      if (puc_dreta_gat() == 1) {
        struct_car_secundari.pos_x += 5;
      }
    }
  }

  /*************************************
   * Dibuixem sprite
   */
  /* PutSprite(PLAGAT, sprites_estats[struct_car_secundari.estat], */
  /*           struct_car_secundari.pos_x, struct_car_secundari.pos_y, 0); */
  /* PutSprite(PLAGAT, sprites_estats[struct_car_secundari.estat+1], */
  /*           struct_car_secundari.pos_x, struct_car_secundari.pos_y, 0); */
  PutSprite(PLAGAT, NUMSPRITEGAT,
            struct_car_secundari.pos_x, struct_car_secundari.pos_y, 0);
  // Posar els colors que toquen a aquest pla
  SetSpriteColors(PLAGAT, &paleta_patrons[sprites_estats[struct_car_secundari.estat]*4]);
  /* SetSpriteColors( */
  /*     PLACONTORNCARPRIN, */
  /*     &paleta_patrons[sprites_estats[struct_car_secundari.estat+1] * 4]); */
}

void calcula_pos_car_secundari_senzill() {
  // Llegim les tecles
  // Farem un sprite senzill per fer de secundari i fer les proves dels teclats
  unsigned char linia_A = GetKeyMatrix(2);
  unsigned char linia_D = GetKeyMatrix(3);
  unsigned char linia_W_X = GetKeyMatrix(5);
  unsigned char linia_Shift = GetKeyMatrix(6);
  if (((linia_A & BRICKS_KEY_A) == 0) && puc_esquerra_gat()) {
    struct_car_secundari.pos_x -= 1;
  }
  if (((linia_D & BRICKS_KEY_D) == 0) && puc_dreta_gat()) {
    struct_car_secundari.pos_x += 1;
  }
  if ((linia_W_X & BRICKS_KEY_W) == 0) {
    struct_car_secundari.pos_y -= 5u;
  }
  if ((linia_W_X & BRICKS_KEY_X) == 0) {
    struct_car_secundari.pos_y += 5u;
  }
}

void init_sprites(void) {
  SpriteReset();
  SpriteSmall();
  Sprite16();
  SpriteOn();

  // He descobert que hi havia una comanda en el Fusion que és Pattern16FlipRam que gira a la dreta o a dalt i a baix un sprite. Així no se'n necessiten tants com els que jo he utilitzat

  SetSpritePattern(0, Sprite32Bytes(main1a), 32); // 0
  SetSpritePattern(4, Sprite32Bytes(main1b), 32); // 1
  SetSpritePattern(8, Sprite32Bytes(main1a_dreta), 32); // 2
  SetSpritePattern(12, Sprite32Bytes(main1b_dreta), 32); //3
  SetSpritePattern(16, Sprite32Bytes(main2a), 32); // 4
  SetSpritePattern(20, Sprite32Bytes(main2b), 32); // 5
  SetSpritePattern(24, Sprite32Bytes(main2a_dreta), 32); // 6
  SetSpritePattern(28, Sprite32Bytes(main2b_dreta), 32); // 7
  SetSpritePattern(32, Sprite32Bytes(main_salt_a), 32); // 8
  SetSpritePattern(36, Sprite32Bytes(main_salt_b), 32); // 9
  SetSpritePattern(40, Sprite32Bytes(main_salt_a_dreta), 32); // 10
  SetSpritePattern(44, Sprite32Bytes(main_salt_b_dreta), 32); // 11
  SetSpritePattern(48, Sprite32Bytes(main_impuls_a), 32); // 12
  SetSpritePattern(52, Sprite32Bytes(main_impuls_b), 32); // 13
  SetSpritePattern(56, Sprite32Bytes(main_impuls_a_dreta), 32); // 14
  SetSpritePattern(60, Sprite32Bytes((unsigned int *)main_impuls_b_dreta), 32); // 15

  SetColorPalette(11, 7, 6, 2);
  SetColorPalette(12, 7, 3, 3);
  SetColorPalette(13, 0, 6, 1);
  SetColorPalette(10, 1, 4, 4);
  SetColorPalette(4, 0, 0, 0);
  SetColorPalette(15, 1, 1, 1);
  SetColorPalette(2, 5, 5, 0); // Energia

  SetSpritePattern(NUMSPRITEMAO, Sprite32Bytes(mao), 32);
  SetSpriteColors(PLAMAO, paleta_mao);

  SetSpritePattern(NUMSPRITEENERGIA, Sprite32Bytes(energia), 32);
  SetSpriteColors(PLAENERGIA, paleta_energia);
  SetSpriteColors(PLAENERGIAGAT, paleta_energia);

  SetSpritePattern(NUMSPRITEGAT, Sprite32Bytes(gat_Sam), 32);
  SetSpriteColors(PLAGAT, paleta_patrons);
}

void FT_SetName( FCB *p_fcb, const char *p_name )  // Routine servant à vérifier le format du nom de fichier
{
  char i, j;
  memset( p_fcb, 0, sizeof(FCB) );
  for( i = 0; i < 11; i++ ) {
    p_fcb->name[i] = ' ';
  }
  for( i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++ ) {
    p_fcb->name[i] =  p_name[i];
  }
  if( p_name[i] == '.' ) {
    i++;
    for( j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.'); j++ ) {
      p_fcb->ext[j] =  p_name[i + j] ;
    }
  }
}


void FT_errorHandler(char n, char *name)            // Gère les erreurs
{
  Screen(0);
  SetColors(15,6,6);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}
 
int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer, unsigned int amplada_linia, unsigned int tamany_buffer)        // Charge les données d'un fichiers
    {
      // El tamany de tamany_buffer és el nombre de bytes que fan les 20 linies. En screen 5 és 2560, però en screen 7 és 2560*2
        int rd=2560;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file  
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, tamany_buffer );  // Read 20 lines of image data (128bytes per line in screen5)
             HMMC(buffer, 0,start_Y,amplada_linia,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

return(1);
}
int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{

  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

int FT_openFile(char *file_name) {
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }
}

void init_pantalla_joc() {
  char mypalette[16 * 4];

  FT_LoadSc5Image("imatge.sc7", 256, LDbuffer,512,BUFFER_SIZE_SC7); // On charge l'umage
  FT_LoadPalette("image.pl7", LDbuffer, mypalette);
  SetPalette((Palette *)mypalette);

  // Copiem la pantalla de joc
  HMMM(0, 256, 0, 0, 48, 212); // és fins a 212 de les y, altrament esborrem
                               // dades de sprites. Sense overscan és 192
  HMMM(49, 256 + 188, 49, 188, 496 - 48, 212 - 188);
  HMMM(497, 256, 497, 0, 511 - 497, 212);

  // L'error de la primera vegada és que com que només copio els laterals, el color del fondo de la resta de pantalla és el 4 i no el 0 que es posa després. Hauré de fer que tot sigui 0
  // Posem les vides i el marcador a 0
  puntuacio = 0;
  posa_puntuacio(puntuacio);
  struct_car_principal.vides = 5;
  pinta_vides(struct_car_principal.vides);
  nivell = 1;
  pinta_numero_level();
}

void esborrar_pantalla_joc() {
  // Anirem copiant els zeros que tinc
  // Hem de borrar des de la casella (49,0) a la (498,187)
  // El vector de zeros per borrar té 450 caràcters, aniré borrant de línia en linia
  for (int k=0; k<188; k++) {
    // HMMC(vectzero, 49, k, 449,1);
    // Copiant de memòria és lent, vaig a provar de VDP a VDP
    HMMM(49,256+186,49,k,449,1);
    // Molt més ràpid. Va perfecte
  }
}

void canvi_nivell() {
  // Esborrem pantalla
  esborrar_pantalla_joc();
  // Esborrem sprites
  // Aturem maons
  // Missatge de nivell resolt o animació maó cau al mig i es tornen a separar.
  pinta_numero_level();
  // Maó del mateix color que el nivell Pintem nova pantalla segons el nivell a
  // on estem
  nivell++;
  struct_mao.velocitat_mao += 2;
  if (nivell==2) {
    // Canviem paleta
    SetColorPalette(9, 5, 2, 5);
    // Dibuixem torra al mig
    HMMM(2, 256, 212, 171, 24, 16);
    HMMM(2, 256, 212+24, 171, 24, 16);
    HMMM(2, 256, 212+24*2, 171, 24, 16);
    HMMM(2, 256, 212+24, 171-16, 24, 16);
    // Augmentem velocitat maons
  } else if (nivell==3) {
    // Canviem paleta
    SetColorPalette(5, 3, 1, 4);
    // Dibuixem 2 torres al mig
    HMMM(2, 256, 212 - 3 * 24, 171, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24, 171, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24 * 2, 171, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24, 171 - 16, 24, 16);
    //
    HMMM(2, 256, 212 + 3 * 24, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24 * 2, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171 - 16, 24, 16);
  } else if (nivell == 4) {
    // Canviem paleta
    SetColorPalette(9, 4, 2, 3);
    // Dibuixem 1 torra al mig més alta que al nivell 2
    // Filera d'abaix
    HMMM(2, 256, 212 - 3 * 24, 171, 24, 16); 
    HMMM(2, 256, 212 - 3 * 24 + 24, 171, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24 * 2, 171, 24, 16);
    HMMM(2, 256, 212, 171, 24, 16);
    HMMM(2, 256, 212 + 24, 171, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24 * 2, 171, 24, 16);
    // Filera de dalt
    HMMM(2, 256, 212 - 3 * 24 + 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24 * 2, 171 - 16, 24, 16);
    HMMM(2, 256, 212, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 3 * 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171 - 16, 24, 16);
    // Filera de més amunt
    HMMM(2, 256, 212 - 3 * 24 + 24 * 2, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212 + 24, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212 + 3 * 24, 171 - 16*2, 24, 16);
    // Filera de més a més amunt
    HMMM(2, 256, 212, 171 - 16 * 3, 24, 16);
    HMMM(2, 256, 212 + 24, 171 - 16 * 3, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171 - 16 * 3, 24, 16);
    // Filera de més a més a més amunt
    HMMM(2, 256, 212 + 24, 171 - 16 * 4, 24, 16);
  } else if (nivell == 5) {
    // Canviem paleta
    SetColorPalette(5, 4, 2, 4);
    // Columnes de 2 tot recte amb altres de 2 columnes esporàdiques
    // plataforma d'alçada 2
    // Filera d'abaix
    HMMM(2, 256, 212 - 3 * 24, 171, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24, 171, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24 * 2, 171, 24, 16);
    HMMM(2, 256, 212, 171, 24, 16);
    HMMM(2, 256, 212 + 24, 171, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24 * 2, 171, 24, 16);
    // 2a filera
    HMMM(2, 256, 212 - 3 * 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 - 3 * 24 + 24 * 2, 171 - 16, 24, 16);
    HMMM(2, 256, 212, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 3 * 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171 - 16, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24 * 2, 171 - 16, 24, 16);
    // 3a filera
    HMMM(2, 256, 212 - 3 * 24 + 24, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171 - 16*2, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171 - 16*2, 24, 16);
    // 4a filera
    HMMM(2, 256, 212 - 3 * 24 + 24, 171 - 16 * 3, 24, 16);
    HMMM(2, 256, 212, 171 - 16 * 3, 24, 16);
    HMMM(2, 256, 212 + 24 * 2, 171 - 16 * 3, 24, 16);
    HMMM(2, 256, 212 + 3 * 24 + 24, 171 - 16 * 3, 24, 16);
  }
  // Col·loquem personatges
  struct_car_principal.pos_x = (char)234;
  struct_car_principal.pos_y = (char)(16 * 10 - 1);
  struct_car_principal.estat = MAIN1ESQ;
  struct_car_principal.hedefersalt = 0;
  struct_car_principal.sona_efecte = 0;
  struct_car_principal.collisionat = 0;
  struct_car_secundari.pos_x = 28;
  struct_car_secundari.pos_y = (char)(16 * 11 - 5);
  struct_car_secundari.estat = MAIN1ESQ;
  PutSprite(PLAGAT, NUMSPRITEGAT, struct_car_secundari.pos_x,
            struct_car_secundari.pos_y, 0);
  // Augmentem velocitat de caiguda dels maons
  temps_entre_maons--;
  // Actualitzem la informació del level
  pinta_numero_level();
}

void gameOver() {
  // Borrar sprites
  SpriteOff();
  struct_mao.continuem_generant_maons=0;
  EndMyInterruptHandler();

  // Posar la pantalla en negre. Copiarem el bloc en negre a la VRAM i
  // després copiarem de VRAM a VRAM
  HMMC(vectzero, 0, 0, 16, 16);
  for(int k=1;k<32;k++){
    HMMM(0,0,k*16,0,16,16);
  }
  for (int kk=1;kk<14;kk++){
    for (int k = 0; k < 32; k++) {
      HMMM(0, 0, k * 16, kk*16, 16, 16);
    }
  }

  // Canviar la música. Potser podria fer un fade out dels canals
  // Per fer un fade és al registre 30, però necessito saber quin instrument està
  // sonant
  apaguemOPLL();
  FT_openFile("badend1.mbm");
  fcb_read(&file, songFile, sizeof(songFile));
  ompleCapcaleraPatrons();
  InicialitzemCanalsReproductor();
  tempo = capcalera.start_tempo;

  InitializeMyInterruptHandler((int)main_loop, 1);
  // Copiar els caràcters de Game Over
  // Els caràcters van del 289-86, fan 203 d'amplada, 512-203=309px espai lliure, entre dos fan 154
  // GAM
  HMMM(52, 256 + 70, 154, 40, 162, 55);
  // E
  HMMM(156, 256 + 135, 154 + 162, 40, 47, 52);
  // OVER
  HMMM(52, 256 + 129, 154, 40+55, 208, 55);

  /* // Esperar un cert temps i/o esperar una tecla */
  WAIT(300);
  KillKeyBuffer();
  WaitKey();
  //Tornem a pantalla inicial
  // Parem joc
  EndMyInterruptHandler();
  apaguemOPLL();
  //StopFX(); // Fa aturar tot el banc d'efectes, després ja no sona
  for (int i = 8; i < 11; i++) PSGwrite(i, 0);  // Silencia el PSG, semblant al SilencePSG però no em compilava
  RestorePalette();
  estat_pantalla = PANT_INICIAL;
}

void suicidi(void) {
  // Animació de suïcidi i efecte sonor
  // Copiem les dades dels dos sprites a on toquen.
  // Provaré sobreescrivint el primer patró
  if ((struct_car_principal.estat == MAIN1ESQ) ||
      (struct_car_principal.estat == MAIN2ESQ) ||
      (struct_car_principal.estat == MAINSALTESQ) ||
      (struct_car_principal.estat == MAINIMPULSESQ) ||
      (struct_car_principal.estat == BAIXANTESQ)) {
    SetSpritePattern(0, Sprite32Bytes(Mort_a), 32);
    SetSpritePattern(4, Sprite32Bytes(Mort_b), 32);
  }
  else {
    // Si estàvem a estat mirant dreta hem de girar les dades i canviar els
    // patrons
    SetSpritePattern(0, Sprite32Bytes(Mort_a_dreta), 32);
    SetSpritePattern(4, Sprite32Bytes(Mort_b_dreta), 32);
  }

  struct_car_principal.estat = SUICIDI;

  PutSprite(PLACARACTERPRIN, 0, struct_car_principal.pos_x,
            struct_car_principal.pos_y, 0);
  PutSprite(PLACONTORNCARPRIN, 4, struct_car_principal.pos_x,
            struct_car_principal.pos_y, 0);

  // Efecte sonor
  PlayFX(8);
  WAIT(50);

  struct_car_principal.vides--;
  if (struct_car_principal.vides==0) {
    // Fem el Game Over
    gameOver();
  }
  else {
    pinta_vides(struct_car_principal.vides);
    nivell--; // El canvi de nivell afegeix un, així continuem amb el mateix
    // Tornem els sprites de caràcter principal en moviment
    SetSpritePattern(0, Sprite32Bytes(main1a), 32); // 0
    SetSpritePattern(4, Sprite32Bytes(main1b), 32); // 1
    /* SetSpriteColors(PLACARACTERPRIN, &paleta_patrons[0]); */
    /* SetSpriteColors(PLACONTORNCARPRIN, &paleta_patrons[16]); */

    struct_mao.velocitat_mao -=2;
    canvi_nivell(); // Cada cop que em suïcido els maons van més ràpids ja que hi ha el canvi de nivell, he de tornar a enrere també la velocitat
  }
}

void animacio_pantalla_inicial(void) {
  char mypalette[] = {
    0, 0,0,0,
    1, 1,0,1,
    2, 1,1,2,
    3, 2,1,1,
    4, 3,1,1,
    5, 1,2,3,
    6, 2,3,4,
    7, 5,0,3,
    8, 6,1,0,
    9, 5,3,2,
    10, 7,5,2,
    11, 7,6,2,
    12, 6,6,6,
    13, 7,6,5,
    14, 6,7,7,
    15, 7,7,7
  };

  Screen(5);

  FT_LoadSc5Image("sergi.SC5", 256, LDbuffer,256,BUFFER_SIZE_SC5);

  SetSC5Palette((Palette *)mypalette);

  // Música
  FT_openFile("IM_LOST.MBM");
  fcb_read(&file, songFile, sizeof(songFile));
  ompleCapcaleraPatrons();
  InicialitzemCanalsReproductor();
  tempo = capcalera.start_tempo;

  InitializeMyInterruptHandler((int)main_loop, 1);

  // Brick dreta
  //HMMM(unsigned int SX, unsigned int SY, unsigned int DX, unsigned int DY, unsigned int NX, unsigned int NY)
  for(int k=0;k<POS_Y_MAONS;k=k+16) {
    HMMM(0, 256 + 128, 80 + 32 * 2, k, 32, 32);
    WAIT(TEMPS_CAIGUDA_MAONS);
    HMMM(200, 256, 80 + 32 * 2, k, 32, 32);
  }
  HMMM(0, 256 + 128, 80 + 32 * 2, POS_Y_MAONS, 32, 32);
  // Brick central
  for (int k = 0; k < POS_Y_MAONS; k = k + 16) {
    HMMM(0, 256 + 128, 80 + 32, k, 32, 32);
    WAIT(TEMPS_CAIGUDA_MAONS);
    HMMM(200, 256, 80 + 32, k, 32, 32);
  }
  HMMM(0, 256 + 128, 80 + 32, POS_Y_MAONS, 32, 32);
  // Brick central segon
  for (int k = 0; k < POS_Y_MAONS-32; k = k + 16) {
    HMMM(0, 256 + 128, 80 + 32, k, 32, 32);
    WAIT(TEMPS_CAIGUDA_MAONS);
    HMMM(200, 256, 80 + 32, k, 32, 32);
  }
  HMMM(0, 256 + 128, 80 + 32, POS_Y_MAONS-32, 32, 32);
  // Brick esquerra
  for (int k = 0; k < POS_Y_MAONS; k = k + 16) {
    HMMM(0, 256 + 128, 80 , k, 32, 32);
    WAIT(TEMPS_CAIGUDA_MAONS);
    HMMM(200, 256, 80, k, 32, 32);
  }
  HMMM(0, 256 + 128, 80, POS_Y_MAONS, 32, 32);

  // Animació MAN / Caràcter principal i Sam / Gat
  char pos_x=255;
  int pos_x_gat = -30;
  while (pos_x>195) {
    for(int k=0;k<6;k++) {
      HMMM(k * 32, 256 + 96, pos_x, POS_Y_MAONS, 32, 32);
      if (pos_x_gat<0) {
        // El Man surt de darrera la pantalla de mica en mica, però el gat si el poso a la coordenada 0 surt tot, l'he d'anar copiant de mica en mica
        HMMM((char)((k * 32)-pos_x_gat), 256 + 11, 0, POS_Y_MAONS, (char)(32+pos_x_gat), 32);
      }
      else{
        HMMM(k * 32, 256 + 11, pos_x_gat, POS_Y_MAONS, 32, 32);
      }
      WAIT(TEMPS_CAIGUDA_MAONS);
      HMMM(200, 256, pos_x, POS_Y_MAONS, 32, 32);  // Borrem els quadrats
      HMMM(200, 256, pos_x_gat, POS_Y_MAONS, 32, 32);
      pos_x = pos_x - 4;
      pos_x_gat = pos_x_gat + 4;
    }
  }
  WaitKey();
}

void gamePlay(void) {
  if (struct_mao.processar_pos_mao == 1) {
    if (struct_car_principal.collisionat == 0) {
      if ((struct_car_principal.pos_x + 12 >= struct_mao.pos_x) &&
          (struct_car_principal.pos_x <= struct_mao.pos_x + 8) &&
          (struct_car_principal.pos_y + 0 >= struct_mao.pos_y) &&
          (struct_car_principal.pos_y <= struct_mao.pos_y + 20)) {
        // He posat 0 en lloc de 16 en la 3a condició perquè mai estarà el
        // personatge per sobre d'un maó i feia que la col·lisió fos entrada
        // dos cops
        struct_car_principal.collisionat = 1;
      }
    }
    if (struct_car_secundari.collisionat == 0) {
      if ((struct_car_secundari.pos_x + 12 >= struct_mao.pos_x) &&
          (struct_car_secundari.pos_x <= struct_mao.pos_x + 8) &&
          (struct_car_secundari.pos_y + 0 >= struct_mao.pos_y) &&
          (struct_car_secundari.pos_y <= struct_mao.pos_y + 20)) {
        // He posat 0 en lloc de 16 en la 3a condició perquè mai estarà el
        // personatge per sobre d'un maó i feia que la col·lisió fos entrada
        // dos cops
        struct_car_secundari.collisionat = 1;
      }
    }
    calcula_pos_mao();
    struct_mao.processar_pos_mao = 0;
  }

  unsigned char linia6 = GetKeyMatrix(6);
  if ((linia6 & 128) == 0) {
    // Tecla F3
    // Ens suicidem
    suicidi();
  } else if ((linia6 & 64) == 0) {
    // F2
    SetSpritePattern(0, Sprite32Bytes(main1a), 32); // 0
    SetSpritePattern(4, Sprite32Bytes(main1b), 32); // 1
    PutSprite(0, 0, 120, 120, 0);
    PutSprite(1, 4, 120, 120, 0);
    InitializeMyInterruptHandler((int)main_loop, 1);
  }
  unsigned char linia4 = GetKeyMatrix(4);
  if ((linia4 & 0x20) == 0) {
    // Tecla P
    canvi_nivell();
  }
  if ((linia4 & 0x01) == 0) {
    // Tecla K
    gameOver();
  }

  // Processem moviment caràcter principal
  if (struct_car_principal.processant_moviment == 1) {
    calcula_pos_car_principal(Rkeys());
    calcula_pos_car_secundari();
    struct_car_principal.processant_moviment = 0;
  }

  // Col·lisió caràcter principal
  if (struct_car_principal.collisionat == 1) {
    PlayFX(6);
    // Efecte visual
    // Traiem el personatge d'estar en col·lisió
    struct_car_principal.pos_y -= 40;
    // Hem de posar-ho en estat sense salt si el maó et cau saltant, ja que
    // altrament no baixa i pots saltar més
    if (struct_car_principal.estat == MAINIMPULSDRE ||
        struct_car_principal.estat == MAINSALTDRE) {
      struct_car_principal.estat = MAIN1DRE;
      // Hem d'esborrar les ales
      PutSprite(PLAENERGIA, NUMSPRITEENERGIA, 255, 0, 0);
    } else if (struct_car_principal.estat == MAINIMPULSESQ ||
               struct_car_principal.estat == MAINSALTESQ) {
      struct_car_principal.estat = MAIN1ESQ;
      // Hem d'esborrar les ales
      PutSprite(PLAENERGIA, NUMSPRITEENERGIA, 255, 0, 0);
    }
    struct_car_principal.vides--;
    if (struct_car_principal.vides == 0) {
      gameOver();
    }
    pinta_vides(struct_car_principal.vides);
    struct_car_principal.collisionat = 0;
  }

  // Col·lisió caràcter secundari / gat
  if (struct_car_secundari.collisionat == 1) {
    PlayFX(6);
    // Efecte visual
    // Traiem el personatge d'estar en col·lisió
    struct_car_secundari.pos_y -= 40;
    // Hem de posar-ho en estat sense salt si el maó et cau saltant, ja que
    // altrament no baixa i pots saltar més
    if (struct_car_secundari.estat == MAINIMPULSDRE ||
        struct_car_secundari.estat == MAINSALTDRE) {
      struct_car_secundari.estat = MAIN1DRE;
      // Hem d'esborrar les ales
      PutSprite(PLAENERGIAGAT, NUMSPRITEENERGIA, 255, 0, 0);
    } else if (struct_car_secundari.estat == MAINIMPULSESQ ||
               struct_car_secundari.estat == MAINSALTESQ) {
      struct_car_secundari.estat = MAIN1ESQ;
      // Hem d'esborrar les ales
      PutSprite(PLAENERGIAGAT, NUMSPRITEENERGIA, 255, 0, 0);
    }
    if (estat_pantalla == PANT_JOC_2P) {
      struct_car_principal.vides--;
      if (struct_car_principal.vides == 0) {
        gameOver();
      }
      pinta_vides(struct_car_principal.vides);
    }
    struct_car_secundari.collisionat = 0;
  }

  // Animació energètica
  if (struct_car_principal.estat == MAINIMPULSDRE ||
      struct_car_principal.estat == MAINIMPULSESQ) {
    // Fer un if per controlar els temps i el canvi d'imatge de l'impuls per fer
    // diferents sons
    if (struct_car_principal.temps_espai == 4) {
      PlayFX(3);
    } else if (struct_car_principal.temps_espai == 15) {
      PlayFX(2);
    } else if (struct_car_principal.temps_espai == 30) {
      PlayFX(0);
    } else if (struct_car_principal.temps_espai == 49) {
      PlayFX(4);
    }
  } else if (struct_car_principal.estat == MAINSALTDRE ||
             struct_car_principal.estat == MAINSALTESQ) {
    if (struct_car_principal.sona_efecte == 0) {
      PlayFX(5);
      struct_car_principal.sona_efecte = 1;
    }
  }

  // Animació energètica gat
  if (struct_car_secundari.estat == MAINIMPULSDRE ||
      struct_car_secundari.estat == MAINIMPULSESQ) {
    // Fer un if per controlar els temps i el canvi d'imatge de l'impuls per fer
    // diferents sons
    if (struct_car_secundari.temps_espai == 4) {
      PlayFX(3);
    } else if (struct_car_secundari.temps_espai == 15) {
      PlayFX(2);
    } else if (struct_car_secundari.temps_espai == 30) {
      PlayFX(0);
    } else if (struct_car_secundari.temps_espai == 49) {
      PlayFX(4);
    }
  } else if (struct_car_secundari.estat == MAINSALTDRE ||
             struct_car_secundari.estat == MAINSALTESQ) {
    if (struct_car_secundari.sona_efecte == 0) {
      PlayFX(5);
      struct_car_secundari.sona_efecte = 1;
    }
  }

  // Detecció col·lisió maó amb caràcter secundari
  // El primer l'havia fet a la interrupció del VDP, però pel Telegram del
  // MSX-Lab havien comentat que només utilitzen la interrupció del VDP per la
  // música i el control del teclat Hem de tenir en compte que la posició del
  // maó x és per 2
  if ((struct_car_secundari.pos_x + 12 >= struct_mao.pos_x * 2) &&
      (struct_car_secundari.pos_x <= struct_mao.pos_x * 2 + 12) &&
      (struct_car_secundari.pos_y + 0 >= struct_mao.pos_y) &&
      (struct_car_secundari.pos_y <= struct_mao.pos_y + 30)) {
    // Posem el caràcter just per sobre
    PutSprite(PLAGAT, NUMSPRITEGAT, struct_car_secundari.pos_x,
              struct_car_secundari.pos_y, 0);
    // Avancem el maó perquè no hi hagi col·lisió un altre cop
    struct_mao.pos_y += 16;
  }

  // Ara provaré de fer la detecció dels dos caràcters aquí fora de la
  // interrupció, com abans he fet la del maó
  if ((struct_car_secundari.pos_x + 12 >= struct_car_principal.pos_x) &&
      (struct_car_secundari.pos_x <= struct_car_principal.pos_x + 12) &&
      (struct_car_secundari.pos_y + 16 >= struct_car_principal.pos_y) &&
      (struct_car_secundari.pos_y <= struct_car_principal.pos_y + 16)) {
    // Donem 200 punts
    puntuacio = puntuacio + 200;
    posa_puntuacio(puntuacio);
    // Animació de retrobament
    // canvi de pantalla i de velocitat
    canvi_nivell();
  }
}

void main(void) {
  SetColors(15, 0, 0); // Després del screen7 no té efecte
  Cls();

  // Inicialitzem PSG
  FT_openFile("bricks.afb");
  fcb_read(&file, ayFX_Bank, sizeof(ayFX_Bank));
  InitFX(ayFX_Bank);

  TIME tm;      // Init the Time Structure variable
  GetTime(&tm); // Retreive MSX-DOS Clock, and set the tm strucutre with
  // clock's values
  srand(tm.sec); // use current clock seconds as seed in the random generator

  estat_pantalla = PANT_INICIAL;
  while(estat_pantalla != PANT_ACABEM) {
    if (estat_pantalla == PANT_PINTEMJOC_1P || estat_pantalla == PANT_PINTEMJOC_2P) {
      // Init FM music
      FT_openFile("pep.mbm");
      fcb_read(&file, songFile, sizeof(songFile));
      ompleCapcaleraPatrons();
      InicialitzemCanalsReproductor();
      tempo = capcalera.start_tempo;


      Screen(7);

      init_pantalla_joc();
      init_sprites(); // Ha d'anar després de init_pantalla_joc, si no es matxaquen bytes de VRAM dels sprites.

      struct_car_principal.pos_x = (char)234;
      struct_car_principal.pos_y = (char)(16*10-1);
      struct_car_principal.estat = MAIN1ESQ;
      struct_car_principal.hedefersalt = 0;
      struct_car_principal.sona_efecte = 0;
      struct_car_principal.collisionat = 0;

      struct_mao.pos_x = 22 + (rand() % 19) * 12;
      struct_mao.pos_y = 0;
      struct_mao.temps_mao = 0;
      struct_mao.velocitat_mao = 10; // A 23 sempre es queden a la primera línia. Hauria de revisar els càlculs. A 20 fa només dues línies
      // Hi ha cops que les col·lisions amb la velocitat no els fa cas. Hauré d'estudiar el punt a on passa
      struct_mao.continuem_generant_maons = 1;

      struct_car_secundari.pos_x = 28;
      struct_car_secundari.pos_y = (char)(16 * 11-5);
      struct_car_secundari.estat = MAIN1ESQ;
      struct_car_secundari.hedefersalt = 0;
      struct_car_secundari.sona_efecte = 0;
      struct_car_secundari.collisionat = 0;

      struct_mao.processar_pos_mao=0;
      InitializeMyInterruptHandler((int)main_loop,1);

      if (estat_pantalla == PANT_PINTEMJOC_1P) {
        estat_pantalla = PANT_JOC_1P;
      }
      else if (estat_pantalla == PANT_PINTEMJOC_2P) {
        estat_pantalla = PANT_JOC_2P;
      }
    } else if (estat_pantalla == PANT_JOC_1P || estat_pantalla == PANT_JOC_2P) {
      gamePlay();
    } else if (estat_pantalla == PANT_INICIAL) {
      // Animació inicial

      animacio_pantalla_inicial();

      // Text pantalla de selecció
      Screen(0);

      Locate(5,10);
      printf("1- Un jugador");
      Locate(5,15);
      printf("2- Dos jugadors");
      // Bucle esperem tecles
      char sortir = 0;
      while(!sortir){
        char linia0 = GetKeyMatrix(0);
        if ((linia0 & 2)==0) {
          estat_pantalla = PANT_PINTEMJOC_1P;
          sortir=1;
        }
        else if ((linia0 & 4) == 0) {
          estat_pantalla = PANT_PINTEMJOC_2P;
          sortir = 1;
        }
        char linia6 = GetKeyMatrix(6);
        if (((linia6 & 32) == 0)) {
          // Tecla F1 
          estat_pantalla = PANT_ACABEM;
          sortir = 1;
        }
      }
      EndMyInterruptHandler();
    }
  }

  apaguemOPLL();
  StopFX();
  for (int i = 8; i < 11; i++) PSGwrite(i, 0);  // Silencia el PSG, semblant al SilencePSG però no em compilava

  Point1 = nivell;
  Point2 = pos_nums[nivell];
  Point3 = pos_nums[nivell+1];

  Screen(0);
  printf("\nEstat: %d,%d,%d, %d\n", struct_car_principal.pos_x,
         struct_car_secundari.pos_y, struct_mao.pos_x,
         struct_mao.pos_y);
  printf("P1: %d\n", Point1);
  printf("P2: %d\n", Point2);
  printf("P3: %d\n", Point3);
  printf("Test: %d\n", test);
  printf("Mao: %d,%d\n", struct_mao.pos_x, struct_mao.pos_y);
  printf("Secunda: %d,%d\n", struct_car_secundari.pos_x, struct_car_secundari.pos_y);
  RestorePalette();
  Locate(0,80);
  printf("Gracies per jugar!!!");
  Locate(0,120);
  Exit(0);
}

// Al nivell 2 ja van molt ràpids els maons. Potser ja no hauria de tocar la velocitat per nivell 2023-01-14
