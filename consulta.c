#include "fusion-c/header/msx_fusion.h"
#include "fusion-c/header/psg.h"
#include "fusion-c/header/vdp_graph1.h"
#include "fusion-c/header/vdp_sprites.h"
#include "fusion-c/header/pt3replayer.h"
#include "fusion-c/header/vars_msxSystem.h"
#include "fusion-c/header/pt3replayer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



static const unsigned int mao[] =
  {
   0b0111111101111111,
   0b0111111101111111,
   0b0111111101111111,
   0b0111111101111111,
   0b0000000000000000,
   0b0111011111110111,
   0b0111011111110111,
   0b0111011111110111,
   0b0111011111110111,
   0b0000000000000000,
   0b0111111101111111,
   0b0111111101111111,
   0b0111111101111111,
   0b0111111101111111,
   0b0000000000000000,
   0b0000000000000000
  };
static const unsigned int prota[]=
  {
   0b0000001100000000,
   0b0000011110000000,
   0b0000011010000000,
   0b0000011110000000,
   0b0000001011000000,
   0b0000111111100000,
   0b0001111110100000,
   0b0010011110100000,
   0b0010011110100000,
   0b0001111110000000,
   0b0000011110000000,
   0b0000010110000000,
   0b0000010011000000,
   0b0000110011000000,
   0b0000000011000000,
   0b0000000000000000
  };

int xMan = 100;
int yMan = 100;
int xMao = 20;
int yMao = 20;
char collisio = 0;
char sortir_bucle = 0;
int interrupcions = 0;
int v_interrupcions = 0;
char copsVsync = 0;
char semaforCollisions = 0;
FCB file;

static char my_interrupt(void) {
  DisableInterrupt();
  if(IsVsync()==1){
    if(copsVsync>1) {
    unsigned char linia8 = KeyboardRead();
    if (linia8 == 128) {
      xMan = xMan + 16;
    } else if (linia8 == 16) {
      xMan = xMan - 16;
    } else if (linia8 == 64) {
      yMan = yMan + 16;
    } else if (linia8 == 32) {
      yMan = yMan - 16;
    } else if (linia8 == 1) {
      sortir_bucle = 1;
    }
    PutSprite(1, 4, xMan, yMan, 10);
    // detectem col·lisió
    if (xMan == xMao && yMan == yMao && semaforCollisions == 0){
      collisio = 1;
      semaforCollisions = 1;
    }
    copsVsync=0;
    }
    copsVsync++;
    v_interrupcions++;
  }
  interrupcions++;
  EnableInterrupt();
  return (1);
}

/* ---------------------------------
                FT_SetName

    Set the name of a file to load
                (MSX DOS)
-----------------------------------*/
void FT_SetName(FCB *p_fcb, const char *p_name) {
  char i, j;
  memset(p_fcb, 0, sizeof(FCB));
  for (i = 0; i < 11; i++) {
    p_fcb->name[i] = ' ';
  }
  for (i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++) {
    p_fcb->name[i] = p_name[i];
  }
  if (p_name[i] == '.') {
    i++;
    for (j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.');
         j++) {
      p_fcb->ext[j] = p_name[i + j];
    }
  }
}
/* ---------------------------------
          FT_LoadData
  Load Data to a specific pointer
  size is the size of data to read
  skip represent the number of Bytes
  you want to skip from the begining of the file
  Example: skip=7 to skip 7 bytes of a MSX bin
-----------------------------------*/
int FT_LoadData(char *file_name, char *buffer, int size, int skip) {
  FT_SetName(&file, file_name);
  fcb_open(&file);
  if (skip > 0) {
    fcb_read(&file, buffer, skip);
  }

  fcb_read(&file, buffer, size);

  fcb_close(&file);

  return (0);
}

int main(void) {
  // Preparem la part d'audio extreta del Pt3Player-MSX2_Demo.c
  InitPSG();
  const char *nom_fitxer = "Music2.pt3";
  int size = 2501;
  unsigned char song[9000];
  FT_SetName(&file, nom_fitxer);
  fcb_open(&file);
  FT_LoadData(nom_fitxer, song, size, 0);
  PT3Init(song+99, 0);

  Screen(2);
  SpriteReset();
  SpriteSmall();

  Sprite16();
  SetSpritePattern(0, Sprite32Bytes(mao), 32);
  SetSpritePattern(4, Sprite32Bytes(prota), 32);
  FX fx01;
  fx01.isTone = true;
  fx01.isNoise = false;
  fx01.Tone = 1024;
  fx01.Noise = 0;
  fx01.Period = 3200;
  fx01.Shape = 8;
  Cls();
  PutSprite(1, 4, 100, 100, 10);
  PutSprite(0,0,xMao,yMao,8);
  InitInterruptHandler();

  SetInterruptHandler(my_interrupt);
  while(!sortir_bucle){
    PT3Rout();
    PT3Play();
    if (collisio==1) {
      // Efectes de xoc
      SoundFX(0, &fx01);
      // llums de colors en els sprites
      for (int xx = 2; xx < 15; xx = xx + 2) {
        PutSprite(1, 4, xMan, yMan, xx + 1);
        PutSprite(0, 0,xMao,yMao, xx);
      }
      SilencePSG();
      xMan = xMan + 16;
      PutSprite(1, 4, xMan, yMan, 10);
      collisio=0;
      semaforCollisions = 0;
    }
    Halt();
    unsigned char linia8 = KeyboardRead();
    if (linia8 == 1) {
      sortir_bucle = 1;
    }
  }
  Screen(0);
  Cls();
  printf("Inter: %d\n", interrupcions);
  printf("V-Interr: %d\n", v_interrupcions);
  EndInterruptHandler();
  Exit(0);

  /* El bombjack usa:
     //----------- Wait some times ------------------
char FT_wait(int cicles)
{
  int i;
  for(i=0;i<cicles;i++)
  {
    EnableInterrupt();
    Halt();
  }
  return(0);
}
I va esperant un cert temps
  */
}
