// Partint del player pro.c intentaré tocar un pro file fet amb el protracker

// Parts extretes del pro.h

#include <stdint.h>

#define MAX_SONG_LENGTH 99

#define SONG_NAME_LENGTH 16
#define AUTHOR_LENGTH 16

#define NR_OF_CHANNELS 9
#define NR_OF_FM_CHANNELS 6
#define NR_OF_DRUM_CHANNELS 3

#define NR_OF_LINES 64
#define LINE_SIZE 8

#define NR_OF_VOICE_PATCHES 15
#define NR_OF_DRUM_PATCHES  3
#define INSTRUMENT_LENGTH   8

#define PAN_SETTING_LEFT  0x10
#define PAN_SETTING_MID   0x30
#define PAN_SETTING_RIGHT 0x20

#define NOTE_ON             0x60    /* 0x01 - 0x60 */
#define INSTRUMENT_CHANGE   0x61    /* 0x61 - 0x7F */
#define NOTE_OFF            0x80    /* 0x80        */
#define SLIDE_UP            0x81    /* 0x81 - 0x9F */
#define RELEASE             0xA0    /* 0xA0        */
#define SLIDE_DOWN          0xA1    /* 0xA1 - 0xBF */
#define VOLUME_CHANGE       0xC0    /* 0xC0 - 0xCF */

#define FLIP                0xE0    /* 0xE0 - 0xE7, 0xE8 - 0xEF */
#define PITCH               0xF0    /* 0xF0 - 0xF7, 0xF8 - 0xFF */

#define DATA_SEGMENT_BASE 0x8000
#define DATA_SEGMENT_SIZE 0x4000

#define READ_BUFFER 0xC000
#define READ_BUFFER_SIZE 0x1000

typedef struct
{
    char signature[4];
    char song_name[SONG_NAME_LENGTH];
    char author[AUTHOR_LENGTH];
  uint8_t original_instruments[15][16]; // Comença al 24
  uint8_t instrument_volumes[32]; // Comença al 114
  uint8_t start_instruments[6]; // Comença al 134
  uint8_t speed; // 13A
  uint8_t drum_frequencies[6]; // 13B-140
  uint8_t drum_volume; // 141
  uint8_t song_length; //142
  uint8_t unknown[2]; // 143-144
} PRO_HEADER;

typedef struct
{
    bool update;
    uint8_t instrument;
    uint8_t volume;
} INSTRUMENT_DATA;

typedef struct 
{
    uint8_t low;
    uint8_t high;
} PRO_FREQUENCY;

typedef enum
{
    SLIDE_TYPE_OFF,
    SLIDE_TYPE_UP,
    SLIDE_TYPE_DOWN
} SLIDE_TYPE;

typedef struct
{
    SLIDE_TYPE type;
    uint16_t   value;
    uint8_t    remainder;
} SLIDE_DATA;

typedef struct 
{
    uint8_t counter;
    uint8_t value;
} FLIP_DATA;

typedef struct
{
    INSTRUMENT_DATA instrument_data;
    SLIDE_DATA      slide_data;
    FLIP_DATA       flip_data;

    PRO_FREQUENCY   frequency;
    uint8_t         note;
    uint8_t         pitch_value;

} CHANNEL_DATA;

static uint8_t *g_pattern_data;

void main() {
  // PRimer de tot hem de llegir el fitxer
  // Suposo que al principi del fitxer hi ha la configuració dels instruments, volums, drums, etc. Tal com es feia al principi del registre.
  // Si faig el protracker també hauré de descomprimir el loader que hi ha
  // Comença fent un load i després el rewind que és el primer pas
  g_pattern_data = (uint8_t *)DATA_SEGMENT_BASE;
  // Primer llegim capçalera
  // Fa un rewind per deixar tots els punters de memòria
  // Després fa un update_registers per configurar-los. He de mirar dins el fitxer a on està aquesta informació de les freqüències i els ritmes
  // Comparar diferents fitxers
}

