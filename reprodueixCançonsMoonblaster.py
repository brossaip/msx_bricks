# Fitxer que busca els mbm i els posa al directori disquet_1f per ser reproduït pel moonblaster
# El pid del procés de l'openMSX ja s'ha d'haver trobat
# L'identificador de la finestra de l'openMSX pel xdotool també. Havia intentat buscar-lo amb el Popen però no me n'he sortit
# L'openMSX ha de tenir en el disquet b el directori disquet_1f. I ja ha d'haver estat seleccionat el drive B pel load song
# Passant de l'xdotool. Hi ha una comanda en l0penmsx que es keymatrixdown/keymatrixup
import time
import socket
import glob
import shutil
import os

# process = subprocess.Popen(['xdotool','search','--class','openmsx'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
# out,err = process.communicate()
# print(out)

# Per trobar el pid fem pidof openmsx. També podem fer un ls de /tmp/openmsx-jepsuse/socket.
pid_openMSX = '8088'

# ******************************
# Aquest era pel moonblaster 1.4 amb el music module
# ******************************
fitxers = sorted(glob.glob('/home/jepsuse/MSX/MUSICPAC/*MBM'))
numPorto = 1;
for fitxer in fitxers:
    s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    s.connect('/tmp/openmsx-jepsuse/socket.' + pid_openMSX) # A la canço 20 es penjava. Vaig prova de reconnectar-ho a cada moment
    print(f"Cançó {fitxer}. Número {numPorto} de {len(fitxers)}")
    [path, nomfitx]=os.path.split(fitxer)
    fitxmbms = glob.glob(fitxer[0:-3]+'*')
    for fitxmbm in fitxmbms:
        [pathmbm, nommbm] = os.path.split(fitxmbm)
        shutil.copy(fitxmbm,pathmbm+'/disquet_1f')

    s.send(bytes("<command>keymatrixdown 7 2</command>","utf-8")) # F4
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 7 2</command>","utf-8"))
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixdown 8 1</command>","utf-8")) # Espai - Seleccionem Load
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 8 1</command>","utf-8"))
    time.sleep(1)
    s.send(bytes("<command>keymatrixdown 8 1</command>","utf-8")) # Espai - Seleccionem la cançó
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 8 1</command>","utf-8"))
    time.sleep(6)
    s.send(bytes("<command>keymatrixdown 7 4</command>","utf-8")) # Esc - Sortim de la càrrega
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 7 4</command>","utf-8"))
    time.sleep(0.5)
    # Comencem a gravar
    s.send(bytes("<command>record start " + nommbm + "</command>","utf-8"))
    time.sleep(0.1)
    s.send(bytes("<command>keymatrixdown 6 32</command>","utf-8")) # F1 - Comença la música
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 6 32</command>","utf-8"))
    time.sleep(180)
    s.send(bytes("<command>record stop</command>","utf-8"))
    # Sortim de la cançó si encara està sonant, prement esc
    s.send(bytes("<command>keymatrixdown 7 4</command>","utf-8")) # Esc - Sortim de la reproducció
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 7 4</command>","utf-8"))
    time.sleep(0.5)
    for fitxmbm in fitxmbms:
        [pathmbm, nommbm] = os.path.split(fitxmbm)
        os.remove(pathmbm+'/disquet_1f/' + nommbm)
    numPorto = numPorto + 1
    s.close()

# ******************************
# Ara per al moonblaster FM
# ******************************
fitxers = sorted(glob.glob('/home/jepsuse/MSX/Music_Moonblaster/MBW/*MWM'))
numPorto = 1;
for fitxer in fitxers:
    s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    s.connect('/tmp/openmsx-jepsuse/socket.' + pid_openMSX) # A la canço 20 es penjava. Vaig prova de reconnectar-ho a cada moment
    print(f"Cançó {fitxer}. Número {numPorto} de {len(fitxers)}")
    [path, nomfitx]=os.path.split(fitxer)
    fitxmbms = glob.glob(fitxer[0:-3]+'*')
    for fitxmbm in fitxmbms:
        [pathmbm, nommbm] = os.path.split(fitxmbm)
        shutil.copy(fitxmbm,pathmbm+'/disquet_1f')

    s.send(bytes("<command>keymatrixdown 7 2</command>","utf-8")) # F4
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 7 2</command>","utf-8"))
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixdown 8 1</command>","utf-8")) # Espai - Seleccionem Load
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 8 1</command>","utf-8"))
    time.sleep(1)
    s.send(bytes("<command>keymatrixdown 8 1</command>","utf-8")) # Espai - Seleccionem Song
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 8 1</command>","utf-8"))
    time.sleep(1)
    s.send(bytes("<command>keymatrixdown 8 1</command>","utf-8")) # Espai - Seleccionem la cançó
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 8 1</command>","utf-8"))
    time.sleep(6)
    s.send(bytes("<command>keymatrixdown 7 4</command>","utf-8")) # Esc - Sortim de la càrrega
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 7 4</command>","utf-8"))
    time.sleep(0.5)
    # Comencem a gravar
    s.send(bytes("<command>record start " + nommbm + "</command>","utf-8"))
    time.sleep(0.1)
    s.send(bytes("<command>keymatrixdown 6 32</command>","utf-8")) # F1 - Comença la música
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 6 32</command>","utf-8"))
    time.sleep(180)
    s.send(bytes("<command>record stop</command>","utf-8"))
    # Sortim de la cançó si encara està sonant, prement esc
    s.send(bytes("<command>keymatrixdown 7 4</command>","utf-8")) # Esc - Sortim de la reproducció
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 7 4</command>","utf-8"))
    time.sleep(0.5)
    for fitxmbm in fitxmbms:
        [pathmbm, nommbm] = os.path.split(fitxmbm)
        os.remove(pathmbm+'/disquet_1f/' + nommbm)
    numPorto = numPorto + 1
    s.close()

# ******************************
# Ara per al moonblaster Wave
# Si té MWK s'ha de carregar primer el wavekit
# ******************************
fitxers = sorted(glob.glob('/home/jepsuse/MSX/Music_Moonblaster/MBW/*MWM'))
numPorto = 1;
for fitxer in fitxers:
    s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    s.connect('/tmp/openmsx-jepsuse/socket.' + pid_openMSX) # A la canço 20 es penjava. Vaig prova de reconnectar-ho a cada moment
    print(f"Cançó {fitxer}. Número {numPorto} de {len(fitxers)}")
    [path, nomfitx]=os.path.split(fitxer)
    fitxmbms = glob.glob(fitxer[0:-3]+'*')
    for fitxmbm in fitxmbms:
        [pathmbm, nommbm] = os.path.split(fitxmbm)
        shutil.copy(fitxmbm,pathmbm+'/disquet_1f')

    TeWaveKit = False
    if len(fitxmbms) > 1:
        TeWaveKit = True

    if not TeWaveKit:
        # Carreguem el que hi ha per defecte en el MoonWave
        shutil.copy('/mnt/DATA/Videojocs/MSX/Moonblaster/MB_Wave/MBWAVE117/FULLEDIT.MWK',pathmbm+'/disquet_1f')
    # Carreguem WAVE
    s.send(bytes("<command>keymatrixdown 7 2</command>","utf-8")) # F5
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 7 2</command>","utf-8"))
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixdown 8 1</command>","utf-8")) # Espai - Seleccionem Load
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 8 1</command>","utf-8"))
    time.sleep(1)
    s.send(bytes("<command>keymatrixdown 8 64</command>","utf-8")) # down - seleccionem wavekit
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 8 64</command>","utf-8"))
    time.sleep(1)
    s.send(bytes("<command>keymatrixdown 8 1</command>","utf-8")) # Espai - Seleccionem Load
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 8 1</command>","utf-8"))
    time.sleep(1)
    s.send(bytes("<command>keymatrixdown 8 1</command>","utf-8")) # Espai - Seleccionem el wavekit
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 8 1</command>","utf-8"))
    time.sleep(40)
    if not TeWaveKit:
        # Carreguem el que hi ha per defecte en el MoonWave
        os.remove(pathmbm+'/disquet_1f/FULLEDIT.MWK')

    # Carreguem la cançó
    s.send(bytes("<command>keymatrixdown 7 2</command>","utf-8")) # F5
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 7 2</command>","utf-8"))
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixdown 8 1</command>","utf-8")) # Espai - Seleccionem Load
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 8 1</command>","utf-8"))
    time.sleep(1)
    s.send(bytes("<command>keymatrixdown 8 1</command>","utf-8")) # Espai - Seleccionem Song
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 8 1</command>","utf-8"))
    time.sleep(1)
    s.send(bytes("<command>keymatrixdown 8 1</command>","utf-8")) # Espai - Seleccionem la cançó
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 8 1</command>","utf-8"))
    time.sleep(6)
    s.send(bytes("<command>keymatrixdown 7 4</command>","utf-8")) # Esc - Sortim de la càrrega
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 7 4</command>","utf-8"))
    time.sleep(0.5)
    # Comencem a gravar
    s.send(bytes("<command>record start " + nommbm + "</command>","utf-8"))
    time.sleep(0.1)
    s.send(bytes("<command>keymatrixdown 6 32</command>","utf-8")) # F1 - Comença la música
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 6 32</command>","utf-8"))
    time.sleep(180)
    s.send(bytes("<command>record stop</command>","utf-8"))
    # Sortim de la cançó si encara està sonant, prement esc
    s.send(bytes("<command>keymatrixdown 7 4</command>","utf-8")) # Esc - Sortim de la reproducció
    time.sleep(0.3)
    s.send(bytes("<command>keymatrixup 7 4</command>","utf-8"))
    time.sleep(0.5)
    for fitxmbm in fitxmbms:
        [pathmbm, nommbm] = os.path.split(fitxmbm)
        os.remove(pathmbm+'/disquet_1f/' + nommbm)
    numPorto = numPorto + 1
    s.close()
