.area _HEADER
                                ;
; conditionals
;
withBIN	.equ	0	;1=include .bin header
withAUD	.equ	1	;1=include check for emulated MSX-MUSIC
withGPIO	.equ	0	;1=include check for real Y8950 GPIO
withDisk	.equ	1	;1=include disk loading code

;
;Note:	The labels CHIPS and SMPADR refered to in this source file should be
;	written to the locations with the same labels in MBPLAY.SRC. When
;	using this source file together with MBPLAY.SRC, make sure to remove
;	these labels at the end of the source in some way to make sure the
;	values are written to the correct location. They are in this source
;	file just for the sake of being able to assemble it without errors.                 ;
;---- miscellaneous labels (these should refer to the ones in MBPLAY.SRC!) ----


CHIPS:	.db	0
SMPADR:	.ds	#56

.if withBIN=1
.db	#0x0fe
  ;; 	.dw	start,end,start           
.org	#0x0c000

start:
.endif


.area _CODE
;---------------- check connected soundchips ---------------
;
;Output = CHIPS contains 0,1 or 2.(MSX-AUDIO,MSX-MUSIC,both)
;

SRCCHP:	xor	a
	ld	(CHIPS),a
  call	SRCFMP                    ;
                               ;	call	SRCAUD                  
	ld	a,(CHIPS)
	or	a
	ret	z
	dec	a
	ld	(CHIPS),a
	ret

;----- find MSX-MUSIC -----

SRCFMP:	ld	hl,#0x0fcca
	xor	a
	ld	b,#4
FMLP2:	push	bc
	ld	b,#4
FMLP1:	push	bc
	push	af
	push	hl
	set	7,a
	ld	h,#0x040
	call	#0x024
	pop	hl
	push	hl
	ld	a,(hl)
	cp	#0x020
	call	z,FMTEST
	jp	z,FMFND
	pop	hl
	pop	af
	add	a,#4
	and	#0x0f
	inc	hl
	inc	hl
	inc	hl
	inc	hl
	pop	bc
	djnz	FMLP1
	add	a,#1
	and	#0x03
	pop	bc
	djnz	FMLP2
	jp	SETBAS
FMTEST:	ld	hl,#0x04018
	ld	de,(FM2TXT)
	ld	b,#8
	call	FMLP
	jr	z,FM2
	ld	hl,#0x0401c
	ld	de,(FMTEXT)
	ld	b,#4
	call	FMLP
.if withAUD=1
	ret	nz
	ld	hl,#0x04018
	ld	de,(FM3TXT)
	ld	b,#3
	call	FMLP
	jr	Z,FMEMU
	xor	a
FMEMU:	or	a
.endif
	ret

FMLP:	ld	a,(de)
	cp	(hl)
	ret	nz
	inc	hl
	inc	de
	djnz	FMLP
	cp	a
	ret
FM2:	ld	a,(CHIPS)
	set	2,a
	ld	(CHIPS),a
	ret
FMFND:	pop	hl
	pop	af
	pop	bc
	pop	bc
	ld	a,(CHIPS)
	bit	2,a
	res	2,a
	set	1,a
	ld	(CHIPS),a
	jr	nz,SETBAS
	ld	a,(#0x07ff6)
	or	#1
	ld	(#0x07ff6),a
SETBAS:	di
	ld	a,(#0x0fcc1)
	ld	h,#0x040
	call	#0x024
	ei
	ret

.if withAUD=1
FM3TXT:	.ascii	"AUD"
.endif
FM2TXT:	.ascii	"APRL"
FMTEXT:	.ascii	"OPLL"

;---- find MSX-AUDIO ----

SRCAUD:	in	a,(#0x0c0)
.if withGPIO=1
	cp	#0x0ff
	ret	z
.else
	or	#0x06
	cp	#0x06
	ret	nz

	ld	a,#0x018
	out	(#0x0c0),a
	ld	a,#0x0f
	out	(#0x0c1),a
	ld	a,#0x019
	out	(#0x0c0),a
	ld	b,#0
_GPIO:	ld	a,b
	out	(#0x0c1),a
	in	a,(#0x0c1)
	and	#0x0f
	cp	b
	ret	nz
	inc	b
	bit	4,b
	jr	z,_GPIO
	ld	a,#0x018
	out	(#0x0c0),a
	xor	a
	out	(#0x0c1),a

.endif
	ld	a,(CHIPS)
	set	0,a
	ld	(CHIPS),a
	ret

;------------------- load MoonBlaster song ----------------
;
; Note:	- Make sure the name is writting in the FCB before
;	  calling the routine!
;	- Make sure to only load USER saved files!

.if withDisk=1
MBLOAD:	push	de
	call	OPENF
	pop	de
	ld	hl,(BLENG)
	call	LOADF
	jp	CLOSEF

;------------------- load MoonBlaster sample kit -----------
;
; Note:	- Make sure to switch off the R800 mode before
;	  calling the routine!
;

MKLOAD:	call	OPENF
	ld	de,#0x08000
	ld	hl,#56
	call	LOADF
	ld	hl,#0x08000
	ld	de,(SMPADR)
	ld	bc,#56
	ldir
	ld	a,#1
	ld	(SAMPDT),a
	ld	hl,#0x04000
	ld	de,#0x08000
	call	LOADF
	call	MOVSMP
	ld	hl,#0x04000
	ld de,#0x08000
	call	LOADF
	call	MOVSMP
	jp	CLOSEF

;------ disk access routines ------

OPENF:	ld	de,(FCB)
	ld	c,#15                     ;OPEN
	call	BDOS
	ld	hl,#1
	ld	(SIZE),hl
	dec	hl
	ld	(BLOCK),hl
	ld	(BLOCK+2),hl
	ret
LOADF:	push	hl
	ld	c,#26                       ;(SETDMA)
	call	BDOS
	ld	de,(FCB)
	pop	hl
	ld	c,#39                     ;(READ)
	jp	BDOS
CLOSEF:	ld	de,(FCB)
	ld	c,#16                     ;(CLOSE)
	jp	BDOS
.endif

;------- transfer samplekit into MSX-AUDIO RAM ------

SAMPDT:	.db	1
MOVSMP:	ld	a,(SAMPDT)
	xor	#1
	ld	(SAMPDT),a
	or	a
	ex	af,af'                    ;
  call	z,MOVSM2 ; No sé a on crida aquesta Z, però jo no necessito els samples que crec que és per MoonSound
	ex	af,af'
	call	nz,MOVSM3
	di
	ld	a,#4
	out	(#0x0c0),a
	ld	a,#0x060
	out	(#0x0c1),a
	ld	bc,#0x04000
	ld	hl,#0x08000
MOVSM6:	ld	a,#0x0f
	out	(#0x0c0),a
	ld	a,(hl)
	out	(#0x0c1),a
	ld	a,#4
	out	(#0x0c0),a
	ld	a,#0x080
	out	(#0x0c1),a
	inc	hl
	dec	bc
	ld	a,b
	or	c
	jr	z,MOVSM4
MOVSM5:	in	a,(#0x0c0)
	bit	3,a
	jr	z,MOVSM5
	jr	MOVSM6
MOVSM4:	ld	a,#4
	out	(#0x0c0),a
	ld	a,#0x078
	out	(#0x0c1),a
	ld	a,#0x080
	out	(#0x0c1),a
	ld	a,#7
	out	(#0x0c0),a
	ld	a,#1
	out	(#0x0c1),a
	ei
	ret
MOVSM3:	ld	hl,(SAMPD3)
	jr	MOVSM7
MOVSM2:	ld	hl,(SAMPD2)
MOVSM7:	ld	b,#10
MOVSM8:	ld	a,(hl)
	out	(#0x0c0),a
	inc	hl
	ld	a,(hl)
	out	(#0x0c1),a
	inc	hl
	djnz	MOVSM8
	ret

.area _DATA
SAMPD2:	.db	#0x004,#0x078,#0x004,#0x080,#0x007,#0x001,#0x007,#0x060,#0x008,#0x000
	.db	#0x009,#0x000,#0x00a,#0x000,#0x00b,#0x0ff,#0x00c,#0x00f,#0x010,#0x0f0
SAMPD3:	.db	#0x004,#0x078,#0x004,#0x080,#0x007,#0x001,#0x007,#0x060,#0x008,#0x000
	.db	#0x009,#0x000,#0x00a,#0x010,#0x00b,#0x0ff,#0x00c,#0x01f,#0x010,#0x0f0


;----------- FCB -----------

.if withDisk=1
FCB:	.db	0
  .ascii "???????????"
	.dw	0
SIZE:	.dw	0
BLENG:	.dw	0	; block length
	.db	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
BLOCK:	.dw	0
	.dw	0
.endif

;
; BDOS constants
;
.if withDisk=1
BDOS: .db	#0x05
  ;;SETDMA: .db	#26
  ;;READ: .db	#39
  ;;OPEN: .db	#15
  ;;CLOSE:  .db	#16
  ;; Les comandes del READ i OPEN donaven error, no sabia com posar-les. L'Eric Boez va pensar d'escriure-ho directament al codi. És el que he fet
.endif

.if withBIN=1
end:
.endif
