// 2022-06-22 Fallen sustinguts i el conjunt de la percussió
// Provaré de fer sonar algun so amb el que he descobert del MBM file
// El Moonblaster sempre usa el OPLL en funció amb els drums. S'ha de configurar
 
// La cançó Pep sona diferent en el joc que en el moonblaster. Hi ha alguna cosa
// en el driver que no està bé
// Aquest fitxer no el tenia a la llista de fitxers C, potser li vaig canviar el
// nom, l'he descarregat del gitlab
// El deuria treure perquè en compilar dona problemes ja que compilo sempre amb la llibreria. No he de fer un doit sinó un make
#include "fusion-c/header/msx_fusion.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define NR_OF_AUDIO_INSTRUMENTS 16
#define NR_OF_MUSIC_INSTRUMENTS 16
#define AUDIO_INSTRUMENT_DATA_SIZE 9
#define STEREO_SETTING_SIZE 10
#define NR_OF_CHANNELS 9
#define NR_OF_ORIGINAL_INSTRUMENTS 6  // Són els instruments que es configuren per software. El moonblaster només deixa tenir-ne 6 configurats pel MSX-Music
#define MUSIC_INSTRUMENT_LENGTH 8  // Els 8 primers registres que s'han de configurar per un instrument original

#define NOTE_ON 96              /* 001 - 096 */
#define NOTE_OFF 97             /* 097       */
#define INSTRUMENT_CHANGE 98    /* 098 - 113 */
#define VOLUME_CHANGE 114       /* 114 - 176 */
#define STEREO_SET 177          /* 177 - 179 */
#define NOTE_LINK 180           /* 180 - 198 */
#define PITCH 199               /* 199 - 217 */
#define BRIGHTNESS_NEGATIVE 218 /* 218 - 223 */
#define REVERB 224              /* 224 - 230 */ // És el detune en el manual
#define BRIGHTNESS_POSITIVE 231 /* 231 - 236 */
#define SUSTAIN 237             /* 237       */
#define MODULATION 238          /* 238       */

#define COMMAND_TEMPO 23          /* 001 - 023 */
#define COMMAND_PATTERN_END 24    /* 024       */
#define COMMAND_DRUM_SET_MUSIC 25 /* 025 - 027 */
#define COMMAND_STATUS_BYTE 28    /* 028 - 039 */
#define COMMAND_TRANSPOSE 49      /* 049 - ... */

#define HALT __asm halt __endasm // wait for the next interrupt

uint8_t __at(0x6996) bytedrum ;
uint8_t __at(0x6995) drumcap ;

static uint8_t tempo;
static uint8_t copsInterrupcio = 0;
static uint8_t filera = 0;

typedef struct {
  uint8_t instrument;
  uint8_t volume;
} INSTRUMENT_DATA_MUSIC;

typedef struct {
  uint8_t song_length;
  uint16_t id;

  uint8_t voice_data_audio[NR_OF_AUDIO_INSTRUMENTS][AUDIO_INSTRUMENT_DATA_SIZE];

  uint8_t instrument_list_audio[NR_OF_AUDIO_INSTRUMENTS];
  INSTRUMENT_DATA_MUSIC instrument_list_music[NR_OF_MUSIC_INSTRUMENTS]; // Els 16 que es poden escollir al llistat. Quan indico una I a les notes, el número indica instrument de l'1 al 16. Si aquest número és més gran que 16 és que és un instrument sintètic i els seus valors de registre es troben a original_instrument_data. Però no sé si estan indexats per original_instrument_prog

  uint8_t channel_chip_set[STEREO_SETTING_SIZE];
  uint8_t start_tempo;
  uint8_t sustain;
  char track_name[41];

  uint8_t start_instruments_audio[NR_OF_CHANNELS];
  uint8_t start_instruments_music[NR_OF_CHANNELS];
  uint8_t original_instrument_data[NR_OF_ORIGINAL_INSTRUMENTS]
                                  [MUSIC_INSTRUMENT_LENGTH];
  // Estan indexats fent l'instument del canvi I? - 16 i aquest instrument és el punter de NR_OF_ORIGINAL_INSTRUMENTS
  uint8_t original_instrument_prog[NR_OF_ORIGINAL_INSTRUMENTS]; // el roboplay no els fa servir

  char sample_kit_name[8];

  uint8_t drum_setup_music_psg[15];
  // Al full drum_setup.ods hi ha l'explicació detallada. Cada byte conté els 3 msb la combinació de l'efecte PSG a activar, ja que només pot funcionar 1, els altres 5 bits contenen l'activació del BD-SD-To-Cy_HH
  uint8_t drum_volumes_music[3]; // També he de mirar com estan guardats pel BD-SD-To-Cy-HH. Quan posa 15 en el moonblaster, es guarden com a 0 en el mbm, ja que el volum màxim és 0
  uint16_t drum_frequencies_music[3][3]; // El primer és el drumset i el segon són pels channels 7, 8 i 9
  // El drumset és el que es pot canviar a la columna de comanda
  // Hi ha 9 bits per la freqüència i 3 per l'octava,  els 9LSB és per la freqüència i després venen els 3 per l'octava. L'octava apareix un 1 en el moonblaster, però hi ha un 0 al fitxer, un 2 fa aparèixer un 1 en el mbm (el fitxer ja té el valor que ha d'anar al registre, ja no cal restar un 1)
  // Pel que he vist en el vgmrips cada cop que canvia la freqüència d'un canal posa el registre 0x0E el valor 0x20
  uint8_t dummy[2];
  uint8_t start_reverb[NR_OF_CHANNELS];
  uint8_t loop_position;
} MBM_HEADER;


static FCB file;
static MBM_HEADER __at(0x8000) capcalera;
static uint8_t __at(0x8215) songFile[80*16*13 + 80*2 + 200 + 0x178]; // 80 patrons de tamany 16 fileres per 13 columnes com a màxim (sample i drum van al mateix byte) més la posició dels 80 patrons dins el fitxer que tenen tamany 2 bytes (d'aquí el 80*2) + 200 de les posicions dels patrons + la capçalera
// No em cal calcular el posPatrons, estan després de la longitud, són uint16_t. És la posició dins el fitxer file. Aleshores jo les hauré de restar dins de song
static uint8_t __at(0x7000) ordrePatrons[200];
static uint8_t posSong; // Patró que estic tocant dins la cançó. Aquest ja és el
                        // de la capçalera, no el necessitaria
static uint16_t posPatro; // Notes que estic tocant dins el patró
static uint8_t __at(0x6998) globfreq;

const uint8_t notesFreq[12] = {
    173, // 0b10101101, // 173 (AD), C,
    181, // 0b10110101, // 181 (B5), c#, freq 272.2
    192, // 0b11000000, // 192 (C0), D, freq 293.7
    204, // 0b11001100, // 204 (CC), D#, freq 311.1
    216, // 0b11011000, // 216 (D8), E, freq 329.6
    229, // 0b11100101, // 229 (E5), F, freq 349.2
    242, // 0b11110010, // 242 (F2), F#, freq 370
    1,   // 0b00000001, // 257 (1+1), G, freq 392 A partir d'aquest porten 1 extra
    16,  // 0b00010000, // 272 (1+10), G#, freq 415.3
    32,  // 0b00100000, // 288 (1+20), A, freq 440
    39,  // 0b00110001, // 305 (1+31), A#, freq 466.2
    57,  // 0b01000011, // 323 (1+43), B, freq 493.9
}; // El definia com a const, però crec que això feia que només el punter fos
   // constant, no tots els valors. Ja és correcte el const. El que no és
   // correcte és posar un at. He fet el provaArray.c per comprovar-ho i he fet
   // un post al forum que m'han contestat molt bé
   // https://msx.org/forum/msx-talk/development/sdcc-global-constant-initialized-array-in-a-fixed-position-in-memory
// Ordre notes C,C#,D,D#,E,F,F#,G, G#,A,A#,B
// A la documentació de Yamaha comença a partir del C#. Al fitxer chipsfmpacpr2_en.txt hi ha la informació de les freqüències i són les mateixes que s'usen en el driver de Moonblaster en assembly, etiquetat fmpac. Jo he afegit només la freqüència C

typedef struct {
  uint8_t octava;
  uint8_t nota;
  // Podria fer un byte amb els dos, però ja està bé així
  // Potser també necessitaré el volum del canal
  int8_t bending;
  uint8_t freq_bending; // Segurament es podria ajuntar amb nota per tenir només una freqüència
  uint8_t overflow_bending; // Per quan passa la freqüència de 8 bits
  // El roboplay té una estrucgtura de status g_mbm_status_table per cada canal que no sé el que hi té
} NOTA_TOCANT;

static NOTA_TOCANT __at(0x7300)
    notaTocantCanal[6]; // Guardarà la informació de la nota de notesFreq que
                        // s'està tocant per aquell canal
static uint8_t
    brightness_inst_orig; // Quan carreguem els registres de l'instrument
                          // original recarregarem aquest registre i anirà
                          // variant en les comandes de Brightness

void FT_SetName(FCB *p_fcb, const char *p_name) // Routine servant à vérifier le
                                                // format du nom de fichier
{
  char i, j;
  memset(p_fcb, 0, sizeof(FCB));
  for (i = 0; i < 11; i++) {
    p_fcb->name[i] = ' ';
  }
  for (i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++) {
    p_fcb->name[i] = p_name[i];
  }
  if (p_name[i] == '.') {
    i++;
    for (j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.');
         j++) {
      p_fcb->ext[j] = p_name[i + j];
    }
  }
}

void FT_errorHandler(char n, char *name) // Gère les erreurs
{
  Screen(0);
  SetColors(15, 6, 6);
  switch (n) {
  case 1:
    Print("\n\rFAILED: fcb_open(): ");
    Print(name);
    break;

  case 2:
    Print("\n\rFAILED: fcb_close():");
    Print(name);
    break;

  case 3:
    Print("\n\rStop Kidding, run me on MSX2 !");
    break;
  }
  Exit(0);
}

int FT_openFile(char *file_name)
{
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }
}

void readFileSong(){
  // He assumit que tots estaran en User Mode. El Edit Mode té un byte inicial més i ocupa més espai
  fcb_read(&file, songFile, sizeof(songFile));

  memcpy(&capcalera, songFile, sizeof(capcalera));

  memcpy(ordrePatrons,&songFile[0x178], capcalera.song_length+1);
}

void WriteOPLLreg(char reg, char value)__naked {
  // Escrivim els registres a l'OPLL tal i com vam fer a les proves de FM_scratch
  __asm
    ld iy,#2
    add iy,sp ;Bypass the return addess of the function. Totes les funcions en asm del llibre de fusion-c fan primer aquest pas

    ld D,(IY) ; reg
    ld E,1(IY)  ; value

    ld A,D
    out (#0x7C),A
    LD A,E
    out (#0x7D),A
    ex (SP),HL
    ex (SP),HL
    ret
  __endasm;
}
void PlayNote(uint8_t columna, uint8_t octava, uint8_t freq, uint8_t highfreq) {
  WriteOPLLreg(0x10 + columna, freq);
  WriteOPLLreg(0x20 + columna, (octava << 1) | highfreq); // Al fer aquest canvi a l'escriptura, el MSX-Music sona com el del moonblaster, amb un so més nítid
  WriteOPLLreg(0x20 + columna, (octava << 1) | 0x10 | highfreq);
}
void PlayBend(uint8_t columna) {
  uint8_t novafreq =
      notaTocantCanal[columna].freq_bending + notaTocantCanal[columna].bending;
  globfreq = novafreq;
  if (notaTocantCanal[columna].bending > 0) {
    if (novafreq < notaTocantCanal[columna].freq_bending) {
      // Hi ha hagut overflow
      if (notaTocantCanal[columna].overflow_bending > 0) {
        novafreq = 0xFF;
      } else {
        notaTocantCanal[columna].overflow_bending = 1;
      }
    }
  } else {
    if (novafreq > notaTocantCanal[columna].freq_bending) {
      // Hi ha hagut overflow
      if (notaTocantCanal[columna].overflow_bending > 0) {
        notaTocantCanal[columna].overflow_bending = 0;
      } else {
        novafreq = 0x01; // Diria que el 0x00 no és cap nota
      }
    }
  }
  // Tocar nota que podria fer una funció. Mirar les parts comunes i a on
  // apareix l'estructura
  PlayNote(columna,notaTocantCanal[columna].octava, novafreq, notaTocantCanal[columna].overflow_bending);
  notaTocantCanal[columna].freq_bending = novafreq;
}

void seguentPatro(){
  posSong = posSong + 1;
  if (posSong > capcalera.song_length) {
    posSong = 0;
  }
  posPatro =
      songFile[0x178 + capcalera.song_length + 2 * ordrePatrons[posSong] - 1] +
      (songFile[0x178 + capcalera.song_length + 2 * ordrePatrons[posSong]]
       << 8);
  filera = 0;
}

void canviDrumset(uint8_t set) {
  WriteOPLLreg(0x16, (uint8_t)capcalera.drum_frequencies_music[set][0]);
  WriteOPLLreg(0x26, (uint8_t)(capcalera.drum_frequencies_music[set][0] >> 8));
  WriteOPLLreg(0x17, (uint8_t)capcalera.drum_frequencies_music[set][1]);
  WriteOPLLreg(0x27, (uint8_t)(capcalera.drum_frequencies_music[set][1] >> 8));
  WriteOPLLreg(0x18, (uint8_t)capcalera.drum_frequencies_music[set][2]);
  WriteOPLLreg(0x28, (uint8_t)(capcalera.drum_frequencies_music[set][2] >> 8));
}

void InicialitzemCanals() {
  // Hem d'inicialitzar amb els instruments definits al mbm. No sempre el violí
  for( int k=0;k<6;k++) {
    WriteOPLLreg(
        0x30 + k,
        capcalera.instrument_list_music[capcalera.start_instruments_music[k]-1].instrument << 4 |
        capcalera.instrument_list_music[capcalera.start_instruments_music[k]-1].volume);
  }

  // Inicialitzem la percussió
  WriteOPLLreg(0x36, capcalera.drum_volumes_music[0]);
  WriteOPLLreg(0x37, capcalera.drum_volumes_music[1]);
  WriteOPLLreg(0x38, capcalera.drum_volumes_music[2]);

  canviDrumset(0);

  // Inicialitzem el notaTocantCanal. Amb el bending a 0 n'hi ha prou. Així no es toquen les notes extres
  for(int k=0; k<6; k++) {
    notaTocantCanal[k].bending = 0;
  }

}

void WAIT(int cicles) {
  int i;
  for (i = 0; i < cicles; i++)
    HALT;
  return;
}

void playPatro() {
  // L'he d'adaptar al nou format que jo tinc de cançó. Tenint en compte que hi ha camps que estan en blanc
  DisableInterrupt();
    uint8_t columna = 0;
    if (copsInterrupcio>=tempo) {
      while (columna < 13) {
        uint8_t byteLlegit = songFile[posPatro];
        if (byteLlegit < 243) {
          // Faig només MSX-Music, només utilitzo 6 dels 9 canals que indica
          if(columna<6){
            if (byteLlegit <= NOTE_ON) {
              uint8_t octava = byteLlegit / 12;
              uint8_t nota = byteLlegit - octava * 12;
              uint8_t notaFreq =  notesFreq[nota - 1];
              if (nota < 8) {
                PlayNote(columna, octava, notaFreq, 0);
              } else {
                PlayNote(columna, octava, notaFreq, 1);
              }
              notaTocantCanal[columna].nota = nota - 1;
              notaTocantCanal[columna].octava = octava;
              notaTocantCanal[columna].bending = 0;
              notaTocantCanal[columna].freq_bending = notaFreq;
            } else if (byteLlegit == NOTE_OFF) {
              WriteOPLLreg(0x10 + columna, 0x0);
              WriteOPLLreg(0x20 + columna, 0x10);
              notaTocantCanal[columna].bending = 0;
            } else if (byteLlegit < VOLUME_CHANGE) {
              // Canvi d'instrument
              // He de tenir guardat un estat actual del volum i instrument pel canal, per quan s'hagi de canviar un, pugui mantenir l'altre, ja que tant l'instrument com al canal estan al mateix byte
              capcalera.instrument_list_music[columna].instrument = byteLlegit;
              WriteOPLLreg(0x30+columna, byteLlegit<<4 | capcalera.instrument_list_music[columna].volume);
            } else if (byteLlegit < STEREO_SET) {
              // Canviem el volum
              // El volum té fins a 64 però l'MSX-Music només té fins a 15 i en ordre invertit. És a dir en el moonblaster v64 és el màxim i pel Music és 0. Hem de dividir entre 4 i negar
              capcalera.instrument_list_music[columna].volume = !(byteLlegit>>4);
              WriteOPLLreg(0x30 + columna,
                           capcalera.instrument_list_music[columna].instrument
                           << 4 |
                           capcalera.instrument_list_music[columna].volume);
            } else if (byteLlegit < NOTE_LINK) {
              // Do nothing. STereo only with moonsound
            } else if (byteLlegit < PITCH) {
              // Do note link
              // Hauré d'anar guardant per cada canal la nota que s'està tocant. Depèn com també hauré de pujar l'octava
              // Roboplay resta 189 per obtenir -9 a +9
              // He de saber la nota que estic tocant i canviar-la
              // LA canço punkadidd del disk de moonblaster té en el patró 10 un Link
              int8_t link = byteLlegit - 189;
              int8_t novaNota = notaTocantCanal[columna].nota + link;
              if (novaNota<0) {
                notaTocantCanal[columna].nota = 12 + novaNota; // Les 12 notes de l'escala
                notaTocantCanal[columna].octava = notaTocantCanal[columna].octava - 1;
              } else if (novaNota >= 12) {
                notaTocantCanal[columna].nota = 12 - novaNota;
                notaTocantCanal[columna].octava = notaTocantCanal[columna].octava + 1;
              } else {
                notaTocantCanal[columna].nota = novaNota;
              }
              // He d'escriure la nova nota al registre perquè sigui tocada
              if (notaTocantCanal[columna].nota <7 ){
                PlayNote(columna, notaTocantCanal[columna].octava,
                         notaTocantCanal[columna].nota, 0);
              } else {
                PlayNote(columna, notaTocantCanal[columna].octava,
                         notaTocantCanal[columna].nota, 1);
              }
            } else if (byteLlegit < BRIGHTNESS_NEGATIVE) {
              // He de fer exemples amb el pitchbend i estudiar-lo, és una entrada de P.
              // El roboplay resta 208 que és el valor per tenir [-9,9]
              // El pitchbend va sumant a cada iteració la freqüència indicada amb la P. Suposo que fins al 0x1FF o fins el 0x0
              // S'atura a la següent nota, OFF, SUS or MOD event. I també si hi ha P0
              // Ho hauré de controlar amb la nota i a cada columna revisar si hi ha bending pendent d'executar. Abans de fer el columna++
              // Si a la mateixa columna trobo una altra P continua augmentant al nou ritme +3,+4,+5... la freqüència que ja tenia
              int8_t pitch = byteLlegit - 208;
              if (pitch==0){
                notaTocantCanal[columna].bending=0; // Amb aquesta informació ja en tinc prou. La resta de camps ja els canviaré quan hi hagi un pitch que s'inicialitza a aquí.
              } else {
                notaTocantCanal[columna].bending = pitch;
                notaTocantCanal[columna].freq_bending = notesFreq[notaTocantCanal[columna].nota];
                if (notaTocantCanal[columna].nota>=7) {
                  notaTocantCanal[columna].overflow_bending = 1;
                } else {
                  notaTocantCanal[columna].overflow_bending = 0;
                }
                PlayBend(columna);
              }
            } else if (byteLlegit < REVERB) {
              // El brightness aplica només a l'instrument original, no al de
              // fàbrica i modifica el carrier, el registre 2
              uint8_t valor = 217 - byteLlegit ; // El 217 és per tenir valors negatius ja que byteLlegit 218-223
              brightness_inst_orig = brightness_inst_orig + valor;
              WriteOPLLreg(0x02, brightness_inst_orig);
            } else if (byteLlegit < BRIGHTNESS_POSITIVE) {
              // El detune agafa la freqüència (nota) i li suma+3 o -3 a la
              // freqüència, pel que he pogut comprovar amb el VGM
              // El roboplay li diu REVERB
              // A les instruccions (apartat 3.3 secció Detune switch) comenta que no s'acumula, per tant no he de tornar a guardar la freqüència dins de notaTocant
              uint8_t valor = (byteLlegit - 227)*3;
              uint8_t octava = notaTocantCanal[columna].octava;
              uint8_t nota = notaTocantCanal[columna].nota;
              uint8_t freq = notesFreq[notaTocantCanal[columna].nota] + valor;
              if (nota < 8) {
                PlayNote(columna, octava, freq, 0);
              } else {
                PlayNote(columna, octava, freq, 1);
              }
            } else if (byteLlegit < SUSTAIN) {
              uint8_t valor = byteLlegit - 230;
              brightness_inst_orig = brightness_inst_orig + valor;
              WriteOPLLreg(0x02, brightness_inst_orig);
            } else if (byteLlegit == SUSTAIN) {
              uint8_t octava = notaTocantCanal[columna].octava;
              uint8_t nota = notaTocantCanal[columna].nota;
              if (nota < 7) {
                WriteOPLLreg(0x20 + columna, (octava << 1) | 0x30);
              } else {
                WriteOPLLreg(0x20 + columna, (octava << 1) | 0x31);
              }
            } else if (byteLlegit == MODULATION) {
              // L'he d'estudiar
              // Les instruccions diu que pugen la freqüència i la baixen. Però quina quantitat?
              // El que sí que he de parar és el bending
              notaTocantCanal[columna].bending = 0;
              // He fet proves posant un C3 i MOD i no he notat cap efecte
            }
          } else if (columna == 11) {
            // Processar sample i drum command
            // Les columnes van juntes. Entenc que el byte baix és el DRM
            uint8_t  drum = byteLlegit & 0x1F - 1; // Comencen indicant el bloc 1 fins al 15, però per indexar vaig del 0 al 14
            WriteOPLLreg(
                    0x0E, capcalera.drum_setup_music_psg[drum] & 0x1F);
            WriteOPLLreg(0x0E,
                         (capcalera.drum_setup_music_psg[drum] & 0x1F) | 0x20);
          } else if (columna == 12) {
            // PRocessar CMD
            if(byteLlegit<24) {
              // Canvi del tempo
              tempo = tempo - byteLlegit;
            } else if (byteLlegit == 24 ) {
              // End of Pattern
              // He de fer el mateix que si la filera == 16. Ho posaré en una funció
              seguentPatro();
            } else if (byteLlegit<28) {
              // Change drumset
              uint8_t nouDrumset = byteLlegit - 25;
            }
            // Hi ha més comandes especificades en el mbm file format, però no entenc què fan i com s'introdueixen
            // El mbm tampoc processa el status byte
            // El transpose command potser si utilitzes un transpose editant queda registrat al mbm. S'ha de comprovar. He fet un fitxer que s'haurà de provar de carregar
          }
          // Abans d'acabar la columna he de mirar si hi ha un pitchbend corrent, ja que hi ha comandes que no afecten al pitchbend
          if (notaTocantCanal[columna].bending != 0 && columna<6) {
            PlayBend(columna);
          }
          columna++;
        } else {
          // Aquestes columnes que no hi ha cap comanda, he de mirar si tenen un pitchbend
          for(int k=0;(k+columna<byteLlegit - 242) && (k+columna<6);k++){
            // El k<6 ja que el moonblaster té 11 columnes de comandes però el msx-music només fa servir les 6 primeres
            if(notaTocantCanal[columna+k].bending!=0){
              PlayBend(columna+k);
            }
          }
          columna = columna + byteLlegit-242;
        }
        posPatro++;
      }
      copsInterrupcio=0;
      filera++;
    }
    if (filera == 16) {
      // Hauré de saltar al següent patró
      // Tanbé podria fer-ho sabent la posició del següent patró i controlant si estic a l'últim patró
      seguentPatro();
    }
    copsInterrupcio++;
    EnableInterrupt();
}

void apaguemOPLL() {
  for (int k=0; k<9; k++) {
    WriteOPLLreg(0x20+k,0x00);
  }
}

void play_sound() {
  // Preparem el fM per sonar els drums
  WriteOPLLreg(0x16,0x20);
  WriteOPLLreg(0x17, 0x50);
  WriteOPLLreg(0x18, 0xC0);
  WriteOPLLreg(0x26, 0x05);
  WriteOPLLreg(0x27, 0x05);
  WriteOPLLreg(0x28, 0x01);
  WriteOPLLreg(0x0E, 0x20);
  /*
  // Volum i instruments dels drums
  WriteOPLLreg(0x36, 0x01);
  WriteOPLLreg(0x37, 0x44);
  WriteOPLLreg(0x38, 0x41);
  WriteOPLLreg(0x0E, 0x33);
  */
  // Volum i instruments canals 5 al 0
  WriteOPLLreg(0x25, 0x00);
  WriteOPLLreg(0x15, 0xAB);
  WriteOPLLreg(0x35, 0x11); // Provo un violí si fa el sustain. I sí que el fa
  //WriteOPLLreg(0x25, 0x1A); // El bit 5 (D4) del registre 0x20 conté si s'activa el canal o no
  // Diria que hi ha la freqüència i després l'octava el fa pujar o baixar. A la guia posa octava 4 i les freqüències de F
  WriteOPLLreg(0x15, 192); // La nota D segons el manual
  WriteOPLLreg(0x25, 0x3A); // Octava 5 i activat i sustained ; Fent proves amb el Moonblaster sembla que la veu piano no fa sustains. Provo de cofnigurar un violí
}

void main(){
  ChangeCPU(0);
  // FT_openFile("c2_c3.mbm");
  FT_openFile("pep.mbm");
  readFileSong();
  //play_sound();
  posSong = 0;
  posPatro = songFile[0x178 + capcalera.song_length + 2*ordrePatrons[posSong]-1] +
    (songFile[0x178 + capcalera.song_length + 2*ordrePatrons[posSong]] << 8); // El segon byte està després, és little endian. Multipliquem per 2 el patró ja que són 2 bytes
  InicialitzemCanals();
  uint8_t txt = (uint8_t)capcalera.drum_frequencies_music[0][0];
  printf("%d", txt);
  txt = (uint8_t)(capcalera.drum_frequencies_music[0][0] >> 8);
  printf("\n%d", txt);
  tempo = capcalera.start_tempo;
  printf("Instru ini 0: %d;\n",   capcalera.instrument_list_music[capcalera.start_instruments_music[0]-1].instrument);
  printf("Vol ini 0: %d\n",capcalera.instrument_list_music[capcalera.start_instruments_music[0]-1].volume);
  printf("Instru ini 1: %d;\n",
         capcalera.instrument_list_music[capcalera.start_instruments_music[1]-1]
             .instrument);
  printf("Vol ini 1: %d\n",
         capcalera.instrument_list_music[capcalera.start_instruments_music[1]-1]
             .volume);
  InitVDPInterruptHandler((int)playPatro);
  //WAIT(1200);
  WaitKey();
  printf("Acabem cI: %d;-;;", copsInterrupcio);
  EndVDPInterruptHandler();
  apaguemOPLL();
  ChangeCPU(1);
  Exit(0);
}

/* provadrum primer registre quan hi ha el 8 escriu a 0x0E<-0x12, 0x0E<-0x32
Quan hi ha el 4 escriu 0xE<-0x02, 0X0E<-0x22
Quan hi ha el 5 escriu 0x0E<-0x01, 0x0E<-0x21
Quan hi ha el 12 escriu 0x0E<-0x09, 0x0E<-0x29*/

/* Sembla que l'inici al no trobar cap nota no faci bé les columnes. He de mirar altres fitxers com es comporten */
