// Provant de fer focs d'artifici
/* El primer intent era massa aleatori i no donava bons resultats
 He trobat aquesta impementació en Python, sembla interessant
 https://pythonprogramming.altervista.org/particles-effects-in-videogames-with-pygame-and-python/?doing_wp_cron=1676579520.4029068946838378906250
Primer provaré fer el linked list que no l'he implementat mai
Després provaré de copiar la imatge en Sprite i fer-ho en Sprites
És el primer cop que creo linked list, estic basant-me en el llibre "Data Structures and Algorithms made easy"
Jo sempre afegiré al final, per tant puc fer més fàcil la funció d'inserir
Al llibre utilitzen el malloc perquè és dinàmic i reserven la memòria. Jo sé la longitud màxima i el lloc a on estaran, no necessito un punter al next, només la posició de l'array
No necessito una linked list, les bombolles aniran creixent i desapareixent, quan desapareix és quan creo una node nova a aquella posició, no necessito cridar-les per ordre com en un string, les aniré dibuixant igual
2022-02-20 L'exemple utilitzava sumes decimals, però he mirat el tema de float amb sdcc i no he entès com aplicar-lo. Faré una velocitat per les y i per les x en enters que sempre acabaran avançant 1
*/

#include "fusion-c/include/msx_fusion.h"
#include "fusion-c/include/vdp_graph2.h"
#include "fusion-c/include/vdp_sprites.h"

static const unsigned char ball_pattern[] = {
    0x3C, /* 00111100 */
    0x7E, /* 01111110 */
    0xFF, /* 11111111 */
    0xFF, /* 11111111 */
    0xFF, /* 11111111 */
    0xFF, /* 11111111 */
    0x7E, /* 01111110 */
    0x3C  /* 00111100 */
};

static const unsigned char ball_pattern2[] = {
    0b00000000, 0b00111100, 0b01111110, 0b01111110,
    0b01111110, 0b01111110, 0b00111100, 0b00000000};

static const unsigned char ball_pattern3[] = {
    0b00000000, 0b00000000, 0b00011000, 0b00111100,
    0b00111100, 0b00011000, 0b00000000, 0b00000000};

static const unsigned char ball_pattern4[] = {
    0b00000000, 0b00000000, 0b00000000, 0b00011000,
    0b00011000, 0b00000000, 0b00000000, 0b00000000};

struct particle{
  char pos_x;
  char pos_y;
  int canvi_x; // Pot prendre valors negatius
  char temps_canvi_x; 
  char temps_canvi_y;
  char radi; //Indicarà el número de patró que és el tamany de la bola
  char temps_dec_radi;
} ;

struct particle memoria_particles[32]; // 32 és el nombre màxim de sprites

#define HALT __asm halt __endasm // wait for the next interrupt
void WAIT(int cicles) {
  int i;
  for (i = 0; i < cicles; i++)
    HALT;
  return;
}

// Part aleatòria extreta de
// https://sourceforge.net/p/sdcc/feature-requests/464/

unsigned long y = 2463534242;

void rand_xor_init() { y = (_Time << 15) | (unsigned long)_Time; }

int rand_xor() {
  y = y ^ (y << 13);
  y = y ^ (y >> 17);
  y = y ^ (y << 5);
  return (y & 0x7fff);
}


void crea_particula(char k) {
  memoria_particles[k].pos_x = 120 + (rand_xor() & 15);
  memoria_particles[k].pos_y = 108 + (rand_xor() & 15);
  memoria_particles[k].canvi_x = (rand_xor() & 1) - (rand_xor() & 1);
  memoria_particles[k].temps_canvi_x = (rand_xor() & 3) + 4;
  memoria_particles[k].temps_canvi_y = (rand_xor() & 1) + 1;
  memoria_particles[k].radi = 3;
  memoria_particles[k].temps_dec_radi = (rand_xor() & 31) + 60;
}


void main(void) {
  rand_xor_init();

  Screen(5);

  Cls();

  SpriteSmall();
  SetSpritePattern(0, ball_pattern4, 8);
  SetSpritePattern(1, ball_pattern3, 8);
  SetSpritePattern(2, ball_pattern2, 8);
  SetSpritePattern(3, ball_pattern, 8);

  // Escombrem per crear les partícules i els patrons
  for (char k=0; k<32; k++) {
    crea_particula(k);
  }

  char aturar=0;
  while(aturar==0) {
    for (char k=0; k<12; k++) {
      PutSprite(k,memoria_particles[k].radi, (int)memoria_particles[k].pos_x, (int)memoria_particles[k].pos_y,0);
      memoria_particles[k].temps_dec_radi -= 1;
      if (memoria_particles[k].temps_dec_radi == 0) {
        memoria_particles[k].radi -= 1;
        memoria_particles[k].temps_dec_radi = (rand_xor() & 31) + 30;
      }
      memoria_particles[k].temps_canvi_x -=1;
      if (memoria_particles[k].temps_canvi_x==0){
        memoria_particles[k].pos_x += memoria_particles[k].canvi_x;
        memoria_particles[k].temps_canvi_x = (rand_xor() & 3) + 4;
      }
      memoria_particles[k].temps_canvi_y -=1;
      if (memoria_particles[k].temps_canvi_y == 0) {
        memoria_particles[k].pos_y -=1;
        memoria_particles[k].temps_canvi_y = (rand_xor() & 3) + 2;
      }
      if (memoria_particles[k].pos_y < 3 || memoria_particles[k].radi == 0) {
        // Hem de crear una nova partícula:
        crea_particula(k);
      }
    }
    char linia6 = GetKeyMatrix(6);
    if (((linia6 & 32) == 0)) {
      // Tecla F1
      aturar = 1;
    }
    WAIT(1);
  }

  Screen(0);
  /* rand_xor_init(); */

  /* PrintDec(rand_xor() & 31); */
  /* Locate(0, 2); */
  /* PrintDec(rand_xor() & 31); */
  /* Locate(0, 4); */
  /* PrintDec(rand_xor() & 31); */
  /* Locate(0,6); */
  /* PrintDec(rand_xor() & 31); */
  /* Locate(0, 8); */
  /* PrintDec(rand_xor() & 31); */
  /* Locate(0, 10); */
  /* PrintDec(rand_xor() & 31); */
  /* Locate(0, 15); */

  Exit(0);
}
