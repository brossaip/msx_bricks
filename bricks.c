/* Sobre el player del tracker he trobat informació a
 * https://www.msx.org/wiki/Music_replayer_routines que sembla que hi hagi les
 * fonts */
/* El pro tracker té un bas a on carrega com a replayer */
/* Aquí hi ha bastanta informació del compilador del fusion-c http://msx.tipolisto.es/category/aplicaciones/ */
#include "fusion-c/header/msx_fusion.h"
#include "fusion-c/header/vdp_graph1.h"
#include "fusion-c/header/vdp_sprites.h"
#include "fusion-c/header/pt3replayer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HALT __asm halt __endasm // wait for the next interrupt

static int xMan;
static int yMan;
static int xTotxo;
static int yTotxo;
static char collisio = 0;
static char sortir_bucle = 0;
static char copsVsync = 0;
static char semaforCollisions = 0;

const  char chase[] = 
{0x08, 0x0F, 0x00, 0x62, 0x00, 0x92, 0x00, 0xF8, 0x00, 0x4D, 0x01, 0xA8, 0x01, 0xC6, 0x01, 0x1E, 
   0x02, 0x6A, 0x50, 0x05, 0x1E, 0x0A, 0x2B, 0x50, 0x06, 0x0B, 0x2C, 0x50, 0x07, 0x0C, 0x6E, 0x50, 
   0x08, 0x0A, 0x0E, 0x0E, 0x0E, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6C, 0x50, 0x07, 0x1E, 0x0C, 
   0x2D, 0x50, 0x08, 0x0D, 0x2E, 0x50, 0x09, 0x0E, 0x6F, 0x52, 0x0A, 0x0A, 0x0F, 0x0F, 0x0F, 0x20, 
   0x00, 0x00, 0x00, 0x00, 0x00, 0x6C, 0x50, 0x09, 0x1E, 0x0C, 0x2D, 0x00, 0x0A, 0x0D, 0x2E, 0x50, 
   0x0A, 0x0E, 0x6F, 0x52, 0x0B, 0x0A, 0x0F, 0x0F, 0x0F, 0xD0, 0x20, 0x6E, 0x65, 0x6D, 0x65, 0x73, 
   0x69, 0x73, 0x5F, 0x31, 0x31, 0x00, 0xEF, 0x80, 0x00, 0x0A, 0xAF, 0x00, 0x01, 0xAE, 0x80, 0x01, 
   0xAE, 0x00, 0x02, 0xAD, 0x80, 0x02, 0xAD, 0x00, 0x03, 0xAE, 0x80, 0x01, 0xAE, 0x40, 0x01, 0xAD, 
   0x00, 0x01, 0xA8, 0xC0, 0x00, 0xA8, 0x80, 0x00, 0xA8, 0x60, 0x00, 0xD0, 0x20, 0x6E, 0x65, 0x6D, 
   0x65, 0x73, 0x69, 0x73, 0x5F, 0x31, 0x32, 0x00, 0x6D, 0x00, 0x01, 0x0A, 0x2E, 0x00, 0x03, 0x2D, 
   0x00, 0x01, 0x2E, 0x00, 0x03, 0x2C, 0x80, 0x00, 0xB0, 0x08, 0x09, 0xAC, 0xA0, 0x02, 0xAC, 0x60, 
   0x01, 0xB0, 0x08, 0x09, 0x29, 0xF0, 0x03, 0x09, 0x09, 0x29, 0x80, 0x03, 0x09, 0x09, 0x2A, 0x20, 
   0x03, 0x0A, 0x0A, 0x2A, 0xB0, 0x02, 0x0A, 0x0A, 0x2B, 0xA0, 0x02, 0x0B, 0x0B, 0x2B, 0x70, 0x02, 
   0x0B, 0x0B, 0x2B, 0xA0, 0x02, 0x0B, 0x0B, 0x2A, 0x00, 0x03, 0x0A, 0x0A, 0x29, 0x00, 0x04, 0x09, 
   0x09, 0x28, 0x00, 0x05, 0x08, 0x08, 0x27, 0x00, 0x06, 0x07, 0x07, 0x26, 0x00, 0x07, 0x06, 0x06, 
   0xA8, 0x01, 0x00, 0xD0, 0x20, 0x66, 0x69, 0x72, 0x65, 0x62, 0x69, 0x72, 0x64, 0x5F, 0x36, 0x00, 
   0xEB, 0x70, 0x02, 0x0A, 0x6F, 0xA0, 0x00, 0x1E, 0xB0, 0x09, 0x09, 0x90, 0x90, 0xAB, 0xA0, 0x02, 
   0xAD, 0xC0, 0x01, 0xAC, 0x1A, 0x02, 0xAD, 0x40, 0x02, 0xAC, 0x6A, 0x02, 0xAB, 0xD0, 0x02, 0xAA, 
   0x00, 0x05, 0xA9, 0x60, 0x06, 0xA8, 0x00, 0x02, 0xA8, 0x00, 0x03, 0xB0, 0x09, 0x09, 0x90, 0xA9, 
   0x1A, 0x02, 0xAA, 0x40, 0x02, 0xAA, 0x6A, 0x02, 0xA9, 0xD0, 0x02, 0xA9, 0x00, 0x05, 0xA8, 0x60, 
   0x06, 0xA7, 0x00, 0x02, 0xA6, 0x00, 0x03, 0xA8, 0x01, 0x00, 0xD0, 0x20, 0x66, 0x69, 0x72, 0x65, 
   0x62, 0x69, 0x72, 0x64, 0x5F, 0x37, 0x00, 0x68, 0x18, 0x00, 0x00, 0x27, 0x10, 0x00, 0x29, 0x24, 
   0x00, 0x28, 0x10, 0x00, 0x2A, 0x24, 0x00, 0x29, 0x10, 0x00, 0x2B, 0x24, 0x00, 0x29, 0x10, 0x00, 
   0x2A, 0x24, 0x00, 0x29, 0x10, 0x00, 0x2A, 0x24, 0x00, 0x28, 0x10, 0x00, 0x29, 0x24, 0x00, 0x27, 
   0x10, 0x00, 0x28, 0x24, 0x00, 0x26, 0x10, 0x00, 0x27, 0x24, 0x00, 0x25, 0x10, 0x00, 0x26, 0x24, 
   0x00, 0x24, 0x10, 0x00, 0x25, 0x24, 0x00, 0x23, 0x10, 0x00, 0x24, 0x24, 0x00, 0x23, 0x10, 0x00, 
   0x23, 0x24, 0x00, 0xA8, 0x01, 0x00, 0xD0, 0x20, 0x66, 0x69, 0x72, 0x65, 0x62, 0x69, 0x72, 0x64, 
   0x5F, 0x31, 0x33, 0x00, 0xEB, 0x28, 0x00, 0x1E, 0x89, 0x88, 0x89, 0xA6, 0x29, 0x00, 0xA5, 0x28, 
   0x00, 0x84, 0x83, 0xA8, 0x01, 0x00, 0xD0, 0x20, 0x66, 0x69, 0x72, 0x65, 0x62, 0x69, 0x72, 0x64, 
   0x5F, 0x31, 0x32, 0x00, 0xED, 0xF0, 0x02, 0x02, 0xAD, 0xB0, 0x02, 0xAD, 0xF0, 0x02, 0xAD, 0xB0, 
   0x02, 0xAD, 0x80, 0x02, 0xAD, 0x50, 0x02, 0xAD, 0x38, 0x02, 0xAD, 0x20, 0x02, 0xAD, 0x08, 0x02, 
   0xAD, 0xF0, 0x01, 0xAD, 0xD8, 0x01, 0xAD, 0xC0, 0x01, 0xAC, 0x20, 0x02, 0xAC, 0x08, 0x02, 0xAC, 
   0xF0, 0x01, 0xAC, 0xD8, 0x01, 0xAC, 0xC0, 0x01, 0xAC, 0xA8, 0x01, 0xAC, 0x90, 0x01, 0xAB, 0xF0, 
   0x01, 0xAB, 0xD8, 0x01, 0xAB, 0xC0, 0x01, 0xAB, 0xA8, 0x01, 0xAB, 0x90, 0x01, 0xA5, 0x00, 0x00, 
   0xD0, 0x20, 0x6D, 0x65, 0x74, 0x61, 0x6C, 0x67, 0x65, 0x61, 0x72, 0x5F, 0x38, 0x00, 0xEE, 0x60, 
   0x00, 0x1E, 0xAE, 0x80, 0x00, 0xAE, 0x60, 0x00, 0xAE, 0x80, 0x00, 0xAE, 0xC0, 0x00, 0xAE, 0x00, 
   0x01, 0xAE, 0x40, 0x01, 0xAE, 0x80, 0x01, 0xAE, 0x40, 0x01, 0xAE, 0x00, 0x01, 0xAE, 0xC0, 0x00, 
   0xAE, 0x80, 0x00, 0xAE, 0x60, 0x00, 0xAC, 0x80, 0x00, 0xAC, 0xC0, 0x00, 0xAC, 0x00, 0x01, 0xAC, 
   0x40, 0x01, 0xAC, 0x80, 0x01, 0xAC, 0x40, 0x01, 0xAC, 0x00, 0x01, 0xAC, 0xC0, 0x00, 0xAC, 0x80, 
   0x00, 0xAC, 0x60, 0x00, 0xD0, 0x20, 0x6E, 0x65, 0x6D, 0x65, 0x73, 0x69, 0x73, 0x5F, 0x38, 0x00};

// Generates a pause in the execution of n interruptions.
// PAL: 50=1second. ; NTSC: 60=1second.
void WAIT(int cicles) {
  int i;
  for (i = 0; i < cicles; i++)
    HALT;
  return;
}

static void my_interrupt(void) {
  DisableInterrupt();
  if (IsVsync() == 1) {
    unsigned char linia8 = Rkeys(); // KeyboardRead deprecated used Rkeys
    if (copsVsync > 5) {
      if(collisio==0){
        if (linia8 == 128) {
          // dreta
          xMan = xMan + 16;
          if (SC2Point(xMan + 2, yMan + 8) == 6 ||
              SC2Point(xMan + 2, yMan + 8) == 9 || xMan > 224) {
            xMan = xMan - 16;
          }
        }
        else if (linia8 == 16) {
          //esquerra
          xMan = xMan - 16;
          // Detectem que hi ha maó per tornar-lo a la posició
          if (SC2Point(xMan + 2, yMan + 8) == 6 || SC2Point(xMan + 2, yMan + 8) == 9 ||
              xMan < 48) {
            xMan = xMan + 16;
          }
        }
        else if (linia8 == 32) {
          // No podem saltar si estem caient
          if (SC2Point(xMan + 2, yMan + 24) == 6 ||
              SC2Point(xMan + 2, yMan + 24) == 9) {
                    yMan = yMan - 16;
          }
        }
        else if (linia8 == 160) {
          // No podem saltar si estem caient
          if (SC2Point(xMan + 2, yMan + 24) == 6 ||
              SC2Point(xMan + 2, yMan + 24) == 9) {
            xMan = xMan + 16;
            yMan = yMan - 16;
            if (SC2Point(xMan + 2, yMan + 8) == 6 ||
                SC2Point(xMan + 2, yMan + 8) == 9 || xMan > 224) {
              xMan = xMan - 16;
            }
          }
        }
        else if (linia8==48)
          {
            if (SC2Point(xMan + 2, yMan + 24) == 6 ||
                SC2Point(xMan + 2, yMan + 24) == 9) {
              xMan = xMan - 16;
              yMan = yMan - 16;
              if (SC2Point(xMan + 2, yMan + 8) == 6 ||
                  SC2Point(xMan + 2, yMan + 8) == 9 || xMan < 48) {
                xMan = xMan + 16;
              }
            }
          }
        PutSprite(1, 4, xMan, yMan, 10);
      }
      if (linia8 == 1) {
        sortir_bucle = 1;
      }
      copsVsync = 0;
    }
    // detectem col·lisió
    if( !((xMan >= xTotxo + 10) || (xMan+10<=xTotxo) || (yMan>=yTotxo+10) || (yMan + 10<=yTotxo )) && semaforCollisions == 0) {
      collisio = 1;
      semaforCollisions = 1;
    }
    copsVsync++;
  }
  EnableInterrupt();
}

static const unsigned int mao[] =
{
 0b0111111101111111,
 0b0111111101111111,
 0b0111111101111111,
 0b0111111101111111,
 0b0000000000000000,
 0b0111011111110111,
 0b0111011111110111,
 0b0111011111110111,
 0b0111011111110111,
 0b0000000000000000,
 0b0111111101111111,
 0b0111111101111111,
 0b0111111101111111,
 0b0111111101111111,
 0b0000000000000000,
 0b0000000000000000
};
static const unsigned int prota[]=
{
 0b0000001100000000,
 0b0000011110000000,
 0b0000011010000000,
 0b0000011110000000,
 0b0000001011000000,
 0b0000111111100000,
 0b0001111110100000,
 0b0010011110100000,
 0b0010011110100000,
 0b0001111110000000,
 0b0000011110000000,
 0b0000010110000000,
 0b0000010011000000,
 0b0000110011000000,
 0b0000000011000000,
 0b0000000000000000
};

void preparar_pantalla(void){
  Screen(2);
  SC2BoxLine(50,40,210,160,15); //SC2Rect replaced by SC2BoxLine
  PutText(85,70,"B R I C K S",8);
  PutText(115,100,"by",8);
  PutText(60,140,"Jose-D. Caldevilla",8);
  PutText(60,180,"Ported to C by Josep Bonet Domenech",8);
  SpriteReset();
  SpriteSmall();
  Sprite16();
  SetSpritePattern(0, Sprite32Bytes(mao),32);
  SetSpritePattern(4, Sprite32Bytes(prota),32);
  PutSprite(0,0,120,120,8);
  PutSprite(1,4,120,140,10);
}
void pinta_totxos(int T,int I){
  // Pintem els totxos 
  SC2BoxFill(T,I,T+6,I+3,6);
  SC2BoxFill(T+8,I,T+14,I+3,6);
  //
  SC2BoxFill(T,I+5,T+2,I+8,6);
  SC2BoxFill(T+4,I+5,T+10,I+8,6);
  //
  SC2BoxFill(T+12,I+5,T+14,I+8,9);
  SC2BoxFill(T,I+10,T+6,I+13,9);
  //
  SC2BoxFill(T+8,I+10,T+14,I+13,9);
}

int main(void) {
  // Inicialitzem números aleatoris
  TIME tm;      // Init the Time Structure variable
  GetTime(&tm); // Retreive MSX-DOS Clock, and set the tm strucutre with
  // clock's values
  srand(
        tm.sec); // use current clock seconds as seed in the random generator

  static int vides = 114;
  static int score = 60 ;

  char generem_nou_totxo = 1;

  // L són les vides
  // X,Y les coordenades del personatge
  // T,I les cooredenades del maó que està caient. Crea un número aleatori
  // fins al 12, ja que només hi caben 12 blocs de 16px
  preparar_pantalla();
  // uWaitForKey();
  Cls();
  // Pintar pantalla
  vides = 5;
  for (int t = 1; t <= 33; t = t + 16) {
    for (int i = 1; i <= 190; i = i + 16) {
      pinta_totxos(t, i);
    }
  }
  for (int t = 49; t <= 255; t = t + 16) {
    for (int i = 161; i <= 190; i = i + 16) {
      pinta_totxos(t, i);
    }
  }
  for (int t = 240; t <= 255; t = t + 16) {
    for (int i = 1; i <= 160; i = i + 16) {
      pinta_totxos(t, i);
    }
  }
  SC2BoxFill(49, 1, 239, 15, 7);
  PutText(100, 5, "Your chance", 1);
  SC2BoxFill(17, 49, 32, 159, 1);
  /* /\* Nom de joc a la pantalla *\/ */
  /* char *nom = "bm21," */
  /*   "52s6c10d7r3e1u1h2e2u1h1l3c4bd20u10r3f1d2g1l3f4d1c7bd7l4r2d7l" */
  /*   "2r4c12bd7bl1h1l2g1d6f1r2e1c15bd7g3u4d8u4f3c13bd7d1g1e1u1h1l2" */
  /*   "g1d2f1r2f1d2g1l2h1e1"; */
  /* SC2Draw(nom); // en el programa original xa$ realitza la comanda de draw */
  /* // anterior i bm21,52 situa el cursor */
  /* // El dibuix no queda igual */
  /* // Explicar les instruccions */
  /* Ja no apareix en el 1.3 */
  PutText(100, 40, "INSTRUCCIONS", 8);
  
  // Rectangles del marcador i vides
  SC2BoxFill(60, 171, 144, 180, 1);
  PutText(64, 172, "Score:   0", LOGICAL_IMP);
  SC2BoxFill(167, 171, 225, 180, 1);
  PutText(168, 172, "Lives: 5", LOGICAL_IMP);
  // Posició inicial home
  xMan = 144;
  yMan = 64;
  xTotxo = 0;
  yTotxo = 1;
  int color;
  // Preparem interrupcions
  InitVDPInterruptHandler((int)my_interrupt);
  InitPSG();
  PT3FXInit(chase,2);
  // ***** Comencem bucle principal ***
  // generem la posició del totxo a caure
  while (!sortir_bucle) {
    if(generem_nou_totxo){
      xTotxo = rand() % 12 + 1;
      xTotxo = xTotxo * 16 + 33;
      yTotxo = 1;
      generem_nou_totxo = 0;
    }
    if (SC2Point(xMan, yMan) == 1) {
      // Gosub 1100
    }
    PutSprite(0, 0, xTotxo, yTotxo, 8);
    WAIT(10); /* Aquest wait controla la velocitat dels maons */
    if (SC2Point(xTotxo + 2, yTotxo + 18) != 6) {
      yTotxo = yTotxo + 16;
      PutSprite(0, 0, xTotxo, yTotxo, 8);
    } else {
      pinta_totxos(xTotxo, yTotxo);
      generem_nou_totxo = 1;
      score = score +15;
      char str_score[6];
      Itoa(score, str_score, 10);
      SC2BoxLine(120, 171, 144, 180, 1);
      PutText(120, 172, str_score, LOGICAL_IMP);
    }
    // Detectem si el jugador encara pot anar baixant
    color = SC2Point(xMan+2, yMan+17);
    if (SC2Point(xMan + 2, yMan + 17) != 6 &&
        SC2Point(xMan + 2, yMan + 17) != 9) {
      yMan = yMan + 16;
      PutSprite(1, 4, xMan, yMan, 10);
    }
    // Detectem teclat ho fem a la interrupció, no aquí, ja que queda controlat per interrupcions i va més suau
    // Col·lisió, també es fa a la interrupció ja que quan s'usa la VDP interruption posa l'sprite detection a 0
    if (collisio==1){
      // Efectes de xoc
      PT3FXPlay(1,5);

      // llums de colors en els sprites
      for(int xx=2; xx<15; xx=xx+2) {
        PutSprite(1, 4, xMan, yMan, xx + 1);
        PutSprite(0, 0, xTotxo, yTotxo, xx);
        WAIT(30);
      }
      yMan = yMan - 16;
      PutSprite(1, 4, xMan, yMan, 10);
      // Restem vida i mirem si és 0. Si és 0 final de joc
      vides = vides - 1;
      // Pintem les noves vides
      char str_vides[1];
      Itoa(vides, str_vides, 10);
      SC2BoxLine(200, 171, 225, 180, 1);
      PutText(202, 173, str_vides, LOGICAL_IMP);
      if (vides == 0) {
        // Fase Final
        sortir_bucle = 1;
      }
      collisio = 0;
      semaforCollisions = 0;
    }
    Halt();
  }
  // Mirem el desplaçament de l'home

  /* char columna_pantalla[192*(3+2)]; */
  /* for(int k=0; k<192; k++) { */
  /*   char pintura[10]; */
  /*   Itoa(SC2Point(xTotxo,k), pintura, 10); */
  /*   StrConcat(columna_pantalla, pintura); */
  /*   if(k%20==0){ */
  /*     StrConcat(columna_pantalla, "\n"); */
  /*   } */
  /* } */

  /* // Bucle principal */
  EndVDPInterruptHandler();
  PT3Mute();
  Screen(0);
  Cls();
  // printf("\nvides: %d", vides);
  /* Print(columna_pantalla); */
  Exit(0);
}

// Posició col·lisió pinta sprite colors és 0BB9 i l'últim del bucle és quan el maó està blanc.
// El 17d1 és pintar el rectangle; 0c57 és un halt
// En el 0bE6 és el punt que resta les vides
