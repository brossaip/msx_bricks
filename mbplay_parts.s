.area _HEADER
                                ;
; conditionals
;
withBIN	.equ	0	;1=include .bin header
withTurboR	.equ	1	;1=include TurboR supported code
				;  (pre-select and timing)
withMapper	.equ	1	;1=include Mapper supported code
withHook	.equ	1	;1=strmus will set the hook
				;0=you will need to call musint
withFade	.equ	1	;1=include fade-out code

;
;
;
.if withBIN=1
	.db	#0x0fe
  ;; 	.dw	start,end,start
	.org	#0x0b000

start:
.endif

;
;------- MoonBlaster 1.4 driver -------
;
; strmus = music start
; stpmus = music stop
; cntmus = music continue
; hltmus = music pause
;
chips:	.db	#1	;soundchip:
			; 0 = msx-audio
			; 1 = msx-music
			; 2 = stereo
busply:	.db	#0 	;status:
			; 0 = not playing
			; 255 = busy playing
.if withMapper=1
muspge:	.db	#3	;music data mapper segment
.endif
musadr:	.dw	#0x08000	;music data base address
pos:	.db	0	;current position
step:	.db	0	;current step
status:	.db	0,0,0	;3 status bytes
stepbf:	.ds	#13	;step data to be played on the NEXT interrupt
			;or has been played previously.

;Note:	To start FADE, fill FADSPD with the fade-out speed (1=fastest and
;	255=slowest), followed by writing 255 to FADING

;Note:	To read the current volumes for a channel the following formula can
;	be used (withFade=1 only):
;	LASPL1 + (channel-1) * 25 + 22	MSX-AUDIO volume
;	LASPL1 + (channel-1) * 25 + 23	MSX-MUSIC volume

.area _CODE
;------- music start -------

strmus:	di
	ld	a,(busply)
	or	a
	ret	nz
	ld	hl,#0
	ld	(status),hl
	ld	(status+1),hl
	xor	a
	ld	(spdcnt),a
	ld	(trbpsg),a
.if withFade=1
	ld	(fading),a
.endif
	dec	a
	ld	(pos),a
	ld	(busply),a
.if withMapper=1
	in	a,(#0x0fe)
	push	af
	ld	a,(muspge)
	out	(#0x0fe),a
.endif
	ld	hl,(musadr)
	ld	de,(xleng)
	ld	bc,#207
	ldir
	ld	c,#41
	add	hl,bc
	ld	c,#128
	ldir
	ld	(xpos),hl
	ld	a,(xleng)
	inc	a
	ld	e,a
	ld	d,#0
	add	hl,de
	ld	(patadr),hl
.if withTurboR
	ld	hl,(pc2out)
	ld	de,(mm2out)
	ld	bc,(pc3out)
	ld	ix,(mm3out)
	ld	a,(#0x02d)
	cp	#3
	jr	c,(strms2)
	ld	a,#0x03d
	ld	(trbpsg),a
	ld	(trbpsg+1),a
	call	#0x0183
	or	a
	jr	z,(strms2)
	ld	hl,(parout)
	ld	de,(mmrout)
	ld	bc,(parout)
	ld	ix,(mmrout)
strms2:	ld	(pacout+1),hl
	ld	(fpcout+1),bc
	ld	(mmout+1),de
	ld	(fmmout+1),ix
.endif
	ld	hl,(chnwc1)
	ld	de,(chnwc1+1)
	ld	bc,#9
	ld	(hl),#0
	ldir
	ld	a,(chips)
	or	a
	call	z,(setaud)
	dec	a
	call	z,(setmus)
	dec	a
	call	z,(setste)

	ld	b,#9
	ld	iy,(xbegvm+8)
	ld	ix,(xrever)
.if withFade=1
	ld	hl,(laspl1+200+6)
	ld	de,#-30
.else
	ld	hl,(laspl1+176+6)
	ld	de,#-27
.endif
mrlus:	ld	(hl),#0
	inc	hl
	inc	hl
	inc	hl
	ld	a,0(ix)
	ld	(hl),a
	inc	hl
	ld	a,0(iy)
	ld	(hl),a
	inc	hl
	ld	a,9(iy)
	ld	(hl),a
	inc	ix
	dec	iy
	add	hl,de
	djnz	(mrlus)

	call	(sinsmm)
	call	(sinspa)
	ld	a,(xtempo)
	ld	(speed),a
	ld	a,#15
	ld	(step),a
.if withFade=1
	ld	(maxpsg),a
.endif
	ld	a,#48
	ld	(tpval),a
.if withMapper=1
	pop	af
	out	(#0x0fe),a
.endif
.if withHook=1
strms3:
	di
	ld	hl,#0x0fd9
	ld	de,(oldint)
	ld	bc,#5
	ldir
	ld	a,#0x0c3
	ld	(#0x0fd9f),a
	ld	hl,(musint)
	ld	(#0x0fda0),hl
	ei
.endif
	ret


;--- MSX-AUDIO initial instruments

sinsmm:	ld	de,(mmrgad)
	ld	hl,(xbegvm)
	ld	iy,(laspl1+20)

	ld	b,#9
sinsm4:	push	bc
	push	hl
	push	de
	ld	b,(hl)
	ld	hl,(xmmvoc-9)
	ld	de,#9
sinsm3:	add	hl,de
	djnz	(sinsm3)
	pop	de

	push	hl
	inc	hl
	inc	hl
	ld	a,(hl)
	ld	0(iy),a
.if withFade=1
	inc	hl
	ld	a,(hl)
	ld	2(iy),a
	ld	bc,#5
	add	hl,bc
	ld	a,(hl)
	ld	4(iy),a
.endif
	pop	hl

	ld	b,#9
sinsm2:	ld	a,(de)
	ld	c,a
	ld	a,(hl)
	inc	de
	inc	hl
	call	(mmout)
	djnz	(sinsm2)
	pop	hl
	inc	hl
.if withFade=1
	ld	bc,#25
.else
	ld	bc,#22
.endif
	add	iy,bc
	pop	bc
	djnz	(sinsm4)

	ld	b,#4
	ld	hl,(strreg)
sinsm5:	ld	c,(hl)
	inc	hl
	ld	a,(hl)
	inc	hl
	call	(fmmout)
	djnz	(sinsm5)
.if withFade=1
	ld	a,#255
	ld	(smpvlm),a
.endif
	ld	a,(xsust)
	and	#0b011000000
	ld	c,#0x0bd

	jp	fmmout

;--- MSX-MUSIC initial instruments

sinspa:	di
	ld	hl,(xbegvp)
	ld	iy,(laspl1+21)
	ld	b,#6
	ld	a,(xsust)
	bit	5,a
	push	af
	jr	nz,sinsp2
	ld	b,#9
sinsp2:	ld	c,#0x30
sinspi:	push	bc
	push	hl
	ld	a,(hl)
	ld	hl,(xpasti-2)
	add	a,a
	ld	c,a
	ld	b,#0
	add	hl,bc
	ld	a,(hl)
	cp	#16
	call	nc,sinspo
	rlca
	rlca
	rlca
	rlca
	inc	hl
	ld	b,(hl)
	add	a,b
.if withFade=1
	rlc	b
	rlc	b
	ld	2(iy),b
	ld	bc,#25
.else
	ld	bc,#22
.endif
	add	iy,bc
	pop	hl
	pop	bc
	call	pacout
	inc	hl
	inc	c
	djnz	sinspi
	xor	a
	ld	c,#0x0e
	call	fpcout
	pop	af
	ret	z

	ld	de,(xdrvol)
	ld	hl,(drmreg)
	ld	b,#9
setdrm:	ld	c,(hl)
	ld	a,(de)
	call	pacout
	inc	hl
	inc	de
	djnz	setdrm
.if withFade=1
	ld	a,(xdrvol)
	and	#0b01111
	rlca
	rlca
	ld	(laspl1+(6*25)+23),a
	ld	a,(xdrvol+1)
	and	#0b011110000
	rrca
	rrca
	ld	(laspl1+(7*25)+23),a
	ld	a,(xdrvol+2)
	and	#0b01111
	rlca
	rlca
	ld	(laspl1+(8*25)+23),a
.endif
	ret

sinspo:	push	hl
	sub	#15
	ld	b,a
	ld	hl,(xorgp1-8)
	ld	de,#8
sinpo2:	add	hl,de
	djnz	(sinpo2)
	push	hl
	inc	hl
	inc	hl
	ld	a,(hl)
	ld	0(iy),a
	pop	hl
	ld	bc,#0x0800
sinpo3:	ld	a,(hl)
	call	(pacout)
	inc	c
	inc	hl
	djnz	(sinpo3)
	pop	hl
	xor	a
	ret

setmus:	push	af
	ld	a,(xsust)
	and	#0b0100000
	ld	a,#2
	jr	z,(setau2)
	ld	hl,(chnwc1)
	ld	de,(chnwc1+1)
	ld	bc,#5
	ld	(hl),a
	ldir
	ld	(chnwc1+9),a
	pop	af
	ret
setaud:	push	af
	inc	a
setau2:	ld	hl,(chnwc1)
	ld	de,(chnwc1+1)
	ld	bc,#9
	ld	(hl),a
	ldir
	pop	af
	ret
setste:	ld	hl,(xstpr)
	ld	de,(chnwc1)
	ld	bc,#10
	ldir
	ret

;--- music continue ---

cntmus:	ld	a,(busply)
	or	a
	ret	nz
	dec	a
	ld	(busply),a
.if withHook=1
	jp	strms3
.else
	ret
.endif

;--- music interrupt routine ---

musint:
	di
	push	af
	ld	a,(busply)
	or	a
	jp	z,stpms3
.if withMapper=1
	in	a,(#0x0fe)
	push	af
	ld	a,(muspge)
	out	(#0x0fe),a
.endif
.if withFade=1
	ld	a,(fading)
	or	a
	call	nz,dofade
.endif
	call	dopsg
	call	dopit
	ld	a,(speed)
	ld	hl,(spdcnt)
	inc	(hl)
	cp	(hl)
	jp	nz,secint
	ld	(hl),#0
	ld	iy,(laspl1)
	ld	hl,(stepbf)
	ld	b,#9
	ld	de,(chnwc1)
intl1:	push	bc
	ld	a,(hl)
	or	a
	jp	z,intl3
	push	de
	push	hl
	cp	#97
	jp	c,onevn
	jp	z,offevt
	cp	#114
	jp	c,chgins
	cp	#177
	jp	c,chgvol
	cp	#180
	jp	c,chgste
	cp	#199
	jp	c,lnkevn
	cp	#218
	jp	c,chgpit
	cp	#224
	jp	c,chgbr1
	cp	#231
	jp	c,chgrev
	cp	#237
	jp	c,chgbr2
	cp	#238
	jp	c,susevt
	jp	chgmod

intl2:	pop	hl
	pop	de
.if withFade=1
intl3:	ld	bc,#25
.else
intl3:	ld	bc,#22
.endif
	add	iy,bc
	inc	de
	inc	hl
	pop	bc
	djnz	intl1
	call	mmdrum
	ld	a,(de)
	bit	1,a
	call	nz,pacdrm
	inc	hl
	ld	a,(hl)
	or	a
	jr	z,cmdint
	cp	#24
	jp	c,chgtmp
	jp	z,endop2
	cp	#28
	jp	c,chgdrs
	cp	#31
	jp	c,chgsta
	call	chgtrs
cmdint:
endint:
.if withMapper=1
	pop	af
	out	(#0x0fe),a
.endif
	pop	af
oldint:	ret
.if withHook = 1
	ret
	ret
	ret
	ret
.endif

secint:	dec	a
	cp	(hl)
	jr	nz,endint
	ld	a,(step)
	inc	a
	and	#0b01111
	ld	(step),a
	ld	hl,(patpnt)
	call	z,posri3
	ld	de,(stepbf)
	ld	c,#13
dcrstp:	ld	a,(hl)
	cp	#243
	jr	nc,crcdat
	ld	(de),a
	inc	de
	dec	c
crcdt2:	inc	hl
	ld	a,c
	or	a
	jr	nz,dcrstp
	ld	(patpnt),hl
	jp	secin2
crcdat:	sub	#242
	ld	b,a
	xor	a
crclus:	ld	(de),a
	inc	de
	dec	c
	djnz	crclus
	jr	crcdt2


secin2:	ld	iy,(laspl1)
	ld	hl,(stepbf)
	ld	b,#9
	ld	de,(chnwc1)
intl4:	push	bc
	ld	a,(hl)
	or	a
	jr	z,intl5
	cp	#97
	jr	nc,intl5
	push	hl
	ld	6(iy),#0
	ld	c,a
	push	de
	push	bc
	ld	a,(de)
	push	af
	and	#0b010
	call	nz,pacple
	pop	af
	pop	bc
	and	#0b01
	call	nz,mmple
	pop	de
	pop	hl
.if withFade=1
intl5:	ld	bc,#25
.else
intl5:	ld	bc,#22
.endif
	add	iy,bc
	inc	de
	inc	hl
	pop	bc
	djnz	intl4
	jp	endint

mmple:	ld	a,(tpval)
	add	a,c
	cp	#96+#48+#1
	jr	c,mmpl4
	sub	#96
	jr	mmpl3
mmpl4:	cp	#1+#48
	jr	nc,mmpl3
	add	a,#96
mmpl3:	sub	#48
	ld	0(iy),a
	ld	hl,(pafreq-1)
	add	a,a
	add	a,l
	ld	l,a
	jr	nc,mmpl2
	inc	h
mmpl2:	ld	d,(hl)
	dec	hl
	ld	e,(hl)
	ex	de,hl
	add	hl,hl
	ld	a,l
	ld	e,9(iy)
	add	a,e
	add	a,e
	ld	l,a
	dec	hl
	ld	16(iy),h
	ld	17(iy),l
	ret

pacple:	ld	a,(tpval)
	add	a,c
	cp	#96+#48+#1
	jr	c,pacpl4
	sub	#96
	jr	pacpl3
pacpl4:	cp	#1+#48
	jr	nc,pacpl3
	add	a,#96
pacpl3:	sub	#48
	ld	5(iy),a
	ld	hl,(pafreq-1)
	add	a,a
	add	a,l
	ld	l,a
	jr	nc,pacpl2
	inc	h
pacpl2:	ld	a,(hl)
	ld	18(iy),a
	dec	hl
	ld	a,(hl)
	add	a,9(iy)
	ld	19(iy),a
	ret

onevn:	ld	a,(de)
	push	af
	and	#0b010
	call	nz,pacpl
	pop	af
	and	#0b01
	call	nz,mmpl
	jp	intl2

offevt:	ld	6(iy),#0
	ld	a,(de)
	push	af
	and	#0b010
	call	nz,offpap
offet2:	pop	af
	and	#0b01
	call	nz,offmmp
	jp	intl2

susevt:	ld	6(iy),#0
	ld	a,(de)
	push	af
	and	#0b010
	call	nz,suspap
	jr	offet2

chgins:	push	de
	ld	6(iy),#0
	sub	#97
	ld	c,a
	ld	a,(de)
	push	af
	push	bc
	and	#0b010
	call	nz,chpaci
	pop	bc
	pop	af
	and	#0b01
	call	nz,chmodi
	pop	de
	jp	intl2

chgvol:	push	de
	sub	#114
	ld	c,a
	ld	a,(de)
	push	af
	push	bc
	and	#0b010
	call	nz,chpacv
	pop	bc
	pop	af
	and	#0b01
	call	nz,chmodv
	pop	de
	jp	intl2

chgste:	ld	c,a
	ld	a,(chips)
	cp	#2
	jp	nz,intl2
	call	chstdp
	jp	intl2

lnkevn:	sub	#189
	ld	c,a
	push	bc
	ld	a,(de)
	push	af
	and	#0b010
	call	nz,chlkpa
	pop	af
	pop	bc
	and	#0b01
	call	nz,chlkmm
	jp	intl2

chgpit:	sub	#208
	ld	6(iy),#1
	ld	14(iy),a
	rlca
	jr	c,chgpi2
	ld	15(iy),#0
	jp	intl2
chgpi2:	ld	15(iy),#0x0ff
	ld	a,(de)
	push	af
	and	#0b010
	call	nz,chpidp
	pop	af
	and	#0b01
	call	nz,chpidm
	jp	intl2

chgbr1:	sub	#224
	jr	chgbr3
chgbr2:	sub	#230
chgbr3:	push	de
	ld	c,a
	ld	a,(de)
	push	af
	push	bc
	and	#0b010
	call	nz,chpcbr
	pop	bc
	pop	af
	and	#0b01
	call	nz,chmmbr
	pop	de
	jp	intl2

chgrev:	sub	#227
	ld	9(iy),a
	jp	intl2
chgmod:	ld	6(iy),#2
	jp	intl2

posri3:	ld	a,(xleng)
	ld	b,a
	ld	a,(pos)
	cp	b
	jr	nz,posri5
	ld	a,(xloop)
	cp	#255
	jr	z,posri4
	dec	a
posri5:	inc	a
	ld	(pos),a
	ld	c,a
	ld	b,#0
	ld	hl,(xpos)
	add	hl,bc
	ld	a,(hl)
	dec	a
	add	a,a
	ld	c,a
	ld	hl,(patadr)
	add	hl,bc
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	ex	de,hl
	ld	de,(musadr)
	add	hl,de
	ret
posri4:	xor	a
	ld	(busply),a
	dec	a
	jr	posri5


;----- MSX-AUDIO routines -----

;-- note on event --

mmpl:	ld	a,17(iy)
	ld	c,7(iy)
	call	fmmout

	ld  1(iy),a
	set	4,c
	ld	a,16(iy)
	call	fmmout
	set	5,a
	ld	2(iy),a
	jp	fmmout

;-- note off event --

offmmp:	ld	c,7(iy)
	ld	a,1(iy)
	call	fmmout
	ld	a,2(iy)
	set	4,c
	and	#0b011011111
	ld	2(iy),a
	jp	fmmout

;-- change instrument --

chmodi:	ld	a,c
	push	bc
	ld	10(iy),a
	ld	b,a
	ld	hl,(xmmvoc-9)
	ld	de,#9
chmoi2:	add	hl,de
	djnz	chmoi2
	pop	bc
	push	hl

	inc	hl	;move brightness
	inc	hl
	ld	a,(hl)
	ld	20(iy),a

	ld	a,#10
	sub	b
	ld	b,a
	ld	hl,(mmrgad-9)
	ld	de,#9
chmoi3:	add	hl,de
	djnz	chmoi3
	pop	de

	ld	b,#9
chmoi4:	ld	c,(hl)
	ld	a,(de)
	call	fmmout
	inc	hl
	inc	de
	djnz	chmoi4
.if withFade=1
	ex	de,hl
	dec	hl
	ld	a,(hl)
	ld	24(iy),a
	ld	de,#-5
	add	hl,de
	ld	b,22(iy)
	ld	a,(hl)
	ld	22(iy),a
	ld	a,(fading)
	or	a
	ret	z

	ld	a,(hl)
	and	#0b0111111
	ld	c,a
	ld	a,b
	and	#0b0111111
	cp	c
	ret	c
	ld	22(iy),b
	ld	a,b
	ld	c,13(iy)
	jp	fmmout
.else
	ret
.endif

;-- change volume --

chmodv:	ld	a,c
	ex	af,af'
	ld	b,10(iy)
	ld	hl,(xmmvoc-6)
	ld	de,#9
chmov2:	add	hl,de
	djnz	chmov2
	ld	a,(hl)
	and	#0b11000000
	ld	c,13(iy)
	ex	af,af'
	add	a,b
.if withFade=1
	call	fmmout
	ld	c,22(iy)
	ld	22(iy),a
	ld	a,(fading)
	or	a
	ret	z

	ld	a,b
	and	#0b0111111
	ld	b,a
	ld	a,c
	and	#0b0111111
	cp	b
	ret	c
	ld	22(iy),c
	ld	a,c
	ld	c,13(iy)
.endif
	jp	fmmout

;-- change stereo setting --

chstdp:	ld	6(iy),#0
	ld	a,c
	sub	#176
	ld	(de),a
	push	af
	and	#1
	call	z,moduit
	pop	af
	and	#0b010
	call	z,msxuit
	ret

;-- note linking, legato --

chlkmm:	ld	a,0(iy)
	add	a,c
	ld	0(iy),a
	ld	hl,(pafreq-1)
	add	a,a
	add	a,l
	ld	l,a
	jr	nc,chlkm2
	inc	h
chlkm2:	push	de
	ld	d,(hl)
	dec	hl
	ld	e,(hl)
	ex	de,hl
	add	hl,hl
	dec	hl
	pop	de
	ld	a,h
	ld	(mmfrqs),a
	ld	a,l
	ld	h,9(iy)
	add	a,h
	add	a,h
	ld	1(iy),a
	ld	c,7(iy)
	call	fmmout
	set	4,c
	ld	a,(mmfrqs)
	or	#0b0100000
	ld	2(iy),a
	ld	6(iy),#0
	jp	fmmout

;-- change brightness --

chmmbr:	ld	a,20(iy)
	and	#0b11000000
	ld	e,a
	ld	a,20(iy)
	and	#0b00111111
	ld	b,a
	ld	a,c
	add	a,b
	add	a,e
	ld	20(iy),a
	ld	c,13(iy)
	dec	c
	dec	c
	dec	c
	jp	fmmout

;-- pitch bending on --

chpidm:	ld	h,2(iy)
	bit	1,h
	ret	nz
	ld	a,h
	and	#0b11111100
	sub	#4
	ld	l,1(iy)
	res	5,h
	add	hl,hl
	ld	1(iy),l
	ex	af,af'
	ld	a,h
	and	#0b00000011
	ld	h,a
	ex	af,af'
	or	h
	ld	2(iy),a
	ret

;--- drumsamples ---

mmdrum:	ld	a,(de)
	rrca
	jr	nc,nommdr
	ld	a,(hl)
	or	a
	jp	z,mmdru2
	exx
	ld	hl,(mmpdt1-2)
	add	a,a
	ld	c,a
	ld	b,#0
	rl	b
	add	hl,bc
	ld	a,(hl)
	ld	c,#0x11
	call	fmmout
	inc	hl
	ld	a,(hl)
	dec	c
	call	fmmout
	exx

mmdru2:	inc	hl
	ld	a,(hl)
	or	a
	jp	z,mmdru3
	scf
	rla
.if withFade=1
	ld	b,a
	ld	a,(smpvlm)
	cp	b
	jr	c,mmdru4
	ld	a,b
.endif
mmdru4:	ld	c,#0x012
	call	fmmout
mmdru3:	inc	hl
	ld	a,(hl)
	and	#0b011110000
	or	a
	ret	z
	srl	a
	srl	a
	srl	a
	srl	a
	exx
	call	mdrblk
	exx
	ld	c,#7
	ld	a,#1
	call	fmmout
	ld	a,#0x0a0
	jp	fmmout
nommdr:	inc	hl
	inc	hl
	ret

mdrblk:	add	a,a
	add	a,a
	ld	hl,(smpadr-4)
	ld	c,a
	ld	b,#0
	add	hl,bc
	ld	c,#9
	ld	a,(hl)
	call	fmmout
	inc	hl
	ld	a,(hl)
	inc	c
	call	fmmout
	inc	hl
	ld	a,(hl)
	inc	c
	call	fmmout
	inc	hl
	ld	a,(hl)
	inc	c
	jp	fmmout

;----- MSX-MUSIC routines -----

;-- note on event --

pacpl:	ld	a,19(iy)
	ld	c,8(iy)
	call	fpcout
	ld	3(iy),a
	ld	a,c
	add	a,#0x010
	ld	c,a
	ld	a,18(iy)
	call	fpcout
	set	4,a
	ld	4(iy),a
	jp	fpcout

;--- note off event (sustained) ---

suspap:	ld	l,#0b0100000
	jr	offpa2

;--- note off event ---

offpap:	ld	l,#0
offpa2:	ld	c,8(iy)
	ld	a,3(iy)
	call	fpcout
	ld	a,c
	add	a,#0x010
	ld	c,a
	ld	a,4(iy)
	and	#0b011101111
	or	l
	ld	4(iy),a
	jp	fpcout

;-- change instrument --

chpaci:	ld	a,c
	ld	11(iy),a
	dec	a
	add	a,a
	ld	c,a
	ld	b,#0
	ld	hl,(xpasti)
	add	hl,bc
	ld	a,(hl)
	cp	#16
	jp	nc,chpaco
	rlca
	rlca
	rlca
	rlca
chpai2:	inc	hl
	ld	c,(hl)
.if withFade=1
	ld	l,a
	add	a,c
	push	bc
	ld	c,12(iy)
	call	fpcout
	pop	bc
	rlc	c
	rlc	c
	ld	b,23(iy)
	ld	23(iy),c
	ld	a,(fading)
	or	a
	ret	z
	ld	a,b
	cp	c
	ret	c
	ld	23(iy),b
	srl	b
	srl	b
	ld	a,l
	add	a,b
.else
	add	a,c
.endif
	ld	c,12(iy)
	jp	fpcout

chpaco:	exx
	sub	#15
	rlca
	rlca
	rlca
	ld	b,#0
	ld	c,a
	ld	hl,(xorgp1-8)
	add	hl,bc

	push	hl	;move brightness
	inc	hl
	inc	hl
	ld	a,(hl)
	ld	21(iy),a
	pop	hl

	ld	bc,#0x0800
chpao3:	ld	a,(hl)
	call	fpcout
	inc	c
	inc	hl
	djnz	chpao3
	exx
	xor	a
	jp	chpai2

;-- change volumes --

chpacv:	ld	a,c
.if withFade=1
	push	af
	srl	a
	srl	a
	ex	af,af'
.else
	srl	a
	srl	a
	ex	af,af'
	ld	hl,xbegvp
.endif
	ld	a,11(iy)
	ld	b,#0
	add	a,a
	ld	c,a
	ld	hl,(xpasti-2)
	add	hl,bc
	ld	a,(hl)
	cp	#16
	jr	c,chpcv2
	xor	a
chpcv2:	rlca
	rlca
	rlca
	rlca
	ld	b,a
	ld	c,12(iy)
	ex	af,af'
	xor	b
.if withFade=1
	call	fpcout
	ld	l,b
	pop	bc
	ld	c,23(iy)
	ld	23(iy),b
	ld	a,(fading)
	or	a
	ret	z

	ld	a,c
	cp	b
	ret	c
	ld	23(iy),c
	ld	a,c
	srl	a
	srl	a
	xor	l
	ld	c,12(iy)
.endif
	jp	fpcout

;-- note linking, legato --

chlkpa:	ld	a,5(iy)
	add	a,c
	ld	5(iy),a

	ld	hl,(pafreq-1)
	add	a,a
	add	a,l
	ld	l,a
	jr	nc,chlkp2
	inc	h
chlkp2:	ld	a,(hl)
	ld	(mmfrqs),a
	dec	hl
	ld	a,(hl)
	add	a,9(iy)
	ld	3(iy),a
	ld	c,8(iy)
	call	fpcout
	ld	a,c
	add	a,#0x010
	ld	c,a
	ld	a,(mmfrqs)
	or	#0b010000
	ld	4(iy),a
	ld	6(iy),#0
	jp	fpcout

;--- change brightness ---

chpcbr:	ld	a,11(iy)
	dec	a
	add	a,a
	ld	e,a
	ld	d,#0
	ld	hl,(xpasti)
	add	hl,de
	ld	a,(hl)
	cp	#16
	ret	c
	ld	a,21(iy)
	and	#0b11000000
	ld	e,a
	ld	a,21(iy)
	and	#0b00111111
	ld	b,a
	ld	a,c
	add	a,b
	add	a,e
	ld	21(iy),a
	ld	c,#2
	jp	fpcout

;-- verander pitch bending --

chpidp:	ld	a,4(iy)
	bit	0,a
	ret	nz
	dec	a
	ld	4(iy),a
	ld	a,3(iy)
	add	a,a
	ld	3(iy),a
	ret

;-- fm-pac drum --

pacdrm:	ld	a,(hl)
	and	#0b01111
	or	a
	ret	z
	ld	e,a
	ld	d,#0
	push	hl
	ld	hl,(xdrblk-1)
	add	hl,de
	ld	a,(hl)
	cp	#0b0100000
	ld	c,a
	call	nc,psgdrm
	pop	hl
	ld	a,(xsust)
	and	#0b0100000
	ret	z
	ld	a,c
	and	#0b011111
	ld	c,#0x0e
	call	fpcout
	set	5,a
	jp	fpcout

psgdrm:	rlca
	rlca
	rlca
	and	#0b0111
	add	a,a
	ld	e,a
	ld	hl,(psgadr-2)
	add	hl,de
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	ex	de,hl
	ld	a,(hl)
	ld	(psgcnt),a
	inc	hl
	ld	b,(hl)
	inc	hl
psgl1:	ld	a,(hl)
	out	(#0x0a0),a
	inc	hl
	ld	a,(hl)
	out	(#0x0a1),a
	inc	hl
	djnz	psgl1
.if withFade=1
	ld	a,(maxpsg)
	ld	b,a
	ld	a,(hl)
	cp	b
	jr	c,psgdr2
	ld	a,b
.else
	ld	a,(hl)
.endif
psgdr2:	ld	(psgvol),a
	jp	dopsg2

;---- command routines ----

chgtmp:	ld	b,a
	ld	a,#25
	sub	b
	ld	(speed),a
	jp	cmdint

endop2:	ld	a,#15
	ld	(step),a
	jp	cmdint

chgdrs:	sub	#25
	add	a,a
	ld	b,a
	add	a,a
	add	a,b
	ld	e,a
	ld	d,#0
	ld	hl,(xdrfrq)
	add	hl,de
	ex	de,hl
	ld	hl,(drmreg+3)
	ld	b,#6
chgdrl:	ld	c,(hl)
	ld	a,(de)
	call	fpcout
	inc	hl
	inc	de
	djnz	chgdrl
	jp	cmdint

;--- status bytes ---

chgsta:	ld	c,a
	ld	b,#0
	ld	hl,(status-28)
	add	hl,bc
	ld	(hl),#255
	jp	cmdint

;--- change transpose ---

chgtrs:	sub	#55-#48
	ld	(tpval),a
	ret
;--- 'every interrupt' routines ----

;--- music fade ---
.if withFade=1
dofade:	ld	a,(fadspd)
	ld	b,a
	ld	a,(fadcnt)
	inc	a
	ld	(fadcnt),a
	cp	b
	ret	nz
	xor	a
	ld	(fadcnt),a
	ld	iy,(laspl1)
	ld	b,#9
	ld	hl,#0
dofadl:	push	bc
	ld	a,22(iy)
	and	#0b11000000
	ld	b,a
	ld	a,22(iy)
	and	#0b0111111
	add	a,#2
	cp	#64
	jr	c,dofad2
	ld	a,#63
dofad2:	ld	c,13(iy)
	add	a,b
	ld	22(iy),a
	call	fmmout
	ld	b,a
	ld	a,24(iy)
	bit	0,a
	call	nz,dofada
	ld	a,b
	and	#0b111111
	xor	#63
	ld	e,a
	ld	d,#0
	add	hl,de
	pop	bc
	push	bc
	ld	a,b
	cp	#3
	jr	nc,dofad6
	ld	a,(xsust)
	bit	5,a
	jp	nz,dofad8
dofad6:	push	hl
	ld	a,11(iy)
	dec	a
	add	a,a
	ld	c,a
	ld	b,#0
	ld	hl,(xpasti)
	add	hl,bc
	ld	a,(hl)
	cp	#16
	jr	c,dofad5
	xor	a
dofad5:	rlca
	rlca
	rlca
	rlca
	ld	b,a
	ld	a,23(iy)
	add	a,#2
	cp	#64
	jr	c,dofad3
	ld	a,#63
dofad3:	ld	23(iy),a
	srl	a
	srl	a
	ld	c,12(iy)
	add	a,b
	call	fpcout
	pop	hl
	and	#0b1111
	xor	#15
	ld	e,a
	ld	d,#0
	add	hl,de
	ld	bc,#25
	add	iy,bc
	pop	bc
	dec	b
	jp	nz,dofadl

	ld	a,(smpvlm)
	sub	#12
	jr	nc,dofad4
	xor	a
dofad4:	ld	(smpvlm),a
	ld	c,#0x012
	call	fmmout
	ld	a,(maxpsg)
	or	a
	jr	nz,dofad9
	ld	a,#1
dofad9:	dec	a
	ld	(maxpsg),a
	ld	de,#0
	rst	#0x020
	ret	nz
	xor	a
	ld	(busply),a
	ret

dofad8:	push	hl
	ld	a,23(iy)
	srl	a
	srl	a
	jp	dofad5

dofada:	ld	a,20(iy)
	and	#0b11000000
	ld	c,a
	ld	a,20(iy)
	and	#0b0111111
	add	a,#2
	cp	#64
	jr	c,dofadb
	ld	a,#63
dofadb:	add	a,c
	ld	20(iy),a
	ld	c,13(iy)
	dec	c
	dec	c
	dec	c
	jp	fmmout
.endif
;--- psg drums ---

dopsg:	ld	a,(psgcnt)
	or	a
	ret	z
	dec	a
	ld	(psgcnt),a
	jr	z,endpsg
dopsg2:	ld	a,#8
	out	(#0x0a0),a
	ld	a,(psgvol)
.if withFade=1
	cp	#4
	jr	nc,dopsg3
	ld	a,#4
.endif
dopsg3:	sub	#2
	ld	(psgvol),a
trbpsg:	nop
	nop
	out	(#0x0a1),a
	ret
endpsg:	ld	a,#7
	out	(#0x0a0),a
	ld	a,#0x0b
	out	(#0x0a1),a
	ld	a,#8
	out	(#0x0a0),a
	xor	a
	out	(#0x0a1),a
	ret

;----- pitch bending -----

dopit:	ld	iy,(laspl1)
	ld	de,(chnwc1)
	exx
	ld	b,#9
.if withFade=1
	ld	de,#25
.else
	ld	de,#22
.endif
dopitl:	exx
	ld	a,6(iy)
	ld	h,a
	or	a
	jp	z,dopit2
	ld	a,(de)
	and	#0b01
	call	nz,pitmm
	ld	a,(de)
	and	#0b010
	call	nz,pitpa
dopit2:	inc	de
	exx
	add	iy,de
	djnz	dopitl
	ret

;---MSX-AUDIO pitch bend ---

pitmm:	push	hl
	dec	h
	jr	nz,modmm
	ld	l,14(iy)
	ld	h,15(iy)
	ld	c,1(iy)
	ld	b,2(iy)
	bit	7,h
	jr	nz,pitmm2
	add	hl,hl
	add	hl,bc
	ld	a,b
	and	#0b00000010
	ld	b,a
pitmm4:	ld	1(iy),l
	ld	c,7(iy)
	ld	a,l
	call	fmmout
	ld	a,h
	or	b
	ld	2(iy),a
	set	4,c
	pop	hl
	jp	fmmout

pitmm2:	add	hl,hl
	add	hl,bc
	bit	1,h
	jr	nz,pitmm3
	dec	h
	dec	h
pitmm3:	ld	b,#0
	jp	pitmm4

;--- MSX-MUSIC pitch bend ---

pitpa:	dec	h
	jr	nz,modpa
	ld	l,14(iy)
	ld	h,15(iy)
	ld	c,3(iy)
	ld	b,4(iy)
	bit	7,h
	jr	nz,pitpa2
	add	hl,bc
	ld	a,b
	and	#0b00000001
	ld	b,a
pitpa4:	ld	3(iy),l
	ld	c,8(iy)
	ld	a,l
	call	fpcout
	ld	a,c
	add	a,#0x010
	ld	c,a
	ld	a,h
	or	b
	ld	4(iy),a
	jp	fpcout
pitpa2:	add	hl,bc
	bit	0,h
	jr	nz,pitpa3
	dec	h
pitpa3:	ld	b,#0
	jp	pitpa4

;---- MSX-AUDIO modulate ----

modmm:	ld	a,h
	add	a,#2
	cp	#12
	jr	nz,modmm3
	ld	a,#2
modmm3:	ld	6(iy),a
	ld	a,h
	add	a,a
	ld	c,a
	ld	b,#0
	ld	hl,(modval-2)
	add	hl,bc
	ld	c,(hl)
	sla	c
	inc	hl
	ld	b,(hl)
	ld	l,1(iy)
	ld	h,2(iy)
	add	hl,bc
	ld	b,#0
	jp	pitmm4

;---- MSX-MUSIC modulate ----

modpa:	ld	a,(de)
	cp	#2
	jr	nz,modpa2
	ld	a,h
	add	a,#2
	cp	#12
	jr	nz,modpa3
	ld	a,#2
modpa3:	ld	6(iy),a
modpa2:	ld	a,h
	add	a,a
	ld	c,a
	ld	b,#0
	ld	hl,(modval-2)
	add	hl,bc
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	ld	l,3(iy)
	ld	h,4(iy)
	add	hl,bc
	ld	b,#0
	jp	pitpa4

;out-routines

mmout:	jp	mm2out
fmmout:	jp	mm3out
pacout:	jp	pc2out
fpcout:	jp	pc3out

mm2out:	ex	af,af'
	ld	a,c
	out	(#0x0c0),a
	ex	af,af'
	out	(#0x0c1),a
	ex	(sp),hl
	ex	(sp),hl
	ret
pc2out:	ex	af,af'
	ld	a,c
	out	(#0x7c),a
	ex	af,af'
	out	(#0x7d),a
	ex	(sp),hl
	ex	(sp),hl
	ret
mm3out:	ex	af,af'
	ld	a,c
	out	(#0x0c0),a
	ex	af,af'
	out	(#0x0c1),a
	ret
pc3out:	ex	af,af'
	ld	a,c
	out	(#0x7c),a
	ex	af,af'
	out	(#0x7d),a
	ret
.if withTurboR=1
mmrout:	ex	af,af'
	call	trbwt
	ld	a,c
	out	(#0x0c0),a
	in	a,(#0x0e6)
	ld	(rtel),a
	ex	af,af'
	out	(#0x0c1),a
	ret
parout:	ex	af,af'
	call	trbwt
	ld	a,c
	out	(#0x7c),a
	in	a,(#0x0e6)
	ld	(rtel),a
	ex	af,af'
	out	(#0x7d),a
	ret
trbwt:	push	bc
	ld	a,(rtel)
	ld	b,a
trbwl:	in	a,(#0x0e6)
	sub	b
	cp	#7
	jr	c,trbwl
	pop	bc
	ret
.endif

;------ music stop ------

stpms3:	call	stpms2
	pop	af
	ret

stpmus:
hltmus:	ld	a,(busply)
	or	a
	ret	z
stpms2:	di
.if withHook=1
	ld	hl,(oldint)
	ld	de,#0x0fd9
	ld	bc,#5
	ldir
.endif
.if withFade=1
hltms2:	ld	de,#25
.else
hltms2:	ld	de,#22
.endif
	ld	iy,(laspl1)
	ld	b,#9
hltmsl: call	msxuit
	call	moduit
	add	iy,de
	djnz	hltmsl
	xor	a
	ld	(busply),a
	ei
	jp	endpsg
msxuit:	ld	a,(xsust)
	bit	5,a
	jr	z,msxui2
	ld	a,b
	cp	#4
	ret	c
msxui2:	ld	c,8(iy)
	xor	a
	call	pacout
	ld	a,c
	add	a,#0x10
	ld	c,a
	xor	a
	jp	fpcout
moduit:	ld	c,7(iy)
	xor	a
	call	mmout
	set	4,c
	jp	fmmout

.area _DATA
;--- psg drum data ---

pbddat:	.db	#5,#3,#0,#179,#1,#6,#7,#0x0be,#17
ps1dat:	.db	#6,#2,#6,#0x013,#7,#0x0b7,#15
ps2dat:	.db	#6,#2,#6,#0x009,#7,#0x0b7,#15
pb1dat:	.db	#4,#3,#0,#173,#1,#1,#7,#0x0be,#15
pb2dat:	.db	#4,#3,#0,#72,#1,#0,#7,#0x0be,#15
ph1dat:	.db	#5,#2,#6,#0x006,#7,#0x0b7,#15
ph2dat:	.db	#5,#2,#6,#0x001,#7,#0x0b7,#14
psgadr:	.dw	(pbddat),(ps1dat),(ps2dat),(pb1dat),(pb2dat),(ph1dat),(ph2dat)
psgcnt:	.db	0
psgvol:	.db	0
.if withFade=1
maxpsg:	.db	0
.endif

;--- MSX-AUDIO data ---

mmpdt1:	.db	#0x005,#0x022,#0x005,#0x06a,#0x005,#0x0ba,#0x006,#0x012,#0x006,#0x073,#0x006,#0x0d3
	.db	#0x007,#0x03b,#0x007,#0x0ab,#0x008,#0x01b,#0x008,#0x09c,#0x009,#0x024,#0x009,#0x0ac
	.db	#0x00a,#0x03d,#0x00a,#0x0d5,#0x00b,#0x07d,#0x00c,#0x02d,#0x00c,#0x0e6,#0x00d,#0x0a6
	.db	#0x00e,#0x07f,#0x00f,#0x057,#0x010,#0x03f,#0x011,#0x038,#0x012,#0x040,#0x013,#0x051
	.db	#0x014,#0x07a,#0x015,#0x0aa,#0x016,#0x0fb,#0x018,#0x05b,#0x019,#0x0c4,#0x01b,#0x04d
	.db	#0x01c,#0x0fe,#0x01e,#0x0b6,#0x020,#0x077,#0x022,#0x070,#0x024,#0x081,#0x026,#0x0aa
	.db	#0x028,#0x0fc,#0x02b,#0x05d,#0x02d,#0x0f6,#0x030,#0x0af,#0x033,#0x089,#0x036,#0x0a2
	.db	#0x039,#0x0f4,#0x03d,#0x066,#0x040,#0x0f7,#0x044,#0x0e1,#0x049,#0x00b,#0x04d,#0x04d
	.db	#0x051,#0x0f0,#0x056,#0x0b2,#0x05b,#0x0ec,#0x061,#0x05f,#0x067,#0x01a,#0x06d,#0x045
	.db	#0x073,#0x0e8,#0x07a,#0x0cb,#0x081,#0x0f0,#0x089,#0x0c4,#0x092,#0x018,#0x09a,#0x0a4
mmrgad:	.db	#0x20,#0x23,#0x40,#0x43,#0x60,#0x63,#0x80,#0x83,#0x0c0	;k1
	.db	#0x21,#0x24,#0x41,#0x44,#0x61,#0x64,#0x81,#0x84,#0x0c1	;k2
	.db	#0x22,#0x25,#0x42,#0x45,#0x62,#0x65,#0x82,#0x85,#0x0c2	;k3
	.db	#0x28,#0x2b,#0x48,#0x4b,#0x68,#0x6b,#0x88,#0x8b,#0x0c3	;k4
	.db	#0x29,#0x2c,#0x49,#0x4c,#0x69,#0x6c,#0x89,#0x8c,#0x0c4	;k5
	.db	#0x2a,#0x2d,#0x4a,#0x4d,#0x6a,#0x6d,#0x8a,#0x8d,#0x0c5	;k6
	.db	#0x30,#0x33,#0x50,#0x53,#0x70,#0x73,#0x90,#0x93,#0x0c6	;k7
	.db	#0x31,#0x34,#0x51,#0x54,#0x71,#0x74,#0x91,#0x94,#0x0c7	;k8
	.db	#0x32,#0x35,#0x52,#0x55,#0x72,#0x75,#0x92,#0x95,#0x0c8	;k9
smpadr:	.dw	#0x0000,#0x03ff,#0x0400,#0x07ff,#0x0800,#0x0bff,#0x0c00,#0x0fff
	.dw	#0x1000,#0x13ff,#0x1400,#0x17ff,#0x1800,#0x1bff,#0x1c00,#0x1fff
	.dw	#0x0000,#0x07ff,#0x0800,#0x0fff,#0x1000,#0x17ff,#0x1800,#0x1fff
	.dw	#0x0000,#0x0fff,#0x1000,#0x1fff
strreg:	.db	16,#0x0f0,#17,#0x051,#18,#255,#0x018,#0

;--- MSX-MUSIC data ---

pafreq:	.db	#0x0ad,#0x000,#0x0b7,#0x000,#0x0c2,#0x000,#0x0cd,#0x000,#0x0d9,#0x000,#0x0e6,#0x000
	.db	#0x0f4,#0x000,#0x003,#0x001,#0x012,#0x001,#0x022,#0x001,#0x034,#0x001,#0x046,#0x001
	.db	#0x0ad,#0x002,#0x0b7,#0x002,#0x0c2,#0x002,#0x0cd,#0x002,#0x0d9,#0x002,#0x0e6,#0x002
	.db	#0x0f4,#0x002,#0x003,#0x003,#0x012,#0x003,#0x022,#0x003,#0x034,#0x003,#0x046,#0x003
	.db	#0x0ad,#0x004,#0x0b7,#0x004,#0x0c2,#0x004,#0x0cd,#0x004,#0x0d9,#0x004,#0x0e6,#0x004
	.db	#0x0f4,#0x004,#0x003,#0x005,#0x012,#0x005,#0x022,#0x005,#0x034,#0x005,#0x046,#0x005
	.db	#0x0ad,#0x006,#0x0b7,#0x006,#0x0c2,#0x006,#0x0cd,#0x006,#0x0d9,#0x006,#0x0e6,#0x006
	.db	#0x0f4,#0x006,#0x003,#0x007,#0x012,#0x007,#0x022,#0x007,#0x034,#0x007,#0x046,#0x007
	.db	#0x0ad,#0x008,#0x0b7,#0x008,#0x0c2,#0x008,#0x0cd,#0x008,#0x0d9,#0x008,#0x0e6,#0x008
	.db	#0x0f4,#0x008,#0x003,#0x009,#0x012,#0x009,#0x022,#0x009,#0x034,#0x009,#0x046,#0x009
	.db	#0x0ad,#0x00a,#0x0b7,#0x00a,#0x0c2,#0x00a,#0x0cd,#0x00a,#0x0d9,#0x00a,#0x0e6,#0x00a
	.db	#0x0f4,#0x00a,#0x003,#0x00b,#0x012,#0x00b,#0x022,#0x00b,#0x034,#0x00b,#0x046,#0x00b
	.db	#0x0ad,#0x00c,#0x0b7,#0x00c,#0x0c2,#0x00c,#0x0cd,#0x00c,#0x0d9,#0x00c,#0x0e6,#0x00c
	.db	#0x0f4,#0x00c,#0x003,#0x00d,#0x012,#0x00d,#0x022,#0x00d,#0x034,#0x00d,#0x046,#0x00d
	.db	#0x0ad,#0x00e,#0x0b7,#0x00e,#0x0c2,#0x00e,#0x0cd,#0x00e,#0x0d9,#0x00e,#0x0e6,#0x00e
	.db	#0x0f4,#0x00e,#0x003,#0x00f,#0x012,#0x00f,#0x022,#0x00f,#0x034,#0x00f,#0x046,#0x00f
drmreg:	.db	#0x36,#0x37,#0x38,#0x16,#0x26,#0x17,#0x27,#0x18,#0x28

;--- common data ---

chnwc1:	.dw	0,0,0,0,0
modval:	.dw	1,2,2,-2,-2,-1,-2,-2,2,2
mmfrqs:	.db	0
.if withFade=1
tpval:	.db	0
.endif
speed:	.db	0
spdcnt:	.db	0
rtel:	.db	0
patadr:	.dw	0
patpnt:	.dw	0
.if withFade=1
xpos:	.dw	0
smpvlm:	.db	0
fading:	.db	0
fadspd:	.db	#12
fadcnt:	.db	#0
laspl1:	.db	#0,#0,#0,#0,#0,#0,#0,#0xa0,#0x10,#0x0,#0,#0,#0x030,#0x043,#0x0,#0,#0,#0,#0,#0,#0,#0,#0,#0,#0
	.db	0,0,0,0,0,0,0,#0x0a1,#0x11,#0x0,0,0,#0x031,#0x044,#0x0,0,0,0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,#0x0a2,#0x12,#0x0,0,0,#0x032,#0x045,#0x0,0,0,0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,#0x0a3,#0x13,#0x0,0,0,#0x033,#0x04b,#0x0,0,0,0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,#0x0a4,#0x14,#0x0,0,0,#0x034,#0x04c,#0x0,0,0,0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,#0x0a5,#0x15,#0x0,0,0,#0x035,#0x04d,#0x0,0,0,0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,#0x0a6,#0x16,#0x0,0,0,#0x036,#0x053,#0x0,0,0,0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,#0x0a7,#0x17,#0x0,0,0,#0x037,#0x054,#0x0,0,0,0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,#0x0a8,#0x18,#0x0,0,0,#0x038,#0x055,#0x0,0,0,0,0,0,0,0,0,0,0
.else
tpval:	.db	0
xpos:	.dw	0
laspl1:	.db	0,0,0,0,0,0,0,0a0,#0x10,#0x0,0,0,030,#0x043,#0x0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,0a1,#0x11,#0x0,0,0,031,#0x044,#0x0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,0a2,#0x12,#0x0,0,0,032,#0x045,#0x0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,0a3,#0x13,#0x0,0,0,033,#0x04b,#0x0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,0a4,#0x14,#0x0,0,0,034,#0x04c,#0x0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,0a5,#0x15,#0x0,0,0,035,#0x04d,#0x0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,0a6,#0x16,#0x0,0,0,036,#0x053,#0x0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,0a7,#0x17,#0x0,0,0,037,#0x054,#0x0,0,0,0,0,0,0,0
	.db	0,0,0,0,0,0,0,0a8,#0x18,#0x0,0,0,038,#0x055,#0x0,0,0,0,0,0,0,0
.endif

;copy of music settings

xleng:	.ds	3
xmmvoc:	.ds	16*9
xmmsti:	.ds	16
xpasti:	.ds	32
xstpr:	.ds	10
xtempo:	.ds	1
xsust:	.ds	1
xbegvm:	.ds	9
xbegvp:	.ds	9
xorgp1:	.ds	6*8
xorgnr:	.ds	6
xsmpkt:	.ds	8
xdrblk:	.ds	15
xdrvol:	.ds	3
xdrfrq:	.ds	20
xrever:	.ds	9
xloop:	.ds	1

.if withBIN=1
end:
.endif
