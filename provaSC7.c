// Basat en l'exemple LoadScreen5Image_2.c

#include "fusion-c/header/msx_fusion.h"
#include "fusion-c/header/vdp_graph2.h"
#include "fusion-c/header/vdp_sprites.h"
#include "spritessc7.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static FCB file;                            // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE (2560 * 2)

unsigned char LDbuffer[BUFFER_SIZE];               // Création d'un buffer de 2560 octets (20 lignes en Sc5)


static const unsigned char ball_pattern[] = {
  0x3C, /* 00111100 */
  0x7E, /* 01111110 */
  0xFF, /* 11111111 */
  0xFF, /* 11111111 */
  0xFF, /* 11111111 */
  0xFF, /* 11111111 */
  0x7E, /* 01111110 */
  0x3C  /* 00111100 */
};
static const unsigned char cross_pattern[] = {
  0b11111111,
  0b10010011,
  0b10010001,
  0b11111111,
  0b10010001,
  0b10010001,
  0b10010011,
  0b11111111
};
int x[32];
int y[32];
int vx[32];
int vy[32];

void FT_SetName( FCB *p_fcb, const char *p_name )  // Routine servant à vérifier le format du nom de fichier
{
  char i, j;
  memset( p_fcb, 0, sizeof(FCB) );
  for( i = 0; i < 11; i++ ) {
    p_fcb->name[i] = ' ';
  }
  for( i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++ ) {
    p_fcb->name[i] =  p_name[i];
  }
  if( p_name[i] == '.' ) {
    i++;
    for( j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.'); j++ ) {
      p_fcb->ext[j] =  p_name[i + j] ;
    }
  }
}
 
void FT_errorHandler(char n, char *name)            // Gère les erreurs
{
  Screen(0);
  SetColors(15,6,6);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}
 
int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer)        // Charge les données d'un fichiers
    {

        int rd=2560;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file  
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, BUFFER_SIZE );  // Read 20 lines of image data (128bytes per line in screen5)
             // Per guadar-ho a la pàgina 1 hi ha un offset de 256
             HMMC(buffer, 0,start_Y+256,512,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

return(1);
}
int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{

  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

char SC7Point(unsigned int x, unsigned char y){
  // Posició memòria a llegir és y*256 + x/2
  // Retorna un byte amb els colors de la posició x,y | x+1,y on la x és parell
  // Potser no cal fer-la i la funció Point 3.c ja ho fa
  unsigned int address = (y<<8) + (x>>1);
  return(Vpeek( address));
}

char mypalette[16 * 4];

void main(void)
{
  Screen(7);
  SetColors(15, 0, 0);
  Cls();
  SetActivePage(0);

  FT_LoadSc5Image("imatge.sc7", 0, LDbuffer); // On charge l'umage
  FT_LoadPalette("image.pl7", LDbuffer, mypalette);
  SetPalette((Palette *)mypalette);

  HMMM(0, 256, 0, 0, 48, 212); // és fins a 212 de les y, altrament esborrem dades de sprites. Sense overscan és 192

  SpriteReset();
  SpriteSmall();
  Sprite16();
  
  SetSpritePattern(0, Sprite32Bytes(main1a), 32); // 0
  SetSpritePattern(4, Sprite32Bytes(main1b), 32); // 1
  SetSpriteColors(0, paleta_main1a);
  SetSpriteColors(1, paleta_main1b);

  SetColorPalette(5, 7, 6, 2);
  SetColorPalette(6, 7, 3, 3);
  SetColorPalette(2, 0, 6, 1);
  SetColorPalette(3, 1, 4, 4);
  SetColorPalette(4, 4, 0, 0);

  PutSprite(0, 0, 50, 50, 0);
  PutSprite(1, 4, 50, 50, 0);

  HMMM(60, 257, 108, 180, 7, 10);
  HMMM(67, 257, 116, 182, 7, 10);
  HMMM(74, 257, 124, 184, 7, 10);
  HMMM(123, 257, 140, 186, 8, 10);

  const int pos_nums[11]={60,67,74,81,88, 95,103,110,117,123,131}; // L'última posició és on acaba el 9
  for(int k=0; k<10; k++) {
    HMMM(pos_nums[k],257,108+k*8,160+2*k,pos_nums[k+1]-pos_nums[k],9);
  }

  WaitForKey();

  Screen(0);

  Exit(0);
 
}
 
