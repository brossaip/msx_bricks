// Començaré des de zero seguint les indicacions de chipsfmpacpr1_en.txt

#include "fusion-c/header/msx_fusion.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void play_sound()__naked{
  // A les instruccions no parlen del registre 0E. NatyPC em va dir d'estudiar els vgm. És el que he fet. A on si parlen del registre 0E és al manual traduït de Yamaha
  // Aquests són els passos del Title.txt del Psycho World
  __asm
    ;; Preparem el FM per fer sonar els drums
    ld DE,#0x1620 
    call 00001$
    ld DE,#0x1750 
    call 00001$
    ld DE,#0x18C0 
    call 00001$
    ld DE,#0x2605
    call 00001$
    ld DE,#0x2705 
    call 00001$
    ld DE,#0x2801
    call 00001$
    ld DE,#0x0E20 
    call 00001$

    ;; Volums i instruments dels drums
    ld DE,#0x3601
    call 00001$
    ld DE,#0x3744 
    call 00001$
    ld DE,#0x3841
    call 00001$
    ld DE,#0x0E33 
    call 00001$

    ;; Volums i instruments canals 5 al 3
    ld DE,#0x2500
    call 00001$
    ld DE,#0x15AB 
    call 00001$
    ld DE,#0x3522
    call 00001$
    ld DE,#0x251A 
    call 00001$
    ld DE,#0x2400
    call 00001$
    ld DE,#0x14CC 
    call 00001$
    ld DE,#0x34B3
    call 00001$
    ld DE,#0x2414 
    call 00001$
    ld DE,#0x2300 
    call 00001$

    ;; Ara configura el canal sintètic
    ld DE,#0x0020
    call 00001$
    ld DE,#0x0171 
    call 00001$
    ld DE,#0x020D
    call 00001$
    ld DE,#0x0320 
    call 00001$
    ld DE,#0x04C1
    call 00001$
    ld DE,#0x05D5 
    call 00001$
    ld DE,#0x0656
    call 00001$
    ld DE,#0x0706 
    call 00001$

    ;; Del 3 al 0
    ld DE,#0x1301
    call 00001$
    ld DE,#0x3303 
    call 00001$
    ld DE,#0x231B
    call 00001$
    ld DE,#0x2200 
    call 00001$
    ld DE,#0x12AB
    call 00001$
    ld DE,#0x32D2 
    call 00001$
    ld DE,#0x2214
    call 00001$
    ld DE,#0x2100 
    call 00001$
    ld DE,#0x11AB
    call 00001$
    ld DE,#0x31B3 
    call 00001$
    ld DE,#0x2114
    call 00001$
    ld DE,#0x2000 
    call 00001$
    ld DE,#0x10CC
    call 00001$
    ld DE,#0x3003 
    call 00001$

    ;; Comença la música, però no sé com fer el wait dels segons
    ld DE,#0x201A
    call 00001$
    ld DE,#0x2500 
    call 00001$
    ld DE,#0x2400
    call 00001$
    ld DE,#0x2200 
    call 00001$
    ld DE,#0x2100
    call 00001$
    ld DE,#0x0E20 
    call 00001$
    ld DE,#0x3604
    call 00001$
    ld DE,#0x3844 
    call 00001$
    ld DE,#0x0E21
    call 00001$
    ld DE,#0x251A 
    call 00001$
    ld DE,#0x2416
    call 00001$
    ld DE,#0x2216 
    call 00001$
    ld DE,#0x2116
    call 00001$
    ld DE,#0x3003 
    call 00001$

    ;; He d'esperar perquè soni la nota
    ;; L'espera de la nota en el mbplayer ho fa a través d'interrupcions. És el que anomena el Hook
    ld B,#0xFF
00003$:
    bit #0,a    ; 8
    bit #0,a    ; 8
    bit #0,a    ; 8
    bit #0,a    ; 8
    bit #0,a    ; 8
    djnz 00003$


    ;; Per apagar el volum s'han de posar els Keys a 0
    ld B,#9
00002$:
    ld A,#0x20
    add A,B
    out (#0x7C),A
    ld A,#0
    out (#0x7D),A
    ex (SP),HL
    ex (SP),HL
    djnz 00002$
    ret

; Write FMPAC registers; El Moonblaster driver els escriu d'una altra manera
00001$:
    ld A,D
    out (#0x7C),A
    LD A,E
    out (#0x7D),A
    ex (SP),HL
    ex (SP),HL
    ret
  __endasm;
}

void main(){
  printf("Toquem una nota");
  ChangeCPU(0);
  play_sound();

  printf("\nJa ha sonat. Prem una tecla");
  WaitKey();
  ChangeCPU(1);
  Exit(0);
}

