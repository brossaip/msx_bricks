#include "FM_MBM.h"
#include "fusion-c/include/ayfx_player.h"
#include "fusion-c/include/msx_fusion.h"
#include "fusion-c/include/vdp_graph2.h"
#include "fusion-c/include/vdp_sprites.h"
#include "spritessc7.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void calcula_pos_mao();
void posa_puntuacio(int);
  void pinta_vides(char);
  void pinta_numero_level();
  char puc_esquerra();
  char puc_esquerra_gat();
  char puc_caure_esquerra();
  char puc_dreta();
  char puc_caure_dreta();
  char puc_caure_dreta_mirantEsq();
  void calcula_pos_car_principal(unsigned char);
  char puc_dreta_gat();
  void calcula_pos_car_secundari();
  void calcula_pos_mao();
  void main_loop(void);
  void init_sprites(void);
  void FT_SetName(FCB *, const char *);
  void FT_errorHandler(char, char *);
int FT_LoadSc5Image(char *, unsigned int, char *, unsigned int, unsigned int);
  int FT_LoadPalette(char *, char *, char *);
  int FT_openFile(char *);
  void init_pantalla_joc();
  void esborrar_pantalla_joc();
  void fer_res();
  void canvi_nivell();
  void gameOver();
void suicidi();
